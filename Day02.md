[TOC]

# 实验环境

- 本篇实验需要提前安装好VMware workstation pro软件

- 准备好CentOS-7-x86_64-DVD-2009.iso镜像文件

# Linux系统简介

- Linux是一种操作系统，操作系统是一大堆软件的集合
- 曾经，被Microsoft视为最大的威胁，服务器领域幕后的老大

![1687186198246](Day02.assets/1687186198246.png)

![1687186218369](Day02.assets/1687186218369.png)

## Linux发行版

![1687186240204](Day02.assets/1687186240204.png)

![1687186255137](Day02.assets/1687186255137.png)

## RHEL与CentOS

![1687186271578](Day02.assets/1687186271578.png)

## 国产操作系统

- 麒麟操作系统
  -  麒麟操作系统是由中国华为技术有限公司自主研发的一款操作系统，主要用于华为自己的产品上，例如手机、平板电脑、智能穿戴等。它主要分为麒麟OS微内核版和麒麟OS分布式微内核版两个版本。
  - 麒麟OS微内核版是采用微内核架构的操作系统，具有高效、灵活、安全、可靠的特点，支持多核、大型内存等高性能的硬件，并且可以实现资源的动态分配。
- 红旗Linux操作系统
  - 红旗Linux操作系统是中国第一个自主可控的操作系统，由中国电子信息产业集团有限公司（简称中电集团）负责开发。该操作系统自2000年正式发布以来，经过十多年的发展，已经形成了一套完整的操作系统产品线，包括服务器、桌面、嵌入式、云计算等多个领域。
- Deepin操作系统
  - Deepin（深度操作系统）是一款基于Linux的操作系统，由中国武汉的深度科技有限公司开发。其初衷是为了提供一体化、易用性基础上又能够满足用户各种需求的完整操作系统。不同于其他Linux版本，Deepin注重桌面使用界面的美观度和易用性。

# 安装CentOS7.9

安装虚拟机分为两步

1、虚拟硬件

2、为虚拟机安装操作系统

所需资料：
提前在电脑中安装好<font color='red'>VMware Workstation Pro</font>

准备好<font color='red'>CentOS-7-x86_64-DVD-2009.iso</font>镜像文件

## 步骤一：虚拟硬件

![1682403448608](Day02.assets/1682403448608.png)

![1682403516915](Day02.assets/1682403516915.png)

![1682403537698](Day02.assets/1682403537698.png)



![1677219935194](Day02.assets/1677219935194.png)

![1677219966889](Day02.assets/1677219966889.png)

![1687186529662](Day02.assets/1687186529662.png)

![1677220273543](Day02.assets/1677220273543.png)

![1677220285813](Day02.assets/1677220285813.png)

![1677220295959](Day02.assets/1677220295959.png)

![1677220302359](Day02.assets/1677220302359.png)

![1677220308279](Day02.assets/1677220308279.png)

![1677220314311](Day02.assets/1677220314311.png)

![1677220320038](Day02.assets/1677220320038.png)

![1677220325079](Day02.assets/1677220325079.png)

![1677220330504](Day02.assets/1677220330504.png)

## 步骤二：为虚拟机安装操作系统

![1687186614471](Day02.assets/1687186614471.png)

![1687186663851](Day02.assets/1687186663851.png)

![1687186687509](Day02.assets/1687186687509.png)



![1687186831597](Day02.assets/1687186831597.png)

![1687186858243](Day02.assets/1687186858243.png)

![1687186884991](Day02.assets/1687186884991.png)

![1687186902483](Day02.assets/1687186902483.png)

![1687186949471](Day02.assets/1687186949471.png)

![1687186976274](Day02.assets/1687186976274.png)

### 为root设置密码

![1687187041970](Day02.assets/1687187041970.png)

![1687187092762](Day02.assets/1687187092762.png)

### 创建普通用户并设置密码

![1687187062503](Day02.assets/1687187062503.png)

![1687187920204](Day02.assets/1687187920204.png)





### 首次初始化虚拟机

![1677220777654](Day02.assets/1677220777654.png)

![1677220790828](Day02.assets/1677220790828.png)

![1677220815195](Day02.assets/1677220815195.png)

# 登录操作系统

使用root用户和其密码登录

![1687187173113](Day02.assets/1687187173113.png)

## 系统偏好设置

![1687187515709](Day02.assets/1687187515709.png)

![1687187592145](Day02.assets/1687187592145.png)

![1687187618505](Day02.assets/1687187618505.png)

选择开始使用CentOS

![1677221090302](Day02.assets/1677221090302.png)

# 快照

- 快照：可以用于定格虚拟机某一个刻的状态

拍摄快照

![1687188807159](Day02.assets/1687188807159.png)

# LINUX目录结构

LINUX是一种<font color='red'>倒挂树形结构</font>

![1677221167861](Day02.assets/1677221167861.png)

## 常见目录及其作用

/bin         #存放二进制可执行文件，常用命令一般都在这里

/etc         #存放系统管理和配置文件 

/home    #存放所有普通用户的家目录

/usr         #存放系统应用程序

/opt 	#额外安装的可选应用程序包所放置的位置。比如，我们可以把tomcat等都安装到这里

/proc       #虚拟文件系统目录，是系统内存的映射，相当于是存储内存中的信息

/root       #管理员的家目录

/sbin       #存放二进制可执行文件，只有root才能访问。

​		   这里存放的是系统管理员使用的系统级别的管理命令和程 序。如ifconfig等

/dev        #用于存放设备文件

/mnt 	#系统管理员安装临时文件系统的安装点，系统提供这个目录是让用户临时挂载其他的文件系统

/boot      #存放用于系统引导时使用的各种文件 

/lib          #存放跟文件系统中的程序运行所需要的共享库及内核模块

/tmp       #用于存放各种临时文件

/var         #用于存放各种服务的日志文件、系统启动日志等

# LINUX磁盘表示方法

## LINUX中常见的磁盘接口类型

IDE接口类型

![1677221823791](Day02.assets/1677221823791.png)

SCSI接口类型

![1677221876963](Day02.assets/1677221876963.png)

PCle接口类型的固态硬盘(SSD)

![1683608229670](Day02.assets/1683608229670.png)

Linux中的"哲理"：一切皆文件~

![1683608459445](Day02.assets/1683608459445.png)

## 举例

/dev/sda: 表示SCSI接口类型的第一块磁盘

/dev/sdb: 表示SCSI接口类型的第二块磁盘

/dev/hdb: 表示IDE接口类型的第二块磁盘

/dev/hdc: 表示IDE接口类型的第三块磁盘

/dev/nvme0n1：表示SSD类型的第一块设备

/dev/nvme0n2：表示SSD类型的第二块设备

# 基本命令使用

## 命令行提示符的含义

打开命令行提示符：活动-->终端

[<font color='red'>当前系统登录用户</font>@<font color='blue'>主机名</font>   <font color='green'>工作目录</font>]<font color='orange'>#</font>

### 举例1

```shell
[root@localhost ~]#
```

当前登录系统用户为: root

主机名为: localhost

工作目录为: ~（~表示用户家目录）

#表示当前系统登录用户的身份为超级管理员（如果为普通用户登录会使用$表示）

### 举例2

```shell
[lisi@localhost ~]$
```

当前登录系统用户为: lisi

主机名为: localhost

工作目录为: ~（~表示用户家目录）

$ 表示当前系统登录用户的身份为普通用户（如果为超级管理登录会使用#表示）

## 目录探索"三剑客"

### pwd命令

pwd--- Print Working Directory

作用：显示当前工作目录

```shell
[root@localhost ~]# pwd
/root
```

### cd命令

cd --- Change Directory

作用：切换工作目录

格式：cd   [目标文件夹位置]



绝对路径：以/开头的路径（/etc/subject/cloudcomputing）

相对路径：不以/开头的路径，相对当前所在位置而言（etc/car/bmw）

```shell
[root@localhost ~]# cd /dev
[root@localhost dev]# pwd
/dev

[root@localhost dev]# cd /
[root@localhost /]# cd boot
[root@localhost boot]# pwd
/boot
```

### ls命令

ls --- List

作用：查看目录里的内容，或者查看资料是否存在

格式：ls  \[选项\]  \[目标\]

```shell	
[root@localhost ~]# cd  /     		#切换到根目录下
[root@localhost /]# pwd       		#查看当前位置
[root@localhost /]# ls        		#显示当前目录下的内容

[root@localhost /]# cd /root		#切换至/root目录下
[root@localhost ~]# ls				#查看当前目录下的内容

[root@localhost ~]# ls /opt			#查看指定目录/opt下的内容
[root@localhost ~]# ls /boot		#查看指定目录/boot下的内容
```

## 命令使用格式

Linux命令： 用来实现某一类功能的指令或程序，命令的执行依赖于解释器（例如：/bin/bash）

内部命令：属于解释器的一部分

外部命令：解释器之外的其他程序

格式：<font color='red'>命令字</font>   <font color='blue'>[选项] ...</font>    <font color='green'>\[参数1\] [参数2] ... </font>  

## cd命令与ls命令的高级使用

### cd命令高级使用

.   表示当前目录

..  表示父目录（也就是上一级目录）

```shell
[root@localhost ~]# cd /etc/pki/tls/				#切换至/etc/pki/tls目录
[root@localhost tls]# pwd							#查看当前工作目录
/etc/pki/tls
[root@localhost tls]# cd ..							#切换至父目录(上一级目录)
[root@localhost pki]# pwd							#查看当前工作目录
/etc/pki
[root@localhost pki]# cd ..							#切换至父目录(上一级目录)
[root@localhost etc]# pwd							#查看当前工作目录
/etc
[root@localhost etc]# cd ..							#切换至父目录(上一级目录)
[root@localhost /]# pwd								#查看当前工作目录
/
[root@localhost /]# cd ..							#切换至父目录(上一级目录)
[root@localhost /]#pwd								#还在根目录，因为根目录没有上一级目录	
/
```

   ~： 表示用户的家目录

   ~用户名：表示该用户的家目录

/root: 管理员用户root的家目录

/home: <font color='red'>存放</font>普通用户家目录<font color='red'>的目录</font>

```shell
[root@localhost /]# cd ~root/			#切换到root用户的家目录
[root@localhost ~]# pwd					#查看当前工作目录
/root
[root@localhost ~]# useradd tom			#创建一个普通用户tom
[root@localhost ~]# cd ~tom				#切换到tom用户的家目录
[root@localhost tom]# pwd
/home/tom

[root@localhost ~]# cd /				#切换到根目录下
[root@localhost /]# pwd					#查看当前工作目录
/
[root@localhost /]# cd 					#cd 不写参数表示切换到当前登录系统用户家目录
[root@localhost ~]# pwd					#查看当前工作目录
/root
```

### ls命令高级使用

常用命令选项

 -l：以长格式显示（显示详细属性）

 -A：包括名称以 . 开头的隐藏文档

 -d：显示目录本身(而不是内容)的属性

 -h：提供易读的容量单位(K、M)等

 -R：递归显示内容

```shell
[root@localhost ~]# ls -l /etc/passwd			#查看/etc/passwd详细属性
[root@locahost  ~]# ls -l -h /etc/passwd		#显示详细属性加上易读的单位
```

也可以将选项写到一起（复合选项）

```shell
[root@localhost ~]# ls -lh /etc/passwd
[root@localhost ~]# ls -ld /boot/				#显示目录本身的详细属性
[root@localhost ~]# ls -A  /root/				#显示目录所有数据，包括隐藏文档
[root@localhost ~]# ls -R  /opt/   				#递归查看/opt/目录，分目录进行显示
[root@localhost ~]# ls --help					#显示ls帮助信息
```

# 总结

- 掌握使用VMware快速安装CentOS7.9虚拟机

- 掌握Linux目录结构

- 掌握命令行提示符的含义

- 掌握ls、cd、pwd命令基本使用及高级使用