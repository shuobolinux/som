

# 监控概述

- 监控的目的
  - 报告系统运行状况
  - 每一部分必须同时监控
  - 内容包括吞吐量、反应时间、使用率等
  - 提前发现问题
  - 进行服务器性能调整前，知道调整什么
  - 找出系统的瓶颈在什么地方

- 监控的资源类别
  - 公开数据
    - Web、FTP、SSH、数据库等应用服务
    - TCP或UDP端口
  - 私有数据
    - CPU、内存、磁盘、网卡流量等使用信息一用户、进程等运行信息

- 自动化监控系统
  - Cacti:基于SNMP协议的监控软件，强大的绘图能力
  - Nagios: 基于Agent监控，强大的状态检查与报警机制插件极多，自己写监控脚本嵌入到Nagios非常方便- 
  - Zabbix 基于多种监控机制，支持分布式监控

# Zabbix简介

- Zabbix是一个高度集成的监控解决方案可以实现企业级的开源分布式监控
- Zabbix通过C/S模式采集监控数据
- Zabbix通过B/S模式实现Web管理

![1688913658582](Day14.assets/1688913658582.png)

## 环境部署

- 监控服务器需要使用Web页面操作，因此需要先部署LNMP
- 监控服务器可以通过SNMP或Agent采集数据数据可以写入MySQL、Oracle等数据库中
- 服务器使用LNMP实现web前端的管理
- 被监控主机
  - 被监控主机需要安装Zabbix_agent
  - 常见的网络设备一般支持SNMP
- 使用模板机克隆下方实验主机

| 主机名        | IP地址        |
| ------------- | ------------- |
| zabbix-server | 192.168.8.100 |
| zabbx-agent   | 192.168.8.101 |


zabbix-server部署监控环境（将教学环境中的som.tar.gz上传至虚拟机Zabbix-server的/root）

```shell
[root@proxy ~]# hostnamectl set-hostname zabbix-server		#修改主机名
```

```shell
[root@zabbix-server ~]# yum -y install gcc pcre-devel openssl-devel make #安装nginx依赖包
[root@zabbix-server ~]# mkdir /root/som
[root@zabbix-server ~]# tar -xf som.tar.gz  -C /root/som
[root@zabbix-server ~]# cd som/
[root@zabbix-server som]# tar -xf nginx-1.16.1.tar.gz 
[root@zabbix-server som]# cd nginx-1.16.1/
[root@zabbix-server nginx-1.16.1]# ./configure --with-http_ssl_module  
													 #配置nginx，支持加密功能
[root@zabbix-server nginx-1.16.1]# make 				 #编译
[root@zabbix-server nginx-1.16.1]# make install		 #编译并安装
```

```shell
[root@zabbix-server ~]# yum -y install php php-mysql php-fpm  #安装php相关软件
[root@zabbix-server ~]# yum -y install mariadb mariadb-devel mariadb-server	 #安装mariadb相关软件
```

- 修改nginx配置文件实现动静分离，并且需要添加几行内容，开启缓存容量
- 因为zabbix的web页面是使用php写的，这些代码需要占用大量的缓存

```shell
[root@zabbix-server~]# vim  /usr/local/nginx/conf/nginx.conf
http{
		fastcgi_buffers 8 16k; 					#缓存php生成的页面内容，8个16k
		fastcgi_buffer_size 32k; 				#缓存php生产的头部信息
		fastcgi_connect_timeout 300;			#连接PHP的超时时间
		fastcgi_send_timeout 300;				#发送请求的超时时间
		fastcgi_read_timeout 300;				#读取请求的超时时间
		location ~\.php${
			root html;							#访问根目录
			fastcgi_pass 127.0.0.1:9000;		#动态页面转发给9000端口
			fastcgi_index index.php;			#访问首页
			include fastcgi.conf;				#调用配置文件
}
```

启动相关服务

```shell
[root@zabbix-server ~]# systemctl start mariadb				#启动mariadb
[root@zabbix-server ~]# systemctl enable mariadb				
[root@zabbix-server ~]# systemctl start php-fpm				#启动php-fpm
[root@zabbix-server ~]# systemctl enable php-fpm			
[root@zabbix-server ~]# /usr/local/nginx/sbin/nginx			#启动nginx
```

编写测试页面

```shell
[root@zabbix-server nginx-1.16.1]# vim /usr/local/nginx/html/test.php
<?php
$i=33;
echo $i;
?>
```

真机访问测试<http://192.168.8.100/test.php>

## 安装Zabbix-server（服务端）

- 源码安装zabbix
- 使用MobaXterm将zabbix-3.4.4.tar.gz上传至zabbix-server和zabbix-agent主机的/root

```shell
[root@zabbix-server nginx-1.16.1]# cd ..
[root@zabbix-server ~]# yum -y install net-snmp-devel curl-devel libevent-devel		
														#安装zabbix所需要依赖
[root@zabbix-server ~]# tar -xf zabbix-3.4.4.tar.gz		#解压zabbix
[root@zabbix-server ~]# cd zabbix-3.4.4/
[root@zabbix-server zabbix-3.4.4]#./configure --enable-server --enable-proxy --enable-agent --with-mysql=/usr/bin/mysql_config  --with-net-snmp --with-libcurl	#配置zabbix
[root@zabbix-server zabbix-3.4.4]# make install			#安装zabbix
[root@zabbix-server zabbix-3.4.4]# ls /usr/local/etc/
[root@zabbix-server zabbix-3.4.4]# ls /usr/local/bin/
[root@zabbix-server zabbix-3.4.4]# ls /usr/local/sbin/			
```

初始化准备，创建数据库与数据库账户

```shell
[root@zabbix-server ~]# mysql      					#进入数据库
mysql> CREATE DATABASE zabbix CHARACTER SET utf8; 	#创建zabbix库支持中文字符集
mysql> GRANT ALL ON zabbix.*  to zabbix@"localhost" IDENTIFIED BY 'zabbix';   
													#授权用户zabbix，密码为zabbix
MariaDB [(none)]> EXIT;								#退出数据库连接
```

私用文件还原数据

```shell
MariaDB [(none)]> exit
[root@zabbix-server zabbix-3.4.4]# cd database/mysql/
[root@zabbix-server mysql]# mysql -uzabbix -pzabbix zabbix < schema.sql  #导入数据
[root@zabbix-server mysql]# mysql -uzabbix -pzabbix zabbix < images.sql  #导入数据
[root@zabbix-server mysql]# mysql -uzabbix -pzabbix zabbix < data.sql    #导入数据
#注：还原数据的时候顺序不能错
```

部署监控端站点

```shell
[root@zabbix-server mysql]# cd
[root@zabbix-server ~]# cd /root/zabbix-3.4.4/frontends/php/
[root@zabbix-server php]# cp -r * /usr/local/nginx/html/			#将页面放到访问根目录
[root@zabbix-server php]# chmod  -R 777 /usr/local/nginx/html/	#服务apache用户权限
```

### 初始化zabbix

- 真机浏览器访问: <http://192.168.8.100/index.php>

![1688917519945](Day14.assets/1688917519945.png)

根据报错提示需要修改php-fpm的配置文件

```shell
[root@zabbix-server~]# vim /etc/php.ini
878 date.timezone = Asia/Shanghai 		#设置时区
384 max_execution_time = 300			#最大执行时间，秒
672 post_max_size = 32M					#POST（上传提交）数据最大容量
394 max_input_time = 300				#服务器接收数据的时间限制
[root@zabbix-server~]# systemctl restart php-fpm		#重启php-fpm
```

```shell
[root@zabbix-server~]# yum -y install php-gd php-xml php-ldap php-bcmath php-mbstring
[root@zabbix-server~]# systemctl restart php-fpm			#重启php-fpm
```

![1688918112643](Day14.assets/1688918112643.png)

- 登录，用户名admin，密码zabbix

![img](Day14.assets/clip_image002.jpg)

- 设置中文环境

![img](Day14.assets/clip_image002-1688936118974.jpg)

## 启动zabbix_server

启动zabbix服务的监控程序，修改zabbix配置文件

```shell
[root@zabbix-server~]# vim /usr/local/etc/zabbix_server.conf
85 DBHost=localhost                                #数据库主机
95 DBName=zabbix                                   #设置数据库名称
111 DBUser=zabbix                                  #设置数据库账户
119 DBPassword=zabbix                              #设置数据库密码
38 LogFile=/tmp/zabbix_server.log                  #设置日志
```

启动服务

```shell
[root@zabbix-server php]# useradd zabbix             #不创建用户无法启动服务
[root@zabbix-server php]# zabbix_server 
[root@zabbix-server php]# ss -antlp | grep 10051
```

将zabbix_server设置为开机自启服务

```shell
[root@zabbix-server ~]# echo zabbix_server >> /etc/rc.d/rc.local
[root@zabbix-server ~]# echo zabbix_agentd >> /etc/rc.d/rc.local
[root@zabbix-server ~]# chmod +x /etc/rc.d/rc.local
```

## 部署zabbix客户端

部署zabbix被监控端服务器

安装软件（以8.101为例)

```shell
[root@zabbix-server ~]# scp -r zabbix-3.4.4/ 192.168.8.101:/root/     #将zabbix拷贝给zabbix-agent
[root@zabbix-agent ~]# yum -y install gcc pcre-devel autoconf   #安装客户端依赖包
[root@zabbix-agent ~]# cd zabbix-3.4.4/ 
[root@zabbix-agent zabbix-3.4.4]# ./configure --enable-agent    #配置zabbix
[root@zabbix-agentzabbix-3.4.4]# make install                   #安装zabbix
[root@zabbix-agent ~]# ls /usr/local/etc
[root@zabbix-agent ~]# ls /usr/local/bin
[root@zabbix-agent ~]# ls /usr/local/sbin
```

修改被监控主机配置文件

```shell
[root@zabbix-agent~]# vim /usr/local/etc/zabbix_agentd.conf
93 Server=127.0.0.1,192.168.8.100                 #允许访问服务地址列表，即允许谁监控我
134 ServerActive=192.168.8.100:10051              #监控服务器ip地址和端口
30 LogFile=/tmp/zabbix_agentd.log                 #日志文件
```

 启动服务zabbix-agentd

```shell
[root@zabbix-agent zabbix-3.4.4]# useradd zabbix        		#创建zabbix用户
[root@zabbix-agent zabbix-3.4.4]# zabbix_agentd          		#启动客户端服务
[root@zabbix-agent zabbix-3.4.4]# ss -antlp | grep 10050        #查看端口
```

设置被监控端服务为开机自启

```shell
[root@zabbix-agent ~]# echo zabbix_agentd >> /etc/rc.d/rc.local
[root@zabbix-agent ~]# chmod +x  /etc/rc.d/rc.local
```

# 基础监控

添加加监控主机

通过Configuration→>Hosts>Create Host创建、

注意: 设置中文环境后，中英文差异

![1688937889379](Day14.assets/1688937889379.png)

添加被监控主机

![1688937944686](Day14.assets/1688937944686.png)![1688938041342](Day14.assets/1688938041342.png)



![img](Day14.assets/clip_image005.png)

为主机添加关联的监控模板

√在"Templates"模板选项卡页面中

√找到Link new templates,select选择合适的模板添加

√这里我们选择Template os Linux模板

点击zabbix-agent被监控主机

![img](Day14.assets/clip_image007.jpg)

查看监控数据

可以点击"Monitoring"->"Latest data"，在过滤器中填写条件，根据群组和主机搜索即可

![img](Day14.assets/clip_image009.jpg)

Zabbix支持查看图形

找到需要的数据，点击后面的Graph（图形）

![img](Day14.assets/clip_image011.jpg)



# 自定义监控

开启自定义监控功能(zabbix-agent操作，被监控端修改Agent文件)

```shell
[root@zabbix-agent~]#vim /usr/local/etc/zabbix_agentd.conf
280 UnsafeUserParameters=1                	 					#是否允许自定义支持key
264 Include=/usr/local/etc/zabbix_agentd.conf.d/                #加载配置文件目录
```

创建自定义Key

```shell
[root@zabbix-agent ~]# cd /usr/local/etc/zabbix_agentd.conf.d/
[root@zabbix-agent zabbix_agentd.conf.d]# vim count.line.passwd        #自定义监控脚本
UserParameter=count.line.passwd, sed -n '$=' /etc/passwd       #统计/etc/passwd有多少行
注：自定义key语法格式UserParameter=自定义key名称,命令
```

重启Agentd服务

```shell
[root@zabbix-agent ~]# killall zabbix_agentd                     #杀掉zabbix_agentd进程
[root@zabbix-agent ~]# zabbix_agentd                             #重启zabbix_agentd服务
```

测试自定义key是否生效

```shell
[root@zabbix-agent~]# zabbix_get -s 127.0.0.1 -k count.line.passwd
```

## 创建自定义监控模板

登录监控服务器Web管理页面

​      √选择Configuration→>Templates创建模板

![img](Day14.assets/clip_image002-1688941045262.jpg)

### 配置监控模板

设置模板名称与组名称

​         √ Template name

​         √ Visible name

​         √ New group

![img](Day14.assets/clip_image004-1688941045262.jpg)

![img](Day14.assets/clip_image005-1688941045263.png)

模板添加后，默认模板中没有任何应用、项目、触发器、图形等

### 创建应用集

​         点击模板后面的Application，刷新出的页面中选择Create

​         Application

​         设置Application name，点击Add

![img](Day14.assets/clip_image007-1688941045263.jpg)

![img](Day14.assets/clip_image009-1688941045263.jpg)

![img](Day14.assets/clip_image011-1688941045263.jpg)

回到模板

### 创建监控项目

​         √Configuration→>Templates→>ltems→Create item

![img](Day14.assets/clip_image013.jpg)

![img](Day14.assets/clip_image014.png)

设置项目参数

​         项目名称

​         自定义key(必须与配置文件一致)

​         应用集选择刚刚创建的应用(Application)

![img](Day14.assets/clip_image016.jpg)

![img](Day14.assets/clip_image017.png)

![img](Day14.assets/clip_image018.png)

### 创建图形

​          与监控项目类似，为监控数据创建图形

![img](Day14.assets/clip_image020.jpg)

设置图形参数

​         √填写名称

​         √图形类别（以此为线条、填充图、饼图、分割饼图)

​         √添加监控项目

![img](Day14.assets/clip_image022.jpg)

![img](Day14.assets/clip_image024.jpg)

## 将模板关联主机

​         Configuration>Hosts→选择主机

![img](Day14.assets/clip_image026.jpg)

点击Templates，select选项监控项目，add添加

添加完成后，点击Update更新主机配置

![img](Day14.assets/clip_image028.jpg)

![img](Day14.assets/clip_image030.jpg)

点击图形查看

 

测试验证，批量创建用户

```shell
[root@zabbix-agent zabbix_agentd.conf.d]# for i in {1..10}
> do 
> 	useradd a$i
> done
```

点击Monitoring→Craphs→>选择条件查看图形

 

 

![img](Day14.assets/clip_image032.jpg)

# zabbix监控网络设备

- 当被监控端无法安装zabbix_agentd的时候将无法使用zabbix_agent监控
- 这种情况可以基于SNMP协议进行监控
- 后续可以使用华为的eNSP设备模拟路由器，进行监控网路设备实验

# 总结

- 掌握Zabbix监控服务的作用
- 掌握监控流程