[TOC]

# MySQL主从同步概述

- 实现不同MySQL服务器之间数据实时同步的解决方案
- 通过主从同步可以实现数据备份的作用

## MySQL主从同步原理

![1684077714595](MySQL主从同步.assets/1684077714595.png)

## MySQL主从同步结构模式

- 一主一从
- 一主多从
- 链式复制
- 互为主从(扩展实验)

## MySQL主从同步搭建

### 搭建步骤

**- master(主服务器)**
​	1）开启binlog日志
​	2）授权主从同步用户
​	3）备份已有数据
**- slave1(从服务器)**
​	1）设置server_id，可不开启binlog日志
​	2）还原数据(实现主从结构前保证服务器基础数据统一)
​	3）搭建主从关系

### 一主一从

![1691911327542](MySQL主从同步.assets/1691911327542.png)

### 实验环境

使用CentOS7.9模板机克隆实验虚拟机（配置如下信息）

IP地址使用的是DHCP自动分配，因此请以自己分配的为准！

| 主机名 | IP地址        | 角色      |
| ------ | ------------- | --------- |
| master | 192.168.8.100 | 主服务器  |
| slave1 | 192.168.8.101 | 从服务器1 |

为两台虚拟机配置IP地址、指定阿里镜像站作为yum源

配置master主机

```shell
[root@localhost ~]# hostnamectl set-hostname master				#配置主机名
[root@master ~]# nmcli connection modify ens33 ipv4.method auto \
connection.autoconnect yes
[root@master ~]# rm -rf /etc/yum.repos.d/*.repo	 				#删除自带的repo文件
[root@master ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     		 #下载阿里镜像源
[root@master ~]# yum clean all									#清空缓存
[root@master ~]# yum repolist									#查看yum
```

配置slave主机

```shell
[root@localhost ~]# hostnamectl set-hostname slave1				#配置主机名
[root@slave1 ~]# nmcli connection modify ens33 ipv4.method auto \
connection.autoconnect yes
[root@slave1 ~]# rm -rf /etc/yum.repos.d/*.repo	 				#删除自带的repo文件
[root@slave1 ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     		 #下载阿里镜像源
[root@slave1 ~]# yum clean all									#清空缓存
[root@slave1 ~]# yum repolist									#查看yum
```

2台主机均使用MobaXterm远程链接

<font color='red'>将master和slave1搭建成MySQL主从结构</font>	

#### master主机

将学习环境中的<font color='red'>mysql8-centos7</font>目录上传至master和slave1主机的/root/

```shell
[root@master ~]# cd mysql8-centos7/
[root@master mysql8-centos7]# yum -y localinstall *.rpm 
```

master主机指定server_id，开启binlog日志

```shell
[root@master ~]# vim /etc/my.cnf
...此处省略1万字，在第4行下方写入，不要写行号！...
  4 [mysqld]
  5 server_id=100					#指定server_id,每台主机都不一样，可以使用IP地址主机位区分
  6 log_bin=master					#指定binlog日志名
  ...此处省略1万字...
```

```shell
 [root@master ~]# systemctl restart mysqld				#重启动mysqld服务
 [root@master ~]# ls /var/lib/mysql/master*				#验证是否成功
 /var/lib/mysql/master.000001  /var/lib/mysql/master.000002 /var/lib/mysql/master.index
```

```shell
[root@master ~]# grep -i password /var/log/mysqld.log 	#过滤初始密码（每个人都不一样）
2023-07-16T13:04:50.381204Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: (wgrk:_s(7yQ
[root@master ~]# mysql -uroot -p'(wgrk:_s(7yQ'			#连接数据库
```

```sql
mysql> ALTER USER root@"localhost" IDENTIFIED BY '123tedu.CN';	#修改root密码
mysql> SET GLOBAL validate_password.policy=LOW;					#设置密码策略
mysql> SET GLOBAL validate_password.length=4;					#设置密码长度为4
mysql> SET GLOBAL validate_password.check_user_name=OFF;  #关闭用户名检测可以用用户名作为密码
```

用户授权（用户slave1，密码为slavepwd，这个用户用于从服务器连接主服务器同步数据）

- 使用 mysql_native_password 插件验证该用户的密码
- REPLICATION SLAVE 表示使用户拥有向主服务器复制的权限

```sql
mysql> CREATE USER 'slave1'@'%' IDENTIFIED with mysql_native_password BY 'slavepwd';
mysql> GRANT REPLICATION SLAVE ON *.* TO 'slave1'@'%';
```

查看日志信息

```sql
mysql> SHOW MASTER STATUS ;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| master.000002 |      984 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

备份master主机上的数据（虽然现在没有多余的数据，但工作中一定会有旧数据）

```shell
[root@master ~]# mysqldump -hlocalhost -uroot -p'123tedu.CN' -A > ab1.sql	#备份已有数据
[root@master ~]# scp ab1.sql 192.168.8.101:/root		#同步备份文件（IP不要照抄）
```

#### slave1主机

- 运行数据库服务
- 指定 server_id
- 指定主服务器信息
- 启动 slave  进程
- 查看状态
- 需要先将master上的数据手动还原至slave主机
- 确保master主机和slave主机UUID是不相同的，因为都是从模板克隆的裸机，所以这里可以不用考虑该问题

slave1主机安装mysql

将学习环境中的<font color='red'>mysql8-centos7</font>目录上传至master和slave1主机的/root/

```shell
[root@slave1 ~]# cd mysql8-centos7/
[root@slave1 mysql8-centos7]# yum -y localinstall *.rpm 
```

slave1主机修改server_id

```shell
[root@slave1 ~]# vim /etc/my.cnf
......
4 [mysqld]
5 server_id=101         						#指定id号，默认与IP地址的主机位相同
......
[root@slave1 ~]# systemctl restart mysqld		#重启服务
```

slave1主机设置密码

```shell
[root@slave1 ~]# grep -i password /var/log/mysqld.log		#过滤初始密码（每个人都不一样）
2023-07-16T13:33:20.939066Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: (f190mI%onK%
[root@slave1 ~]# mysql -uroot -p'(f190mI%onK%'
```

修改密码

```sql
mysql> ALTER USER root@"localhost" IDENTIFIED BY '123tedu.CN';	#修改root密码
mysql> SET GLOBAL validate_password.policy=LOW;					#设置密码策略
mysql> SET GLOBAL validate_password.length=4;					#设置密码长度为4
mysql> SET GLOBAL validate_password.check_user_name=OFF;  #关闭用户名检测可以用用户名作为密码
```

还原master主机备份过来的数据

```shell
[root@slave1 ~]# mysql  -uroot -p'123tedu.CN' <  /root/ab1.sql 	#数据还原
```

slave1指定主服务器信息

```sql
####指定主服务器信息
#MASTER_HOST=       				指定主服务器的IP地址
#MASTER_USER=       				指定主服务器授权用户 
#MASTER_PASSWORD=   				指定授权用户的密码
#MASTER_LOG_FILE=   				指定主服务器binlog日志文件(到master上查看)
#MASTER_LOG_POS=   					指定主服务器binlog日志偏移量(去master上查看)
mysql> CHANGE MASTER TO 
    -> MASTER_HOST="192.168.8.100",				#指定自己主服务器master的IP地址
    -> MASTER_USER="slave1", 
    -> MASTER_PASSWORD="slavepwd",
    -> MASTER_LOG_FILE="master.000002",
    -> MASTER_LOG_POS=984;
Query OK, 0 rows affected, 2 warnings (0.01 sec)
mysql> START SLAVE;					#启动SLAVE进程
mysql> SHOW SLAVE STATUS \G;		#查看主从同步状态
```

#### 验证主从同步

主服务器master写入数据验证

```sql
mysql> CREATE DATABASE som;			#新建som库
```

从服务器slave1写入数据验证

```slave
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| som                |
| sys                |
+--------------------+
5 rows in set (0.00 sec)
```

主服务器master写入数据验证

```sql
mysql> CREATE TABLE som.t1(id INT,name CHAR(10));
mysql> INSERT INTO som.t1 VALUES(1,"Sam");
mysql> INSERT INTO som.t1 VALUES(2,"Jack");
```

从服务器slave1写入数据验证

```shell
mysql> SELECT * FROM som.t1;
+------+------+
| id   | name |
+------+------+
|    1 | Sam  |
|    2 | Jack |
+------+------+
```

### 一主多从

![1691911686625](MySQL主从同步.assets/1691911686625.png)

| 主机名 | IP地址        | 角色      |
| ------ | ------------- | --------- |
| master | 192.168.8.100 | 主服务器  |
| slave1 | 192.168.8.101 | 从服务器1 |
| slave2 | 192.168.8.102 | 从服务器2 |

使用CentOS7.9模板机克隆slave2虚拟机，并为该虚拟机配置IP地址和阿里云镜像站点的yum源

<font color='red'>配置完基础环境之后为slave2拍摄一张快照命名为"基础环境"，用于后边的主从从实验，在操作前还原快照即可</font>

```shell
[root@localhost ~]# hostnamectl set-hostname slave2				#配置主机名
[root@slave2 ~]# nmcli connection modify ens33 ipv4.method auto \
connection.autoconnect yes
[root@slave2 ~]# rm -rf /etc/yum.repos.d/*.repo	 				#删除自带的repo文件
[root@slave1 ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     		 #下载阿里镜像源
[root@slave2 ~]# yum clean all									#清空缓存
[root@slave2 ~]# yum repolist									#查看yum
```

#### master主机

master主机需要重新完全备份（因为相对与上次备份已经产生了新数据）

```shell
[root@master ~]# mysqldump -uroot -p'123tedu.CN' -A > ab2.sql
[root@master ~]# scp ab2.sql 192.168.8.102:/root
```

#### slave2主机

slave2主机操作

将学习环境中的<font color='red'>mysql8-centos7</font>目录上传至master和slave2主机的/root/

```shell
[root@slave2 ~]# cd mysql8-centos7/
[root@slave2 mysql8-centos7]# yum -y localinstall *.rpm				#安装MySQL
```

slave2主机修改server_id

```shell
[root@slave2 ~]# vim /etc/my.cnf
......
4 [mysqld]
5 server_id=102         						#指定id号，默认与IP地址的主机位相同
......
[root@slave2 ~]# systemctl restart mysqld		#重启服务
```

slave2主机设置密码

```shell
[root@slave2 ~]# grep -i password /var/log/mysqld.log		#过滤初始密码（每个人都不一样）
2023-07-16T16:28:20.939066Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: eqCaZ1f=2*nu
[root@slave2 ~]# mysql -uroot -p'eqCaZ1f=2*nu'
```

修改密码

```sql
mysql> ALTER USER root@"localhost" IDENTIFIED BY '123tedu.CN';	#修改root密码
mysql> SET GLOBAL validate_password.policy=LOW;					#设置密码策略
mysql> SET GLOBAL validate_password.length=4;					#设置密码长度为4
mysql> SET GLOBAL validate_password.check_user_name=OFF;  #关闭用户名检测可以用用户名作为密码
```

还原master主机备份过来的数据（使用ab2.sql）

```shell
[root@slave2 ~]# mysql  -uroot -p'123tedu.CN' <  /root/ab2.sql 	#数据还原
```

slave2指定主服务器信息

- MASTER_LOG_FILE和MASTER_LOG_POS指定为master主机现使用的文件（SHOW MASTER STATUS查看）

- master主机查看（每个人都不一样，以自己的为准）

```sql
mysql> SHOW MASTER STATUS ;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| master.000003 |     1461 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

```sql
####slave2指定主服务器信息
#MASTER_HOST=       				指定主服务器的IP地址
#MASTER_USER=       				指定主服务器授权用户 
#MASTER_PASSWORD=   				指定授权用户的密码
#MASTER_LOG_FILE=   				指定主服务器binlog日志文件(到master上查看)
#MASTER_LOG_POS=   					指定主服务器binlog日志偏移量(去master上查看)
mysql> CHANGE MASTER TO 
    -> MASTER_HOST="192.168.8.100",				#指定自己主服务器master的IP地址
    -> MASTER_USER="slave1", 
    -> MASTER_PASSWORD="slavepwd",
    -> MASTER_LOG_FILE="master.000003",
    -> MASTER_LOG_POS=1461;
Query OK, 0 rows affected, 2 warnings (0.01 sec)
mysql> START SLAVE;					#启动SLAVE进程
mysql> SHOW SLAVE STATUS \G;		#查看主从同步状态
```

#### 验证主从同步

master测试数据写入

```sql
mysql> CREATE TABLE som.t2(id INT,name CHAR(10),male ENUM("male","female"));
mysql> INSERT INTO som.t2 VALUES(1,"Sam","male");
mysql> INSERT INTO som.t2 VALUES(2,"Janner","female");
```

slave1验证数据是否同步

```sql
mysql> USE som;
mysql> SHOW TABLES;						#多出了som.t2表
mysql> SELECT * FROM som.t2;
+------+--------+--------+
| id   | name   | male   |
+------+--------+--------+
|    1 | Sam    | male   |
|    2 | Janner | female |
+------+--------+--------+
2 rows in set (0.00 sec)
```

slave2验证数据是否同步

```sql
mysql> USE som;
mysql> SHOW TABLES;						#多出了som.t2表
mysql> SELECT * FROM som.t2;
+------+--------+--------+
| id   | name   | male   |
+------+--------+--------+
|    1 | Sam    | male   |
|    2 | Janner | female |
+------+--------+--------+
2 rows in set (0.00 sec)
```

### 链式复制（主从从）

slave1主机既是master主机的从服务器，也是slave2主机的主服务器

<font color='red'>slave2使用之前拍摄的"基础环境"快照还原至没有安装MySQL状态</font>

![1691918139190](MySQL主从同步.assets/1691918139190.png)

| 主机名IP | IP地址        | 角色              |
| -------- | ------------- | ----------------- |
| master   | 192.168.8.100 | 主服务器          |
| slave1   | 192.168.8.101 | 从服务器/主服务器 |
| slave2   | 192.168.8.102 | 从服务器          |

#### slave1主机

slave1备份所有数据，同步至slave2（主从同步前的准备工作）

```sql
[root@slave1 ~]# mysqldump  -uroot -p'123tedu.CN' -A > /root/all.sql
[root@slave1 ~]# scp /root/all.sql root@192.168.8.102:/root/
```

slave1主机<font color='red'>启用允许级联复制</font>

```shell
[root@slave1 ~]# vim /etc/my.cnf								#编辑主配置文件
......
4 [mysqld]
5 server_id=101         			#指定id号，默认与IP地址的主机位相同
6 log_bin=master					#指定binlog日志
7 log_slave_updates					#启用级联复制(本身是主和从两种角色的机器需配置该项)
......
[root@slave1 ~]# systemctl restart mysqld		#重启服务
```

#### slave2主机

slave2主机安装mysql，修改配置文件，指定serve_id，重启服务

```shell
[root@slave2 ~]# cd mysql8-centos7/
[root@slave2 mysql8-centos7]# yum -y localinstall *.rpm
[root@slave2 ~]# vim /etc/my.cnf
......
4 [mysqld]
5 server_id=102         						#指定id号，默认与IP地址的主机位相同
......
[root@slave2 ~]# systemctl restart mysqld		#重启服务
```

slave2主机修改密码

```shell
[root@slave2 ~]# grep -i password /var/log/mysqld.log		#过滤初始密码（每个人都不一样）
2023-07-16T17:38:40.939066Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: awwB%7?uu#yf
[root@slave2 ~]# mysql -uroot -p'awwB%7?uu#yf'
```

修改密码

```sql
mysql> ALTER USER root@"localhost" IDENTIFIED BY '123tedu.CN';	#修改root密码
mysql> SET GLOBAL validate_password.policy=LOW;					#设置密码策略
mysql> SET GLOBAL validate_password.length=4;					#设置密码长度为4
mysql> SET GLOBAL validate_password.check_user_name=OFF;  #关闭用户名检测可以用用户名作为密码
```

slave2主机还原slave1主机备份的数据

```shell
[root@slave2 ~]# mysql -uroot -p'123tedu.CN' < /root/all.sql
```

slave1主机查看binlog日志和偏移量

```sql
mysql> SHOW MASTER STATUS ;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| master.000001 |      157 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

slave2指定slave1为主服务器

```sql
####slave2指定主服务器信息
#MASTER_HOST=       				指定主服务器的IP地址
#MASTER_USER=       				指定主服务器授权用户 
#MASTER_PASSWORD=   				指定授权用户的密码
#MASTER_LOG_FILE=   				指定主服务器binlog日志文件(到master上查看)
#MASTER_LOG_POS=   					指定主服务器binlog日志偏移量(去master上查看)
mysql> CHANGE MASTER TO 
    -> MASTER_HOST="192.168.8.101",			#指定slave1主机的IP地址
    -> MASTER_USER="slave1", 				#这个用户slave1从master同步过，如果没有需要授权
    -> MASTER_PASSWORD="slavepwd",
    -> MASTER_LOG_FILE="master.000001,		#slave1的日志文件
    -> MASTER_LOG_POS=157;					#slave1的偏移量
Query OK, 0 rows affected, 2 warnings (0.01 sec)
mysql> START SLAVE;							#启动SLAVE进程
mysql> SHOW SLAVE STATUS \G;				#查看主从同步状态
			...
            Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
            ...
```

#### 验证链式复制

master主机写入数据验证链式复制（主从从）

```sql
mysql> CREATE TABLE som.t3(id INT,name CHAR(8),age TINYINT UNSIGNED);
mysql> INSERT INTO som.t3 VALUES(1,"tom",8);
mysql> INSERT INTO som.t3 VALUES(2,"jerry",15);;
Query OK, 1 row affected (0.00 sec)
```

slave1验证数据是否写入

```sql
mysql> USE som;
mysql> SHOW TABLES ;					#多出了som.t3表
+---------------+
| Tables_in_som |
+---------------+
| t1            |
| t2            |
| t3            |
+---------------+
3 rows in set (0.00 sec)

mysql> SELECT * FROM som.t3 ;			#som.t3表中有数据
+------+-------+------+
| id   | name  | age  |
+------+-------+------+
|    1 | tom   |    8 |
|    2 | jerry |   15 |
+------+-------+------+
2 rows in set (0.00 sec)
```

slave2验证数据是否写入

```sql
mysql> USE som;
mysql> SHOW TABLES ;					#多出了som.t3表
+---------------+
| Tables_in_som |
+---------------+
| t1            |
| t2            |
| t3            |
+---------------+
3 rows in set (0.00 sec)

mysql> SELECT * FROM som.t3 ;			#som.t3表中有数据
+------+-------+------+
| id   | name  | age  |
+------+-------+------+
|    1 | tom   |    8 |
|    2 | jerry |   15 |
+------+-------+------+
2 rows in set (0.00 sec)
```

## MySQL主从同步复制模式

MySQL主从同步复制模式指的是主服务器(执行写操作的服务器)什么时候将SQL命令的执行结果返还给客户端

分为三种情况：

- 异步复制(默认)

  - 主节点在执行写操作后，将写操作的日志异步发送到从节点。主节点不会等待从节点的同步完毕，直接讲结果返回给客户端，因此主节点可以以较高的速度执行写操作，而从节点可能会有一定的延迟。由于异步复制的延迟，如果主节点故障或数据丢失，可能会造成从节点数据与主节点不一致。

- 全同步复制

  - 在全同步复制中，主节点在执行写操作后，等待所有从节点全部同步完数据之后，在将结果返回至客户端这样可以确保主节点和从节点的数据一致性，但会影响主节点的写操作速度，因为主节点需要等待从节点的确认。

- 半同步复制

  - 在半同步复制中，主节点在执行写操作后，等待至少一个从节点同步完数据再将结果返回至客户端。这样可以提高主节点的写操作速度，同时保证主节点和至少一个从节点的数据一致性。但如果从节点故障或延迟高，可能会造成主节点的等待时间增加。

# 总结

- 掌握MySQL主从同步部署流程
  - 一主一从
  - 一主多从
  - 链式复制
- 掌握MySQL主从复制模式特点


