# 软件运维Day04-补充

## 周期性计划任务

- 在固定时间可以完成相同的任务，被称之为周期性计划任务
- 由crond服务提供
- 需要将定时任务，写到一个文件
- 书写格式如下
  - 分  时  日   月  周     任务(绝对路径)
  - 分：0-59
  - 时：0-23
  - 日：1-31
  - 月：1-12
  - 周：0-7（其中0和7表示周日）
  - *表示匹配相应位置里的每一个
  - /表示每
  - ,表示间隔
  - -表示范围
- 示例
  - 53   14   *    *     *       	xxx 		#每天14:53执行xxx任务
  - 10    17   2    *     1        xxx           #每月2号17:10或者每月每周周一17:10执行xxx任务
  - */2   16   *   *    *          xxx           #每天16:00的每2分钟执行一次xxx任务

## 周期性计划任务使用

- 命令：crontab  [选项]
  - -e：编辑计划任务
  - -l：查看计划任务
  - -u：指定用户
  - -r：清空计划任务

### 案例一

为root用户编写周期性计划任务，每一分钟做一次date  >>  /opt/date.txt

```shell
[root@localhost ~]# crontab -e				#为当前登录系统用户编写计划任务
* * * * * date >>  /opt/date.txt
[root@localhost ~]# crontab -l				#查看当前用户有哪些计划任务
* * * * * date >>  /opt/date.txt
```

验证（需要等至少1分钟）

```shell
[root@localhost ~]# cat /opt/date.txt 
2023年 09月 21日 星期四 17:06:01 CST
2023年 09月 21日 星期四 17:07:01 CST
2023年 09月 21日 星期四 17:08:02 CST
2023年 09月 21日 星期四 17:09:01 CST
2023年 09月 21日 星期四 17:10:01 CST
2023年 09月 21日 星期四 17:11:01 CST
```

清空计划任务

```shell
[root@localhost ~]# crontab -l
```

### 案例二

每天14:23分对/var/log/目录打包，将打包文件存放在/tmp/log.tar.gz

```shell
[root@localhost ~]# which tar			#查看tar命令的绝对路径（计划任务要求命令写绝对路径）
/usr/bin/tar
[root@localhost ~]# crontab -e -u root		#为root用户编写计划任务
23 14 * * * /usr/bin/tar  -zcf /tmp/log.tar.gz  /var/log
[root@localhost ~]# crontab -e -u root		#查看root用户的计划任务
23 14 * * * /usr/bin/tar  -zcf /tmp/log.tar.gz  /var/log
```

如果为了及时验证结果，可以把之间改成每分钟都打包

```shell
[root@localhost ~]# crontab -e -u root		#为root用户编写计划任务
* * * * * /usr/bin/tar  -zcf /tmp/log.tar.gz  /var/log
```

验证，（需要至少等待1分钟）

```shell
[root@localhost ~]# ls /tmp/log.tar.gz 
/tmp/log.tar.gz
```

清除root用户的计划任务

```shell
[root@localhost ~]# crontab -r -u root		#清空root用户的计划任务
[root@localhost ~]# crontab -l -u root		#查看root用户的计划任务
no crontab for root
```

