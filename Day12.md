[TOC]

# yum概述

- Yum是Linux系统中的一个软件包管理器，它能够自动解决软件包及其依赖关系的安装、更新和卸载等问题。它是Red Hat Linux发行版中的默认包管理工具，如Fedora、CentOS等，也被许多其他Linux发行版广泛采用。

配置虚拟机A、虚拟机B使用NAT模式联网

将虚拟机A、B网络模式选择为NAT(两台主机都设置)

![1688506962153](Day12.assets/1688506962153.png)

虚拟机A自动获取入网参数（网卡名不要照抄）

```shell
[root@som ~]# nmcli connection modify eth0 ipv4.method auto connection.autoconnect yes
[root@som ~]# nmcli connection up eth0
[root@som ~]# ping www.baidu.com					#测试验证
```

虚拟机B自动获取入网参数（网卡名不要照抄）

```shell
[root@pc207 ~]# nmcli connection modify ens33 ipv4.method auto \
> connection.autoconnect yes
[root@pc207 ~]# nmcli connection up ens33			
[root@pc207 ~]# ping www.baidu.com 					#测试验证
```

# 部署阿里镜像源

- 阿里云镜像站点，主要提供各种开发者资源和开源软件的镜像下载
  - https://developer.aliyun.com/mirror/
- wget为Linux下载命令
  - -O表示将数据另存为制定路径

虚拟机A操作

```shell
[root@som ~]# rm -rf /etc/yum.repos.d/*.repo	 	#删除自带的repo文件
[root@som ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		  #下载阿里镜像源
```

虚拟机B操作

```shell
[root@pc207 ~]# rm -rf /etc/yum.repos.d/	 *.repo	 	#删除自带的repo文件
[root@pc207 ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo	  	#下载阿里镜像源
```

## yum基本使用

- yum clean all                                 #清空缓存
- yum  repolist -v                             #列出仓库状态
- yum list                                          #列出现有所有软件包
- yum -y install  软件名			#安装软件包
- yum remove  软件名                    #卸载软件

```shell
[root@som ~]# yum clean all                 #清空缓存
[root@som ~]# yum  repolist -v              #列出仓库状态
[root@som ~]# yum list                      #列出现有所有软件包
[root@som ~]# yum -y install  nfs-utils     #安装软件包
[root@som ~]# rpm -q  nfs-utils     		#查看本地是否安装了nfs-utils软件
```

# NFS网络文件系统

## NFS共享概述

- Network File System,网络文件系统
  - 用途:为客户机提供共享使用的文件夹
  - 协议:NFS(TCP/UDP 2049)、RPC(TCP/UDP 111)
- 所需软件包:nfs-utils

![1688506986995](Day12.assets/1688506986995.png)

## 部署NFS服务端

虚拟机A为服务端，构建NFS共享

安装软件包nfs-utils

```shell
[root@som ~]# yum -y install  nfs-utils     #安装软件包
[root@som ~]# rpm -q nfs-utils				#查看是否安装
```

创建共享目录及完成共享配置                                

```shell
[root@som ~]# mkdir /test
[root@som ~]# echo haha > /test/1.txt
[root@som ~]# man exports         #查看帮助，搜索example
[root@som ~]# vim  /etc/exports
/test    192.168.192.0/24(ro)      #不要照抄共享至自己的B主机所在网段，文件夹路径客户机地址(权限) 
```

3.重启nfs-server服务

```shell
[root@som ~]# systemctl restart nfs-server			#重新启动nfs-server服务
[root@som ~]# systemctl enable nfs-server			#将nfs-server服务设置为开机自启动
```

## 部署NFS客户端 

虚拟机B作为客户端测试访问

1.查看服务端共享

```shell
[root@pc207 ~]# yum -y install  nfs-utils     	#安装软件包
[root@pc207 ~]# rpm -q nfs-utils				#查看是否安装
[root@pc207 ~]# showmount -e 192.168.192.100	#查看服务端共享的数据（IP地址不要照抄）
Export list for 192.168.192.100:
/test    192.168.192.0/24
```

2.进行手动挂载

```shell
[root@pc207 ~]# mkdir /mnt/nfsmnt
[root@pc207 ~]# mount 192.168.192.100:/test  /mnt/nfsmnt 
[root@pc207 ~]# df -h
```

3.开机自动挂载

- _netdev：声明网络设备，系统在网络服务配置完成后,再挂载本设备

```shell
[root@pc207 ~]# vim /etc/fstab 
...
192.168.192.100:/test /mnt/nfsmnt  nfs defaults,_netdev  0 0
```

# Tomcat服务

- Tomcat是一个开源的Java Servlet容器，它实现了Java Servlet和JavaServer Pages（JSP）规范，并提供了一个运行环境，用于在服务器上托管Java Web应用程序。
- Tomcat服务特点
  - <font color='red'>轻量级和灵活性</font>
    - Tomcat被设计成一种轻量级容器，它只提供了最基本的Servlet和JSP容器功能，使其易于部署和使用。Tomcat还支持多种配置选项，可以根据特定需求进行自定义设置。
  - <font color='red'>跨平台支持</font>
    - Tomcat可以在多个操作系统上运行，包括Windows、Linux和Mac OS等。这使得开发人员和系统管理员能够选择适合他们环境的平台。
  - <font color='red'>高性能和可扩展性</font>
    - Tomcat通过使用Java的线程池技术和异步请求处理来提高性能。此外，Tomcat还提供了可扩展的插件机制，允许用户添加自定义组件和功能，以满足特定应用程序的需求。
  - <font color='red'>安全性</font>
    - Tomcat提供了一些安全特性，例如支持SSL/TLS加密协议、访问控制和认证机制，保护Web应用程序免受各种攻击和安全威胁。
  - <font color='red'>管理和监控工具</font>
    - Tomcat附带了一组管理和监控工具，使系统管理员能够轻松管理和监控Tomcat服务器，包括Web界面管理工具、日志记录和性能监控等。

## 实验拓扑

```mermaid
graph LR
A[真机客户端] --发送请求--> B[A主机Tomcat服务器]
```

虚拟机A安装JDK（tomcat的运行环境）

```shell
[root@som ~]# yum -y install java-1.8.0-openjdk			#安装java执行环境
```

### 安装Tomcat

- 将教学环境中的<font color='red'>som.tar.gz</font>压缩包上传至虚拟机A的/root/

![1688488866441](Day12.assets/1688488866441.png)

```shell
[root@som ~]# mkdir /root/som
[root@som ~]# tar -xf /root/som.tar.gz	-C /root/som	#解压缩 
[root@som ~]# cd /root/som
[root@som som]# tar -xf apache-tomcat-9.0.6.tar.gz		#解压缩tomcat
[root@som som]# mv apache-tomcat-9.0.6 /usr/local/tomcat
[root@som som]# ls /usr/local/tomcat/ 
bin/			//主程序目录
lib/			//库文件目录
logs/			//日志目录  
temp/			//临时目录
work/			//自动编译目录jsp代码转换servlet
conf/			//配置文件目录
webapps/		//页面目录
```

### 启动服务

```shell
[root@som som]# /usr/local/tomcat/bin/startup.sh 	#通过脚本启动服务
[root@som som]# ss -antlp | grep java				#过滤是否正常启动服务
```

tomcat启动之后，会启动三个端口，8005 8009 8080

<font color='red'>提示：</font>如果检查端口时，如果8005端口启动非常慢，默认tomcat启动需要从/dev/random读取大量的随机数据，默认该设备生成随机数据的速度很慢，可用使用下面的命令用urandom替换random<font color='red'>（非必须操作）</font>

```shell
[root@som ~]# mv /dev/random  /dev/random.bak
[root@som ~]# ln -s /dev/urandom  /dev/random				#生成新的random
```

### 客户端（真机）浏览访问页面测试

- 打开真机浏览器，访问自己虚拟机A的IP地址:8080，例如(192.168.8.100:8080)

![1688489344895](Day12.assets/1688489344895.png)

# Tomcat虚拟主机

配置服务器虚拟主机

修改server.xml配置文件，创建虚拟主机

```shell
[root@som ~]# yum -y install java-1.8.0-openjdk-devel        #提供jar命令
[root@som ~]# cd
[root@som ~]# jar -cf log.war /var/log/
[root@som ~]# cp log.war  /usr/local/tomcat/webapps/
[root@som ~]# ls /usr/local/tomcat/webapps/  				#有log目录
```

```xml
[root@som ~]# vim /usr/local/tomcat/conf/server.xml
……
     <Host name="www.a.com"  appBase="a"
         unpackWARs="true" autoDeploy="true">     
  		#自动解压war包，autoDeploy="true"自动部署java项目，如果有改动
     </Host>
     
	<Host name="www.b.com"  appBase="b"
            unpackWARs="true" autoDeploy="true">
     </Host>

……
```

创建虚拟主机对应的页面根路径

```shell
[root@som ~]# mkdir -p /usr/local/tomcat/a/ROOT
[root@som ~]# mkdir -p /usr/local/tomcat/b/ROOT
[root@som ~]# echo "AAA" > /usr/local/tomcat/a/ROOT/index.html
[root@som ~]# echo "BBB" > /usr/local/tomcat/b/ROOT/index.html
```

重启Tomcat服务器

```shell
[root@som ~]# /usr/local/tomcat/bin/shutdown.sh 
[root@som ~]# /usr/local/tomcat/bin/startup.sh
```



客户端设置host文件，并浏览测试页面进行测试，修改hosts域名解析

Windows： C：\Windows\System32\drivers\etc\hosts     (<font color='red'>需要以管理员身份打开记事本</font>)

追加写入

```powershell
192.168.8.100 www.a.com  www.b.com
```

Windows测试访问

![img](file:///C:/Users/15790/AppData/Local/Temp/msohtmlclip1/01/clip_image003.png) ![img](Day12.assets/clip_image004.png)

改www.b.com网站的首页目录为base

使用docBase参数可以修改默认网站首页路径，docBase默认某个路径下面的ROOT是首页目录

```xml
[root@som ~]# vim /usr/local/tomcat/conf/server.xml
… …
    <Host name="www.b.com"  appBase="b"
          unpackWARs="true" autoDeploy="true">
        <Context path="" docBase="base" />
    </Host>
… …
```

```shell
[root@som ~]# mkdir /usr/local/tomcat/b/base
[root@som ~]# echo BASE > /usr/local/tomcat/b/base/index.html
```

重新启动tomcat

```shell
[root@som ~]# /usr/local/tomcat/bin/shutdown.sh 
[root@som ~]# /usr/local/tomcat/bin/startup.sh
```

测试查看页面是否正确

![img](Day12.assets/clip_image005.png)

 

当用户访问http://www.b.com:8080/test打开/myself/目录下的页面，实现<font color='red'>地址重写</font>

编写自定义页面

```shell
[root@som ~]# mkdir /myself
[root@som ~]# echo "myself" > /myself/index.html
```

修改配置文件实现页面跳转

```xml
[root@som ~]# vim /usr/local/tomcat/conf/server.xml
     <Host name="www.b.com"  appBase="b"
           unpackWARs="true" autoDeploy="true">
         <Context path="" docBase="base" />
         <Context path="/test" docBase="/myself/" />      
     </Host>
```

重启服务

```shell
[root@som ~]# /usr/local/tomcat/bin/shutdown.sh 
[root@som ~]# /usr/local/tomcat/bin/startup.sh
```

浏览器访问、测试，出现的是自定义的页面

# NGINX服务

## web服务器对比

![1688505657658](Day12.assets/1688505657658.png)

## NGINX简介

![1688505629211](Day12.assets/1688505629211.png)

## 实验拓扑

```mermaid
graph LR
A[真机客户端] --发送请求--> B[A主机NGINX服务器]
```

## 虚拟机A源码编译安装NGINX

### 安装依赖包

```shell
[root@som ~]# yum  -y install gcc make pcre-devel openssl-devel
```

### 解压NGINX压缩包

- 采用<font color='red'>nginx-1.16.1.tar.gz</font>

```shell
[root@som ~]# cd som
[root@som som]# tar -xf nginx-1.16.1.tar.gz
[root@som som]# cd nginx-1.16.1
```

### 初始化

```shell
[root@som nginx-1.16.1]# ./configure --prefix=/usr/local/nginx --with-http_ssl_module
```

### 编译

```shell
[root@som nginx-1.16.1]# make
```

### 编译安装

```shell
[root@som nginx-1.16.1]# make install
```

### 查看验证

```shell
[root@som ~]# ls /usr/local/nginx/
conf sbin html logs
```

- conf: 专门用于存放NGINX配置文件
- sbin：专门用于存放程序文件
- html：专门用于存放网页文件
- logs：专门用于存放日志

## 服务管理

```shell
[root@som ~]# /usr/local/nginx/sbin/nginx				#启动服务
[root@som ~]# /usr/local/nginx/sbin/nginx -s reload		#重新加载配置
[root@som ~]# /usr/local/nginx/sbin/nginx -s stop		#停止服务
[root@som ~]# /usr/local/nginx/sbin/nginx -V			#查看NGINX版本信息
```

## 启动NGINX，访问测试

- NGINX服务默认监听80端口

```shell
[root@som ~]# /usr/local/nginx/sbin/nginx				#启动服务
```

- 真机打开浏览器访问：192.168.8.100

![1688506458878](Day12.assets/1688506458878.png)

# 升级Nginx

## 源码编译新版Nginx

```shell
[root@som nginx-1.16.1]# cd ..
[root@som som]# tar -xf nginx-1.17.6.tar.gz 
[root@som som]# cd nginx-1.17.6
[root@som nginx-1.17.6]# ./configure --with-http_ssl_module          #支持加密功能
[root@som nginx-1.17.6]# make                                        #编译
```

## 使用新版Nginx替换旧版Nginx

```shell
[root@som nginx-1.17.6]# mv /usr/local/nginx/sbin/nginx  /usr/local/nginx/sbin/nginx.old
[root@som nginx-1.17.6]# cp objs/nginx  /usr/local/nginx/sbin/
[root@som nginx-1.17.6]# make upgrade           	 	#升级或者手动重启Nginx服务
[root@som ~]# /usr/local/nginx/sbin/nginx -V			#查看NGINX版本信息
```

# 虚拟WEB主机

- 基于域名的虚拟主机

- 配置基于域名的虚拟主机，实现以下目标：

- 实现两个基于域名的虚拟主机，域名分别为som.tedu.cn和web2.tedu.cn 

修改Nginx服务配置，添加相关虚拟主机配置如下

```shell
[root@som ~]# vim /usr/local/nginx/conf/nginx.conf``
.. ..
  server {
      listen       80;             				   #端口
      server_name  som.tedu.cn;               	   #域名
 	location / { 
          root   web;                              #指定网站根路径
          index  index.html index.htm;
      }   
….
      }
```

```shell
… …
  server {
      listen       80;            				#端口
      server_name  web2.tedu.cn;                #域名
      location / { 
          root   html;                           #指定网站根路径
          index  index.html index.htm;
       }   

}
```

创建网站根目录及对应首页文件

```shell
[root@som nginx-1.17.6]# mkdir /usr/local/nginx/web
[root@som nginx-1.17.6]# echo  "web"  >  /usr/local/nginx/web/index.html
```

重新加载配置

```shell
[root@som nginx-1.17.6]#  /usr/local/nginx/sbin/nginx -s reload
```

修改Windows主机的hosts文件

Windows以管理员身份打开记事本

![img](Day12.assets/clip_image003-1688589625086.png)

![img](Day12.assets/clip_image005.jpg)

修改客户端IP和域名对应关系，IP地址不要照抄，以自己服务端的IP地址为准

```powershell
...
192.168.8.100  som.tedu.cn  web2.tedu.cn
```

用Windows的浏览器访问som.tedu.cn            web2.tedu.cn

# LNMP动态网站

- L: Linux操作系统
- N：NGINX服务器
- M：Mariadb、MySQL数据库
- P：php、python、perl语言所写的代码

## 环境准备

安装软件，部署nginx环境，在这之前的环境已经准备过nginx，这里不再安装

安装MariaDB，安装php解释器和php-fpmfastcgi接口，php连接数据库的扩展包

- mariadb-server: 提供数据库服务
- mairadb：提供数据管理命令
- mariadb-devel：数据库的扩展功能包
- php：php解释器
- php-fpm：管理php进程，快速网关通信接口，用于管理和处理PHP FastCGI进程的程序
- php-mysql：php连接数据库的扩展功能包

```shell
[root@som ~]# yum -y install mariadb mariadb-server mariadb-devel 
[root@som ~]# yum -y install php   php-mysql php-fpm
```

启动MySQL服务，设置开机自启

```shell
[root@som ~]# systemctl restart  mariadb           
[root@som ~]# systemctl enable mariadb       
```

启动PHP-FPM服务，设置开机自启

```shell
[root@som ~]# systemctl restart php-fpm 
[root@som ~]# systemctl enable php-fpm 
[root@som ~]# ss -utnlp | grep :80
[root@som ~]# ss -utnlp | grep :3306
[root@som ~]# ss -utnlp | grep :9000
```

## 构建LNMP平台（配置动静分离）

修改Nginx配置文件并启动服务

```shell
[root@som ~]# vim /usr/local/nginx/conf/nginx.conf 
server {

 location  ~  \.php$  {
        root           html;
        fastcgi_pass   127.0.0.1:9000;    	#将请求转发给本机9000端口，PHP解释器
        fastcgi_index  index.php;
        #fastcgi_param   SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi.conf;       	#加载其他配置文件
      }
[root@som ~]# /usr/local/nginx/sbin/nginx -s reload
```

创建PHP页面，测试LNMP架构能否解析PHP页面

创建PHP测试页面1：

```php
[root@som ~]# vim /usr/local/nginx/html/test.php
<?php
$i=33;
echo $i;
?>
```

测试访问http://som.tedu.cn/test.php，成功

创建PHP测试页面,连接并查询MariaDB数据库

```php
[root@som ~]# cp /root/som/php_scripts/mysql.php  /usr/local/nginx/html/
[root@som ~]# vim /usr/local/nginx/html/mysql.php
<?php
$mysqli = new mysqli('localhost','root','','mysql');
//注意：root为mysql数据库的账户名称，密码需要修改为实际mysql密码，无密码则留空即可
//localhost是数据库的域名或IP，mysql是数据库的名称
if (mysqli_connect_errno()){
   die('Unable to connect!'). mysqli_connect_error();
}
$sql = "select * from user";
$result = $mysqli->query($sql);
while($row = $result->fetch_array()){
    printf("Host:%s",$row[0]);
    printf("</br>");
    printf("Name:%s",$row[1]);
    printf("</br>");
}
?>
```

测试访问http://som.tedu.cn/mysql.php

# WordPress案例

- WordPress是一种开源的内容管理系统（CMS），用于创建和管理网站。它最初是作为一个用于博客网站的平台而开发的，但现在已经发展成一个功能强大的网站建设工具。

准备数据库，配置数据库账户与权限

为网站提前创建一个数据库、添加账户并设置该账户有数据库访问权限。

```sql
[root@som ~]# mysql
MariaDB [(none)]> create database wordpress character set utf8mb4;
MariaDB [(none)]> grant all on wordpress.* to wordpress@'localhost' identified by 'wordpress';
MariaDB [(none)]> grant all on wordpress.* to wordpress@'192.168.8.100' identified by 'wordpress';
MariaDB [(none)]> grant all on wordpress.* to wordpress@'%' identified by 'wordpress';
MariaDB [(none)]> flush privileges;                 #刷新权限
MariaDB [(none)]> exit
```

上线PHP动态网站代码

```shell
[root@som ~]# unzip /root/som/wordpress.zip
[root@som ~]# cd wordpress/
[root@som wordpress]# tar -xf wordpress-5.0.3-zh_CN.tar.gz 
[root@som wordpress]# cp -r wordpress/* /usr/local/nginx/html/
[root@som wordpress]# chown -R  apache:apache /usr/local/nginx/html/
#提示：动态网站运行过程中，php脚本需要对网站目录有读写权限，而php-fpm默认启动用户为apache。
```

初始化网站配置（访问som.tedu.cn）,发现显示的是nginx的默认首页，需要修改配置文件，更改首页

```shell
[root@som wordpress]# vim /usr/local/nginx/conf/nginx.conf
…
       location / {
           root   html;
           index  index.php index.html index.htm;
 …
[root@som wordpress]# /usr/local/nginx/sbin/nginx -s reload
```

再次访问som.tedu.cn，正常，第一次访问服务器会自动进入config配置页面

![img](Day12.assets/clip_image002-1688590676916.jpg)

开发人员在写代码的时候并不知道未来数据库服务器的IP、端口、数据库名称、账户等信息，该配置页面主要的作用就是动态配置数据库信息，根据前面步骤配置的数据库信息填空即可，效果如下

![1688590878445](Day12.assets/1688590878445.png)

点击提交即可完成数据库的初始化工作，php动态脚本会自动在wordpress数据库中创建若干数据表，后期网站的数据都会写入对并的数据表中

![img](Day12.assets/clip_image006.jpg)

![img](Day12.assets/clip_image008.jpg)

第一次使用Wordpress需要给你的网站设置基本信息，如网站标题、网站管理员账户与密码等信息，配置完成后点击安装wordpress即可

 

网站后台管理

访问192.168.2.100服务器，进入并熟悉后台管理界面（浏览器访问som.tedu.cn）

通常情况下，开发人员会开发一个后台管理界面，当代码上线后，普通用户就可以管理和配置网站页面（需要使用网站的超级管理员身份才可以进入后台界面）

访问首页后点击登陆菜单，输入账户和密码进入后台管理界面。

![img](Day12.assets/clip_image009.png)

![img](Day12.assets/clip_image011.jpg)

输入管理员用户名和密码。登陆后台管理界面。

# 总结

- 掌握yum工作原理
- 掌握NFS服务
- 掌握tomcat功能及使用
- 掌握源码编译安装
- 掌握NGINX服务基础
- 掌握NGINX平滑升级
- 掌握NGINX基于域名的虚拟web主机
- 掌握LNMP平台
- 掌握上线WordPress流程