[TOC]

# 实验环境

本即可需要一台<font color='red'>CentOS7.9的虚拟机</font>即可

# grep命令使用

- 功能：输出包含指定字符/匹配规则的行

- 格式： grep   '查找条件'  目标文件
- -i：查找是忽略关键字大小写
- -v：对查找结果去反（匹配不包含指定字符/不满足条件的行）
- ^关键字：匹配关键字开头的行
- 关键字$：匹配关键字结尾的行
- ^$：匹配空行

```shell
[root@som ~]# grep root /etc/passwd				#在/etc/passwd文件中匹配包含root的行
[root@som ~]# grep bash /etc/passwd				#在/etc/passwd文件中匹配包含bash的行
```

```shell
[root@som ~]# grep -i ROOT /etc/passwd			#忽略大小写匹配/etc/passwd文件中包含ROOT的行
[root@som ~]# grep -v root /etc/passwd			#在/etc/passwd文件中匹配不包含root的行
[root@som ~]# grep ^root /etc/passwd			#匹配/etc/passwd文件中以root开头的行
[root@som ~]# grep bash$ /etc/passwd			#匹配/etc/passwd文件中以bash结尾的行	
[root@som ~]# grep ^$ /etc/default/useradd		#匹配/etc/default/useradd中的空行
```

过滤/etc/default/useradd中的有效代码

- 有效代码不含注释（#开头）
- 有效代码不含空行

```shell
[root@som ~]# grep -v  ^# /etc/default/useradd  | grep -v  ^$
```

# vim文本编辑器

功能：使用vim文本编辑器可以<font color='red'>修改/创建</font>文件

- 若目标文件不存在，则新建空文件并编辑

- 若目标文件已存在，则打开此文件并编辑

三大模式：<font color='red'>命令模式</font>、<font color='blue'>插入模式</font>、<font color='green'>末行模式</font>

![1683179705800](Day04.assets/1683179705800.png)

```shell
[root@som ~]# vim /opt/haha.txt
AAAAAAAAAAAAAAA
BBBBBBBBBBBBBBB
CCCCCCCCCCCCCCC
```

按Esc键回到命令模式，按:（英文冒号）进入末行模式，输入wq，保存并退出

## 命令模式基本操作

### 光标跳转

![1683180373579](Day04.assets/1683180373579.png)

### 复制、粘贴、删除

![1683180423322](Day04.assets/1683180423322.png)

## 末行模式基本操作

### 存盘、退出、文件操作

![1683180515831](Day04.assets/1683180515831.png)

### 开关设置

![1683180620913](Day04.assets/1683180620913.png)

# Linux命令补充

## man帮助

- man命令可以帮助用户查找、浏览和使用操作系统中的命令、配置文件以及其他相关的程序和文档

- 支持上下键翻阅
- 支持PageUp、PageDown翻页
- 按/所有关键字
- 按q退出

```shell
[root@som ~]# man ls					#查看ls的帮助手册
[root@som ~]# man hier					#文件Linux系统层次结构标准（作用）
```

## 历史命令

- history：查看历史命令列表

- history  -c：清空历史命令

- !n：执行命令历史中的第n条命令

- !str：执行最近一次以str开头的历史命令

```shell
[root@som ~]# history 					#查看历史命令
[root@som ~]# history -c				#清空历史命令
```

```shell
[root@som ~]# cat /etc/shells			#查看/etc/shells文件
[root@som ~]# history					#查看历史命令
[root@som ~]# !1						#执行历史命令标号为1的指令
[root@som ~]# cat /etc/hosts			#查看/etc/hosts文件
[root@som ~]# !cat						#执行最近一次cat开头的指令
```

```shell
[root@som ~]# grep HISTSIZE /etc/profile		#/etc/profile中记录了历史命令可以记录多少条
HISTSIZE=1000
```

## du命令

- 作用：统计文件的占用空间
- du  [选项]...  [目录或文件]...
  - -s：只统计每个参数所占用的总空间大小
  - -h：提供易读容量单位（K、M等） 

```shell
[root@som ~]# du -sh /boot/ /etc/pki/			#统计/boot/和/etc/pki/占用磁盘空间大小
```

## date指令

- 作用：查看/修改系统时间
- date  +%F、date +%R
- date  +"%Y-%m-%d %H:%M:%S"
- 修改时间格式：date  -s  "yyyy-mm-dd  HH:MM:SS" ，<font color='red'>不可将时间修改为1970-01-01 00:00:00之前</font>

```shell
[root@som ~]# date								#查看当前系统时间
[root@som ~]# date +"%F"						#查看年-月-日
[root@som ~]# date +"%R"						#查看时:分:秒
```

将时间修改为: '2008-08-08 20:08:08'

```shell
[root@som ~]# date -s '2008-08-08 08:08:08'	#修改时间为'2008-08-08 08:08:08'
```

# 归档及压缩

归档的含义：

- 将许多零散的文件整理为一个文件
- 文件总的大小基本不变

压缩的含义：

- 按某种算法减小文件所占用空间的大小
- 恢复时按对应的逆向算法解压

| 压缩格式 | 扩展名 |          特点          |
| :------: | :----: | :--------------------: |
|   gzip   |  .gz   |   速度快，压缩比例低   |
|  bzip2   |  .bz2  | 速度中等，压缩比例中等 |
|    xz    |  .xz   |   速度慢，压缩比例高   |

tar命令工具

- -c:创建归档
- -x:释放归档
- -f:指定归档文件名称
- -z、-j、-J:调用 .gz、.bz2、.xz 格式的工具进行处理
- -t:显示归档中的文件清单
- -C(大写):指定释放的位置
- 注：<font color='red'>f选项必须放在所有选项的最后 </font>

## 制作压缩包

- 制作压缩包又被称之为打包
- 格式：tar    选项   <font color='red'>/路径/压缩包名字</font>    <font color='blue'> 被压缩归档的源数据1   被压缩归档的源数据2 ...</font>

将/boot/目录和/home/目录打包至/opt/

```shell
[root@som ~]# rm -rf /opt/*			#删除/opt/里边的所有数据（不是必须，只是为了方便查看）
[root@som ~]# tar -zcf /opt/two.tar.gz /boot/ /home/	#使用gzip格式
[root@som ~]# tar -jcf /opt/two.tar.bz2 /boot/ /home/	#使用bzip2格式
[root@som ~]# tar -Jcf /opt/two.tar.xz /boot/ /home/	#使用xz格式
```

## 释放压缩包

- 释放压缩包有被称之为解包
- 格式：tar    选项   <font color='red'>/路径/压缩包名字</font>    <font color='blue'> [-C]   释放路径</font>

将/opt/two.tar.gz释放到/opt下

```shell
[root@som ~]# tar -zxf /opt/two.tar.gz -C /opt/			#将压缩包释放至/opt
[root@som ~]# cd /opt									#切换至/opt目录
[root@som opt]# tar -zxf /opt/two.tar.gz -C .			#.表示当前路径
[root@som opt]# tar -zxf /opt/two.tar.gz				#不指定释放目录默认为当前所在位置 
```

## zip压缩包

- zip是一种跨平台的压缩格式，即在Linux操作系统的zip格式压缩包传递至Windows操作系统任然可用。

### 制作zip格式压缩包(打包)

- zip  [-r]   备份文件.zip   被归档的文档1  被归档的文档2  ... 

将/etc/selinux打包至/root/selinux.zip

```shell
[root@som ~]# zip -r /root/selinux.zip /etc/selinux/ 
[root@som ~]# du -sh /root/selinux.zip /etc/selinux/ 	#查看压缩包和源数据的大小
4.0M	/root/selinux.zip
9.6M	/etc/selinux/
```

### 释放zip格式压缩包(解包)

- 格式：unzip  备份文件.zip  [-d 目标文件夹]  



```shell
[root@som ~]# unzip /root/selinux.zip -d /opt/			#将/root/selinux.zip解压至/opt/
```

# 总结

- 掌握grep命令及选项使用
  - -i、-v、^关键字、关键字$、^$
- 掌握vim文本编辑器的使用（命令模式、插入模式、末行模式）
- 掌握常用命令：history、date、du...
- 掌握Linux常见压缩包格式
  - .gz、bzip2、xz、zip