#!/bin/bash
#部署LNMP环境
#部署NGINX.
echo -e "\033[32m正在安装NGINX，请稍等...\033[0m"
yum -y install gcc pcre-devel openssl-devel make
mkdir /root/som
tar -xf /root/som.tar.gz -C /root/som
cd /root/som
tar -xf nginx-1.16.1.tar.gz
cd nginx-1.16.1
./configure --with-http_ssl_module
make && make install
#部署mariadb数据库
echo -e "\033[32m正在安装mariadb，请稍等...\033[0m"
yum -y install mariadb-server mariadb-devel maridb

#部署php-程序
echo -e "\033[32m正在安装php稍等...\033[0m"
yum -y install php php-mysql php-fpm
#运行服务
/usr/local/nginx/sbin/nginx
systemctl restart mariadb
systemctl enable  mariadb
systemctl restart php-fpm 
systemctl enable php-fpm

#修改NGINX配置
nginxfile="/usr/local/nginx/conf/nginx.conf"
sed  -i '65,71 s/#//' $nginxfile
sed  -i '69s/^./#/' $nginxfile
sed -i '70s/fastcgi_params/fastcgi\.conf/' $nginxfile
/usr/local/nginx/sbin/nginx -s reload
