[TOC]
# 环境准备

远程管理linux主机

- 环境准备如下方表格(主机名IP)
- 使用CentOS7.9克隆一台新的机器

| 主机名        | IP地址           |
| ------------- | ---------------- |
| som.tedu.cn   | 192.168.4.7/24   |
| pc207.tedu.cn | 192.168.4.207/24 |

将虚拟机A、B的网络模式选择为vmnet1

![1685424285001](Day09.assets/1685424285001.png)

真机网络适配器中将vmnet1 IP地址设置为192.168.4.254/24

![1685424383138](Day09.assets/1685424383138.png)

![1685424520360](Day09.assets/1685424520360.png)

虚拟机A关闭firewalld服务并且设置为开机不自启，将SElinux设置为disabled模式

```shell
[root@som ~]# systemctl stop firewalld					#停止firewalld服务
[root@som ~]# systemctl disable firewalld				#禁止firealld开机自启
```

```shell
[root@som ~]# vim /etc/selinux/config					#禁止firealld开机自启
SELINUX=disabled
[root@som ~]# reboot									#重启生效
```

选择虚拟机CentOS7.9（模板机），右击 管理—>克隆 ，如果之前已经客隆过虚拟机B，可以不用重复克隆

![1685423765211](Day09.assets/1685423765211.png)



![1685423809603](Day09.assets/1685423809603.png)

![1685423842119](Day09.assets/1685423842119.png)

![1685423910430](Day09.assets/1685423910430.png)

![1685423925037](Day09.assets/1685423925037.png)

之后使用root用户分别登陆A 和 B 主机

- 虚拟机A
  - 主机名：som.tedu.cn
  - IP地址：192.168.4.7/24
- 虚拟机B
  - 在主机名：pc207.tedu.cn
  - IP地址：192.168.4.207

A主机更改主机名、IP地址（网卡名不要照抄）

```shell
[root@som ~]# hostnamectl set-hostname som.tedu.cn			#修改过就不用改了
[root@som ~]# nmcli connection modify  eth0  ipv4.method manual ipv4.addresses 192.168.4.7/24 connection.autoconnect yes
[root@som ~]# nmcli connection up eth0  
```

B主机更改主机名、IP地址（网卡名不要照抄）

```shell
[root@som ~]# hostnamectl set-hostname pc207.tedu.cn
[root@pc207 ~]# nmcli connection modify  eth0  ipv4.method manual ipv4.addresses 192.168.4.207/24 connection.autoconnect yes
[root@pc207 ~]# nmcli connection up eth0  
[root@pc207 ~]# ping 192.168.4.7          						#可以ping通
```

#  ssh远程管理

- SSH协议，Secure Shell
  - 为客户及提供安全的Shell环境
  - 默认端口：TCP 22
- OpenSSH服务
  - 服务名：sshd
  - 主程序: /usr/bin/sshd、/usr/bin/ssh
  - 服务端配置文件: /etc/ssh/sshd_config
  - 客户端配置文件: /etc/ssh/ssh_config

- ssh 命令选项 
  - -p  端口：连接到指定的端口
  - -X  启用-X转发，在本机运行对方的图形程序

  虚拟机A远程操作虚拟机B

```shell
[root@som ~]# ssh root@192.168.4.207
…
Are you sure you want to continue connecting (yes/no)? yes        #第一次远程会让输入yes
root@192.168.4.207's password:                                    #输入密码（不显示）
Last login: Wed Jul  8 11:24:37 2020
[root@pc207 ~]# firefox                          			#运行firefox图形程序，失败
Error: GDK_BACKEND does not match available displays
```

退出，使用-X选项登陆，成功

```shell
[root@pc207 ~]# exit
[root@som ~]# ssh -X root@192.168.4.207
root@192.168.4.207's password: 
[root@pc207 ~]# firefox
```

修改默认端口，pc207主机操作

```shell
[root@pc207 ~]# vim /etc/ssh/sshd_config
Port 8022											#去掉#注释，将端口号改为8022
[root@pc207 ~]# systemctl restart sshd         
#重启sshd服务，切记selinux需要是宽松或者禁止状态setenforce 0
```

测试：som主机操作（如果测试失败，出现以下问题，请关闭防火墙，当然前方环境准备中已关闭防火墙）

```shell
[root@som ~]# ssh -p 8022 -X root@192.168.4.207     
ssh: connect to host 192.168.4.207 port 8022: No route to host
[root@som ~]# systemctl stop firewalld
[root@som ~]# ssh -p 8022 -X root@192.168.4.207     #测试
root@192.168.4.207's password: 
[root@pc207 ~]#
```

恢复端口，直接注释更改的文件，重启服务即可

```shell
[root@pc207 ~]# vim /etc/ssh/sshd_config			 #编辑配置文件 
# Port 8022
[root@pc207 ~]# systemctl restart sshd				 #重启服务
```

- scp基于ssh远程管理，安全复制工具scp
  - scp  /路径/源数据（本地文件）       root@对方IP地址:/路径/
  - scp   root@对方IP地址:/路径/       /路径/源数据（本地文件）

把本机的boot目录放到207主机的opt下面

```shell
[root@som ~]#  scp -r  /boot/  root@192.168.4.207:/opt/    
```

把远程主机的目录boot放到本地opt下

```shell
[root@som ~]#  scp -r  root@192.168.4.207:/boot    /opt/  
[root@som ~]#  ls  /opt/
```

# 远程管理进阶

- 实现ssh无密码验证

- 生成公钥与私钥，完成ssh无密码验证

生成公钥与私钥

```shell
[root@som ~]# ssh-keygen     						#一路回车
[root@som ~]# ls /root/.ssh/						#查看密钥
```

将公钥传递给对方

```shell
[root@som ~]# ssh-copy-id  root@192.168.4.207
root@192.168.4.207's password:           			#输入密码
[root@som ~]# ssh root@192.168.4.207				#验证
```

# MobaXterm远程工具

- 在教学环境中解压MobaXterm软件即可使用，同时群文件也给了一个汉化版

![1688050211008](Day09.assets/1688050211008.png)
