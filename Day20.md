[TOC]

# 环境准备

- IP地址采用自动分配，以自己的为准

- 可以将之前的mysql主机的主机名改名为mysql-a

- 重新安装mysql-b主机，配置基础环境

| 主机名  | IP地址        |
| ------- | ------------- |
| mysql-a | 192.168.8.100 |
| mysql-b | 192.168.8.101 |

为两台主机配置IP地址和指定案例的yum源，下方已mysql-b主机为例

```shell
[root@mysql ~]# hostnamectl set-hostname mysql-b
[root@mysql-b ~]# systemctl stop firewalld 				#关闭防火墙
[root@mysql-b ~]# systemctl disable firewalld			#禁止防火墙开机自启
[root@mysql-b ~]# vim /etc/selinux/config				#修改配置文件，关闭SELinux
...
SELINUX=disabled
...
[root@mysql-b ~]# reboot								#重启生效
[root@mysql-b ~]# nmcli connection modify ens33 ipv4.method auto connection.autoconnect yes
[root@mysql-b ~]# nmcli connection up ens33
```

配置yum，使用阿里云的镜像站点

```shell
[root@mysql-b ~]# rm -rf /etc/yum.repos.d/*.repo	 	#删除自带的repo文件
[root@mysql-b ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     #下载阿里镜像源
```

# 数据备份

## 为什么要备份

数据是企业生存的命脉

## 什么是备份

将数据另外保存一份

## 备份到哪里

通常采用异地保存

## 什么时候备份

备份的窗口期，通常是业务压力最低点

## 如何备份

- 备份方法
  - 物理备份
  - 逻辑备份
- 备份策略
  - 完整备份
  - 增量备份
  - 差异备份
- 备份三要素
  - BW：完成备份需要的时间
  - RPO：客户可承受的最大数据丢失量
  - RTO：客户可承受的最长停机时间

# 完整备份

## 物理备份

方法：cp、tar、zip ...

```shell
##物理备份及恢复测试：使用cp、tar、zip等命令对数据库磁盘文件进行备份

#mysql-a操作

[root@mysql-a ~]# systemctl stop mysqld						#停止MySQL服务
[root@mysql-a ~]# mkdir /bak								#创建备份文件存储目录
[root@mysql-a ~]# tar -zcPf /bak/db.tar.gz /var/lib/mysql/*	#对MySQL磁盘文件打包
[root@mysql-a ~]# ls /bak/									#确认备份成功
db.tar.gz

[root@mysql-a ~]# rm -rf /var/lib/mysql/*					#删除MySQL磁盘文件模拟数据丢失
[root@mysql-a ~]# tar -zxPf /bak/db.tar.gz -C /				#解压备份数据，恢复数据
[root@mysql-a ~]# ls /var/lib/mysql/						#确认数据目录下有文件
[root@mysql-a ~]# chown -R mysql.mysql /var/lib/mysql/			#修改文件归属！！！

[root@mysql-a ~]# systemctl start mysqld						#启动服务测试数据

[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW DATABASES;"
```

## 逻辑备份

- mysqldump命令用于备份数据
- mysql命令用于恢复数据

```mysql
##逻辑备份及恢复测试

#备份指定库下的多个表
[root@mysql-a ~]# mysqldump -hlocalhost -uroot -p'tedu123...A' tarena departments employees salary > /bak/des.sql

#备份多个指定库下的所有表
[root@mysql-a ~]# mysqldump -hlocalhost -uroot -p'tedu123...A' -B tarena execdb > /bak/te.sql

#备份所有库下的表
[root@mysql-a ~]# mysqldump -hlocalhost -uroot -p'tedu123...A' -A > /bak/all.sql

[root@mysql-a ~]# ls /bak/*.sql			#确认数据备份成功
/bak/all.sql  /bak/des.sql  /bak/te.sql

##测试恢复指定库下的表

#删除tarena库下的表模拟数据丢失，注意顺序不能改
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "DROP TABLE tarena.salary; DROP TABLE tarena.employees; DROP TABLE tarena.departments;"

#确认departments表、employees表和salary表被删除
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW TABLES FROM tarena;"

#利用des.sql恢复数据
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' tarena < /bak/des.sql 

#确认3张表恢复成功
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW TABLES FROM tarena;"

##测试恢复多个库

#删除tarena库和execdb库模拟数据丢失
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "DROP DATABASE tarena; DROP DATABASE execdb;"

#确认tarena库和execdb库被删除
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW DATABASES;"

#利用te.sql恢复两个库
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' < /bak/te.sql 

#确认两个库恢复成功
[root@mysql-a ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW DATABASES;"
```

## 测试恢复所有库

配置基础环境

```shell
[root@localhost ~]# hostnamectl set-hostname mysql
[root@mysql ~]# nmcli connection modify ens33 ipv4.method auto connection.autoconnect yes
[root@mysql ~]# nmcli connection up ens33
```

配置yum，使用阿里云的镜像站点

```shell
[root@localhost ~]# rm -rf /etc/yum.repos.d/*.repo	 	#删除自带的repo文件
[root@localhost ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     #下载阿里镜像源
```

# 构建MySQL服务

将学习环境中的：<font color='red'>C-4软件运维\05_医疗项目\HIS医疗项目\mysql8-centos7</font>上传到虚拟机mysql-b的/root/

mysql主机操作

```shell
[root@mysql-b ~]# cd  /root/mysql8-centos7
[root@mysql-b ~]# yum -y  localinstall *.rpm				#安装mysql
[root@mysql-b ~]# systemctl   start      mysqld     		#启动服务
[root@mysql-b ~]# systemctl   enable   mysqld    			#开机运行
[root@mysql-b ~]# ss  -utnlp  |  grep  :3306  			#查看服务信息
```

修改初始密码

```shell
[root@mysql-b mysql8-centos7]# grep -i password /var/log/mysqld.log	#过滤root初始密码
2023-08-26T14:22:39.197619Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: 4afdh9OY6&Ef
```

```shell
mysql> ALTER USER root@"localhost" IDENTIFIED BY "123qqq...A";  	 #修改登陆密码
[root@mysql ~]# mysql -hlocalhost  -uroot  -p'123qqq...A'	         #使用新密码登陆
```



```shell
##测试恢复所有库(注意含有mysql-b操作)

#发送all.sql到mysql-b
[root@mysql-a ~]# scp /bak/all.sql 192.168.8.101:/root	

#确认mysql-b上只有初始库
[root@mysql-b ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"

#确认all.sql存在
[root@mysql-b ~]# ls /root/all.sql 

#恢复所有库
[root@mysql-b ~]# mysql -hlocalhost -uroot -p'123qqq...A' < all.sql 

#确认库恢复情况
[root@mysql-b ~]# mysql -hlocalhost -uroot -p'123qqq...A' -e "SHOW DATABASES;"

#小问题处理(此时依旧使用旧密码登录，由于服务没重启导致还没加载新的mysql.user文件)
[root@mysql-b ~]# systemctl restart mysqld
[root@mysql-b ~]# mysql -hlocalhost -uroot -p'tedu123...A' -e "SHOW DATABASES;"
```

# xtrabackup完全备份与恢复

- xtrabackup一款强大的在线热备份工具
- 备份过程中不锁库表，适合生产环境
- 由专业组织Percona提供（改进MySQL分支）

mysql-a主机操作，安装percona-xtrabackup（libev是依赖包）

```shell
[root@mysql-a ~]# yum -y install libev
[root@mysql-a ~]# yum install -y https://downloads.percona.com/downloads/Percona-XtraBackup-8.0/Percona-XtraBackup-8.0.33-27/binary/redhat/7/x86_64/percona-xtrabackup-80-8.0.33-27.1.el7.x86_64.rpm
```

## 完全备份

- 命令格式

  - xtrabackup  --backup  --user=用户名    --password=密码  --databases="库名" 

    --target-dir=备份文件的存储目录

- 如有报错请执行：OPTIMIZE  TABLE 库名.表名;

在mysql-a主机将所有数据库进行备份

```shell
[root@mysql-a ~]# mkdir /db_all
[root@mysql-a ~]# xtrabackup --backup --user=root --password="tedu123...A"  \
--target-dir=/db_all
```

将备份目录拷贝至mysql-b

```shell
[root@mysql-a ~]# scp -r /db_all root@192.168.8.101:/
```

## 完全恢复

步骤如下

1、systemctl   stop  mysqld       					#停止数据库服务

2、rm  -rf  /var/lib/mysql/*      						#清空数据库目录

3、xtrabackup --prepare --target-dir=/备份目录  		#准备恢复数据

4、xtrabackup --copy-back --target-dir=/备份目录     	#恢复数据  

5、chown -R  mysql:mysql  /var/lib/mysql     			#修改所有者和组

6、systemctl   start   mysqld     						#启动服务

在mysql-b主机安装软件包

```shell
[root@mysql-b ~]# yum -y install libev
[root@mysql-b ~]# yum install -y https://downloads.percona.com/downloads/Percona-XtraBackup-8.0/Percona-XtraBackup-8.0.33-27/binary/redhat/7/x86_64/percona-xtrabackup-80-8.0.33-27.1.el7.x86_64.rpm
```

恢复数据

```shell
[root@mysql-b ~]# rm -rf /var/lib/mysql/*							#清空数据库目录
[root@mysql-b ~]# xtrabackup --prepare --target-dir=/db_all			#准备恢复数据
[root@mysql-b ~]# xtrabackup --copy-back --target-dir=/db_all		#恢复数据
[root@mysql-b ~]# chown -R mysql:mysql /var/lib/mysql
```

启动服务验证

```shell
[root@mysql-b ~]# systemctl restart mysqld
[root@mysql-b ~]# mysql -uroot -ptedu123...A
mysql> SHOW DATABASES;								#数据已回复
```

## 增量备份

- 增量备份：备份上次备份后，新产生的数据。
  - 增量备份时，必须先有一次备份,通常是完全备份
  - 例如：周一完全备份 ， 周二~周日增量备份

![1693102435231](Day20.assets/1693102435231.png)

- 增量备份格式

  - xtrabackup --backup --user=用户名  --password=密码  --target-dir=/增量备份目录  \ 

    --incremental-basedir=/上一次备份目录  

mysql-a新增数据

```sql
mysql> CREATE DATABASE game;						#创建库
mysql> CREATE TABLE game.t1(id INT,name CHAR(10));	#创建表
mysql> INSERT INTO game.t1 VALUES(1, "zhangsan");	#插入数据
```

mysql-a增量备份

```shell
[root@mysql-a ~]# mkdir /db_firstinc							#创建备份目录
[root@mysql-a ~]# xtrabackup --backup  --user=root --password='tedu123...A' \
--target-dir=/db_firstinc --incremental-basedir=/db_all			#增量备份
[root@mysql-a ~]# scp -r /db_firstinc/ root@192.168.8.101:/		#将增量备份拷贝至备份服务器
```

## 增量恢复

1. xtrabackup --prepare --apply-log-only --target-dir=目标目录 --user=用户名 --password=密码                                             		   #将最新的全量备份恢复到指定的目录
2. xtrabackup --prepare --apply-log-only --target-dir=目标目录 --user=用户名 --password=密码 \

​     --incremental-dir=增量备份目录1                #指的是第一个增量备份文件所在的目录

1. systemctl  stop mysqld
2. rm  -rf  /var/lib/mysql/*
3. xtrabackup --prepare --target-dir=目标目录 --user=root --password=密码  
4. xtrabackup --copy-back --target-dir=目标目录 --user=用户名 --password=密码  //恢复数据
5. chown -R  mysql:mysql  /var/lib/mysql/
6. systemctl  start mysqld 

mysql-b主机恢复操作

```shell
[root@mysql-b ~]# xtrabackup --prepare --apply-log-only --target-dir=/db_all \
--user=root --password='tedu123...A'   #将最新的全量备份恢复到指定的目录

[root@mysql-b ~]# xtrabackup --prepare --apply-log-only --target-dir=目标目录 \
--user=root --password='tedu123...A' --incremental-dir=/db_firstinc
#将增量备份数据恢复到指定的目录
#--incremental-dir=/db_firstinc指的是第一个增量备份文件所在的目录
```

```shell
[root@mysql-b ~]# systemctl  stop mysqld
[root@mysql-b ~]# rm  -rf  /var/lib/mysql/*
```

```shell
[root@mysql-b ~]# xtrabackup --prepare --target-dir=/db_all \
--user=root --password='tedu123...A'									#准备恢复数据
[root@mysql-b ~]# xtrabackup --copy-back --target-dir=/db_all 
--user=root --password='tedu123...A'									#恢复数据

[root@mysql-b ~]# chown -R  mysql:mysql  /var/lib/mysql/
[root@mysql-b ~]# systemctl  start mysqld
```

# 总结

- 掌握数据库备份的重要性
- 掌握物理备份、热备的方法
- 掌握数据库备份策略

