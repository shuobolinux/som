[TOC]

# 磁盘空间管理

磁盘空间管理

扇区默认512字节

##  一块硬盘的“艺术”之旅

- 识别硬盘 => 分区规划 => 格式化 => 挂载使用

- 毛坯楼层 => 打隔断  => 装修  => 入驻 

## 识别磁盘

关机为虚拟机CentOS7.9添加一块大小为10G的磁盘

```shell
[root@som ~]# poweroff
```

![1687779455939](Day06.assets/1687779455939.png)

 

![img](Day06.assets/clip_image003.png)

![img](Day06.assets/clip_image004.png)

![img](Day06.assets/clip_image005.png)

![img](Day06.assets/clip_image006.png)

 

![img](Day06.assets/clip_image007.png)

最后点击确定，开启虚拟机

查看识别的硬盘

```shell
[root@som ~]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   80G  0 disk 
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   79G  0 part 
  ├─centos-root 253:0    0   50G  0 lvm  /
  ├─centos-swap 253:1    0    2G  0 lvm  [SWAP]
  └─centos-home 253:2    0   27G  0 lvm  /home
sdb               8:16   0   10G  0 disk
```

## 分区规划

### 分区模式

#### MBR(主启动记录模式)

- 三种分区类型：主分区  扩展分区  逻辑分区
- 1~4个主分区，或者3个主分区+1个扩展分区(n个逻辑分区)
- 最多只能有4个主分区，扩展分区最多只能有一个，且扩展分区不能之间存储数据
- 逻辑分区可以是无限个，必须建立在扩展分区之上
- 最大支持容量为 2.2TB 的磁盘
- 扩展分区不能格式化

#### GPT分区

- 全局唯一标识分区表
- 突破固定大小64字节的分区表限制
- 最多可支持128个主分区，最大支持18EB磁盘
- 1EB=1024PB=1024*1024TB

### 使用GPT分区方案分区

```shell
[root@som ~]# parted  /dev/sdb 				#对/dev/sdb进行分区
GNU Parted 3.2
使用 /dev/sdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) mktable gpt								#指定分区表为gpt分区方案
(parted) mkpart 									#创建分区
分区名称？  []? test									 #自定义
文件系统类型？  [ext2]? ext2						    #自定义
起始点？ 0 						
结束点？ 2G
警告: The resulting partition is not properly aligned for best
performance: 34s % 2048s != 0s
忽略/Ignore/放弃/Cancel? Ignore						  #忽略
(parted) mkpart 									 #创建分区
分区名称？  []? test2								 #自定义
文件系统类型？  [ext2]? ext2						    #自定义
起始点？ 2G						
结束点？ 4G
(parted)quit										#退出（会自动保存）
```

## 格式化与挂载使用

### 格式化

mkfs 工具集

- mkfs.ext3 分区设备路径
- mkfs.ext4 分区设备路径
- mkfs.xfs  分区设备路径
- mkfs.vfat -F 32 分区设备路径

 ```shell
[root@som ~]# mkfs.ext4 /dev/sdb1  							#格式化分区为ext4
[root@som ~]# blkid  /dev/sdb1          						#查看分区文件系统类型
/dev/sdb1: UUID="b986a067-752a-4690-a881-28f5ee5c6d39" TYPE="ext4"  #UUID是设备的唯一标识
 ```

```shell
[root@som ~]# mkfs.xfs /dev/sdb2
[root@som ~]# blkid  /dev/sdb2
/dev/sdb2: UUID="53383c80-522b-401f-a914-3da12942dfd8" TYPE="xfs"
```

### 挂载使用

挂载第一块分区sdb1

```shell
[root@som ~]# mkdir /mypart1							#创建挂载点
[root@som ~]# mount /dev/sdb1  /mypart1					#将/dev/sdb1挂载至/mypart1
[root@som ~]# df -h                 					#查看正在挂载设备的使用情况
文件系统                 容量  已用  可用 已用% 挂载点
/dev/sdb1                2.0G  6.0M  1.8G    1% /mypart1
```

验证写入数据

```shell
[root@som ~]# cp /etc/passwd /mypart1/p.txt				#拷贝数据
[root@som ~]# cat /mypart1/p.txt
```

挂载第二块分区sdb2

```shell
[root@som ~]# mkdir /mypart2							#创建挂载点
[root@som ~]# mount /dev/sdb2 /mypart2					#挂载
[root@som ~]# df -h
文件系统                 容量   已用  可用 已用% 挂载点
/dev/sdb2              2.0G   33M  2.0G  2%  /mypart2
```

验证写入数据

```shell
[root@som ~]# cp /etc/passwd /mypart2/s.txt
[root@som ~]# ls /mypart2/
s.txt
```

## 实现开机自动挂载

- /etc/fstab是专门用于配置设备开机自动挂载的文件
- 该文件每行有六个字段：<font color='red'>设备路径</font>  <font color='orange'>挂载点</font>  <font color=green>类型</font>  <font color='turquoise'>参数</font>  <font color='blue'>备份标记</font>   <font color='purple'>检测顺序</font> 
- 修改完之后系统不会实时更新挂载配置，需要重启系统或者mount -a手动刷新

### 实验

将/dev/sdb1使用/etc/fstab永久挂载至/mypart1目录

将/dev/sdb2使用/etc/fstab永久挂载至/mypart2目录

注意：如果不记得设备是什么文件系统，可以使用 blkid  设备名，进行查看

```shell
[root@som ~]# vim /etc/fstab
...此处省略一万字...
/dev/sdb1 /mypart1 ext4 	defaults  0  0
/dev/sdb2 /mypart2 xfs    	defaults  0  0

[root@som ~]# mount  -a						#刷新/etc/fstab
[root@som ~]# df -h							#查看挂载情况
```

## 光盘挂载

- 光盘也是一种常用的物理设备，需要通过挂载才能看到光盘文件中的数据
- 光盘默认的文件系统类型为：iso9660

临时挂载至/mydvd

```shell
[root@som ~]# mkdir /mydvd
[root@som ~]# mount /dev/cdrom /mydvd
```

永久挂载至/mydvd

```shell
[root@som ~]# vim /etc/fstab
...此处省略一万字...
/dev/sdb1 /mypart1 ext4 	defaults  0  0
/dev/sdb2 /mypart1 xfs    	defaults  0  0
/dev/cdrom /mydvd iso9660   defaults  0  0
[root@som ~]# mount  -a						#刷新/etc/fstab
[root@som ~]# df -h							#查看挂载情况
```

## 交换空间

- 交换空间是一种计算机系统中存储器的扩展技术，用于缓解物理内存不足的情况
- 填充方式
  - 硬盘/分区空间
  - 大文件

# 总结

- 一块磁盘的艺术之旅
  - 识别磁盘
  - 分区
  - 格式化
  - 挂载使用
- 掌握永久挂载、掌握挂载光盘
- 了解交换空间

 

 