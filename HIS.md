# 医疗项目简介

![1689195707797](HIS.assets/1689195707797.png)

![1689195725792](HIS.assets/1689195725792.png)

![1689195752336](HIS.assets/1689195752336.png)

![1689195767842](HIS.assets/1689195767842.png)

![1689195784006](HIS.assets/1689195784006.png)



# 内存设置为4G或以上

![1689173301757](HIS.assets/1689173301757.png)

![1689173328499](HIS.assets/1689173328499.png)

# 部署NGINX服务

- 负责动静分离
  - 静态页面交给网页根目录
  - 动态页面交给SpringBoot

- 可以作为web服务，访问服务器本地静态文件
- 可以作为反向代理，向后台其他服务转发访问

![1689204249065](HIS.assets/1689204249065.png)



部署安装NGINX服务

从环境资料中把nginx文件夹上传到his主机的/root目录

```shell
[root@his ~]# cd nginx/
[root@his nginx]# cd nginx-dependency/
[root@his nginx-dependency]# yum -y localinstall *.rpm
```

```shell
[root@his nginx-dependency]# cd ..
[root@his nginx]# tar -xf nginx-1.24.0.tar.gz
[root@his nginx]# cd nginx-1.24.0/
[root@his nginx-1.24.0]# ./configure --prefix=/usr/local/nginx \
--with-http_ssl_module --with-pcre				#--with-pcre表示支持正则表达式
[root@his nginx-1.24.0]# make 
[root@his nginx-1.24.0]# make install
```

```shell
[root@his nginx]# /usr/local/nginx/sbin/nginx	#启动NGINX服务
[root@his nginx]# echo "/usr/local/nginx/sbin/nginx"  >> /etc/rc.d/rc.local #设置开机自启
[root@his nginx]# chmod +x /etc/rc.d/rc.local 	
```

# 部署web

- 部署静态网站
- 将课前资料<font color='red'> project/his/his-web.zip</font> 上传到his主机的 /root/

修改NGINX配置，支持动静分离

```shell
[root@his ~]# vim /usr/local/nginx/conf/nginx.conf
#在倒数第二行前添加如下配置
server {
	listen 80;
    server_name www.his.cn;
    index index.html;
    
    location / {								#匹配静态网站
        root html;								#静态网站网页根目录
    }
    location /prod-api/ {						#匹配动态网站
        proxy_pass http://192.168.8.104:8888/;  #将请求转发给后端服务器（IP地址不能照抄）
    }
}
```

部署静态网页

```shell
[root@his ~]# mkdir -p /opt/his/web
[root@his ~]# unzip ~/his-web.zip -d /opt/his/web/		#将静态网页代码释放到/opt/his/web
[root@his ~]# mv /opt/his/web/dist/* /usr/local/nginx/html #将网页代码移动至网页根目录(覆盖)
```

重新加载服务

```shell
[root@his ~]# /usr/local/nginx/sbin/nginx -t  				#检查配置语法
[root@his ~]# /usr/local/nginx/sbin/nginx -s reload			#重新加载配置
```

访问验证，在windows中以<font color='red'>管理员的身份</font>向文件`C:\Windows\System32\drivers\etc\hosts`追加域名映射：

```shell
...此处省略1万字...
192.168.8.104  www.his.cn
```

真机打开浏览器访问http://www.his.cn，目前这个位置因为没有配置动态网页所以点不了登录

![1691633173011](HIS.assets/1691633173011.png)



# 安装JDK

- 因为动态网站是java语言写的，所以需要安装java运行环境、
- 将资料环境中的<font color='red'>openjdk8</font>文件夹上传至his主机的root/

安装JDK，即Java运行环境

```shell
[root@his ~]# cd /root/openjdk8/
[root@his openjdk8]# yum -y localinstall *.rpm	
```

为了使用方便，使用jre目录的软链接路径，配置JDK的环境变量

```shell
[root@his openjdk8]# vim /etc/profile
...此处省略1万字...
在文件末尾添加如下内容
export JAVA_HOME=/etc/alternatives/jre
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.
```

刷新配置文件，使用上方配置生效

```shell
[root@his openjdk8]# source /etc/profile
```

测试

```shell
[root@his openjdk8]# java -version
openjdk version "1.8.0_372"
OpenJDK Runtime Environment (build 1.8.0_372-b07)
OpenJDK 64-Bit Server VM (build 25.372-b07, mixed mode)
```

# 部署Elasticsearch

- 在该项目中主要用于检索

- 将<font color='red'>elasticsearch</font>上传至his主机的root目录
- 默认elasticsearch监听9200、9300端口号

安装 Elasticsearch

```shell
[root@his ~]# cd elasticsearch
[root@his elasticsearch]# yum -y localinstall *.rpm
```

启动 Elasticsearch 服务并设置为开机自启动

```shell
[root@his ~]# systemctl start elasticsearch
[root@his ~]# systemctl enable elasticsearch
```

检查是否安装成功，如果有下方输出则代表成功

```shell
[root@his ~]# curl -X GET "http://localhost:9200/"
{
  "name" : "his",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "ID_M5dflQ4SoFiY8XPpRFw",
  "version" : {
    "number" : "7.17.7",
    "build_flavor" : "default",
    "build_type" : "rpm",
    "build_hash" : "78dcaaa8cee33438b91eca7f5c7f56a70fec9e80",
    "build_date" : "2022-10-17T15:29:54.167373105Z",
    "build_snapshot" : false,
    "lucene_version" : "8.11.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

## 安装ik中文分词器

在elasticsearch中安装ik分词器，使用elasticsearch的插件安装

```shell
[root@his elasticsearch]# mv elasticsearch-analysis-ik-7.17.7.zip /root/
[root@his ~]# /usr/share/elasticsearch/bin/elasticsearch-plugin install file:///root/elasticsearch-analysis-ik-7.17.7.zip
```

查看插件列表

```shell
[root@his ~]# /usr/share/elasticsearch/bin/elasticsearch-plugin list
```

测试ik分词器

- standard，es的标准分词器，中文按一个字一个字拆分，英文按单词
- ik_smart，分词比较少
- ik_max_word，尽可能多的中文词

```shell
[root@his ~]# systemctl restart elasticsearch	#重启服务
[root@his ~]# curl -H "Content-Type: application/json" -XPOST http://localhost:9200/_analyze?pretty -d '
{
"analyzer": "standard",
"text": "爆华为Mate60系列将支持5G,或于今年9月份发布"
}'

#将-d后边的字符串提交至地址
#-X指定POST请求
#-H告诉服务器向服务器提交的数据格式是json格式
```

```shell

[root@his ~]# curl -H "Content-Type: application/json" -XPOST http://localhost:9200/_analyze?pretty -d '
{
"analyzer": "ik_smart",
"text": "爆华为Mate60系列将支持5G,或于今年9月份发布"
}'
#将-d后边的字符串提交至地址
#-X指定POST请求
#-H告诉服务器向服务器提交的数据格式是json格式
```

```shell
[root@his ~]# curl -H "Content-Type: application/json" -XPOST http://local	host:9200/_analyze?pretty -d '
{
"analyzer": "ik_max_word",
"text": "爆华为Mate60系列将支持5G,或于今年9月份发布"
}'
#将-d后边的字符串提交至地址
#-X指定POST请求
#-H告诉服务器向服务器提交的数据格式是json格式
```

# 部署rabbitmq

- rabbitmq在本项目中,主要用于优化写操作,让写入数据进入消息队列,慢慢写入MySQL

- 将环境中的<font color='red'>rabbitmq</font>上传至his主机的root目录
- rabbittmq是erlang语言开发的，所以需要安装erlang的语言包，在环境中
- rabbitmq的管理界面默认端口：15672
- rabbitmq的默认端口：5672

```shell
[root@his ~]# cd rabbitmq/
[root@his rabbitmq]# yum -y localinstall *.rpm
```

配置rabbitmq的登录管理员

```shell
[root@his ~]# vim /etc/rabbitmq/rabbitmq.conf
default_user=guest			#用户名
default_pass=guest			#密码
loopback_users=none			#禁止任何机器访问RabbitMQ
```

启动rabbitmq

```shell
[root@his ~]# systemctl start rabbitmq-server
[root@his ~]# systemctl enable rabbitmq-server
```

启用管理界面插件（也可以使用命令行管理）

```shell
[root@his ~]# rabbitmq-plugins enable rabbitmq_management		#启用管理界面插件
[root@his ~]# systemctl restart rabbitmq-server					#重启服务
```

创建/his虚拟主机

```shell
[root@his ~]# rabbitmqctl add_vhost /his				#创建新的虚拟主机
[root@his ~]# rabbitmqctl add_user admin hisadmin		
							#添加管理员为admin，密码hisadmin，这个用户在HIS医疗后端系统里边使用
[root@his ~]# rabbitmqctl set_permissions -p /his admin ".*" ".*" ".*"  
							#允许admin用户访问/his虚拟主机，并且拥有所有权限
```

浏览器访问http://192.168.8.104:15672，使用用户名和密码guest登录。

使用图形的web管理页面也可以，也可以使用命令行



# 部署MySQL服务

## 安装MySQL服务

- MySQL在本项目中主要负责持久化存储数据,医疗产生的所有数据

- 将环境中的<font color='red'>mysql8-centos7</font>文件夹上传到/root

```shell
[root@his ~]# cd mysql8-centos7/
[root@his mysql8-centos7]# yum -y localinstall *.rpm			#安装MySQL
```

启动mysqld服务，修改root用户密码

- 与Mariadb不同，mysqld启动之后root会产生一个随机密码，这个密码放在/var/log/mysqld.log


```shell
[root@his ~]# systemctl enable mysqld				#将服务设置为开机自启
[root@his ~]# systemctl start mysqld				#启动服务
```

查看数据库随机密码

```shell
[root@his ~]# cat /var/log/mysqld.log | grep -i password	# 查看root管理员临时登录密码
2023-07-12T16:55:22.055303Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: d6;e0nLo72<l

# 登录MySQL，提示输入密码时使用上面得到的临时密码，不要照抄，每个人都是随机的密码
[root@his ~]# mysql -uroot -p'上面生成的密码'
```

修改数据库密码

```shell
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY '123qqq...A';	# 第一次修改成复杂密码
mysql> set global validate_password.policy=LOW;		#设置为低策略
mysql> set global validate_password.length=4;		#最小密码长度设置为4
mysql> set global validate_password.check_user_name=OFF;#将用户名检查关闭，密码可以使用用户名
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';	#将root用户密码改为root
mysql> EXIT;										#退出数据库
```

重新登录

```shell
[root@his ~]#  mysql -uroot -proot
```

## 建库、授权用户

创建his库，创建his用户，并为用户授权

- 未来动态页面所产生的数据全部写入his这个库里
- 后端动态页面使用his用户连接数据库（<font color='red'>用户：his，密码：'hisadmin'</font>）
- mysql8版本中，需要先创建用户，再为用户授权

```sql
mysql> CREATE DATABASE his;
mysql> CREATE USER 'his'@'%' IDENTIFIED BY 'hisadmin';
mysql> GRANT ALL ON his.* TO 'his'@'%';
mysql> EXIT;
```

## 导入数据

- 将环境资料中的project目录中的his文件夹到/root/

```shell
[root@his ~]# cd /root/his/
[root@his his]# mysql -uroot -proot his < his.sql 
```

验证

```sql
[root@his ~]# mysql -uroot -p'root'		#连接数据库
mysql> USE his; 						#切换至his库
mysql> SHOW TABLES;						#查看有哪些表
```

# 部署Redis

- Redis在本项目中负责缓存读取数据,加速用户访问的速度

- 将环境资料中的<font color='red'>redis文件夹</font>上传至his主机的/root目录

```shell
[root@his ~]# cd redis/
[root@his redis]# yum -y localinstall *.rpm			#安装redis
```

修改配置文件，需要配置redis密码，因为后端的HIS系统中配置文件里写了需要redis的密码为hisadmin

```shell
[root@his redis]# vim /etc/redis/redis.conf
...此处省略1万字...
1036 requirepass hisadmin				#去掉注释顶格写，把密码改为hisadmin
...此处省略1万字...
```

启动服务并设置为开机自启

```shell
[root@his redis]# systemctl start redis
[root@his redis]# systemctl enable redis
```

## 测试Redis

```sql
[root@his redis]# redis-cli					#连接redis数据库
127.0.0.1:6379> AUTH hisadmin				#输入密码，AUTH认证的意思
OK
127.0.0.1:6379> PING						#ping如果返回PONG则代表服务正常
PONG
127.0.0.1:6379> SET name sam				#这是name变量的值为sam
OK
127.0.0.1:6379> GET name					#获取name变量的值
"sam"
127.0.0.1:6379> EXIT						#退出Redis数据库
```

# 部署后端项目

- SpringBoot主要负责处理后端动态页面	

- 后端his项目的配置是通过application.yml和application-prod.yml文件启动运行
- 这两个文件中主要定义了各个组件之间的调用关系（已经在开发的时候全部记录好了，开发的事）
- application.yml资料环境中project/his文件夹中，这个his文件夹在上边已经上传过了
- <font color='red'>将程老师修改过的application-prod.yml单独上传至/root</font>/

准备启动环境

- <font color='red'>注意application-prod.yml不要使用his文件夹中的,要使用我单独给的</font>

```shell
[root@his redis]# mkdir -p /opt/his/log
[root@his redis]# chmod 777 /opt/his/log
[root@his redis]# mkdir -p /opt/his/api
[root@his redis]# cp /root/his/HIS-api-1.0-SNAPSHOT.jar /opt/his/api/
[root@his redis]# cp /root/his/application.yml /opt/his/api/
[root@his ~]# cp application-prod.yml /opt/his/api/
```

启动服务（指定两个yml文件启动）部署过程需要耐心等待...

```shell
[root@his redis]# cd /opt/his/api/
[root@his redis]# nohup java -jar HIS-api-1.0-SNAPSHOT.jar > ./his.log 2>&1 &
```

真机测试访问前端项目：http://www.his.cn

使用用户名"<font color='red'>管理员</font>"和密码"<font color='red'>test</font>"登录

# HIS医疗系统

## 数据表

1. bms_bills_record：记录了患者就诊的账单信息，包括账单号、病历号、就诊时间、挂号费、药品费、手术费等。
2. bms_invoice_exp：记录了患者的发票费用信息，包括发票号、收费项目名称、费用等。
3. bms_invoice_record：记录了患者的发票信息，包括发票号、开票日期、操作员、发票金额等。
4. bms_operator_settle_record：记录了医生或药师结算信息，包括结算日期、医生姓名、结算金额、结算状态等。
5. bms_settlement_cat：记录了医疗费用结算类别，包括个人结算、混合结算、医保结算等。
6. dms_case_history：记录了病人的病历信息，包括病历号、医生姓名、就诊时间、临床表现等。
7. dms_case_model：记录了医生制定的病例模板，包括模板名称、创建时间、创建者等。
8. dms_case_model_catalog：记录了病例模板的分类目录，包括分类名称、分类描述、上级分类等。
9. dms_dise：记录了疾病信息，包括疾病名称、诊断标准、症状描述等。
10. dms_dise_catalog：记录了疾病的分类目录，包括分类名称、分类描述、上级分类等。
11. dms_dosage：记录了药品的使用剂量信息，包括药品名称、剂量单位、是否常用等。
12. dms_drug：记录了药品信息，包括药品名称、剂型、规格、生产厂家等。
13. dms_drug_model：记录了医生制定的用药模板，包括模板名称、创建时间、创建者等。
14. dms_drug_refund_item_record：记录了药品退费记录，包括退费时间、退费数量、退费原因等。
15. dms_herbal_item_record：记录了患者中药颗粒使用记录，包括用药时间、用药数量、用药剂量等。
16. dms_herbal_model_item：记录了医生制定的中药颗粒使用模板，包括模板名称、创建时间、创建者等。
17. dms_herbal_prescription_record：记录了中药颗粒处方记录，包括处方编号、病历号、就诊时间等。
18. dms_medicine_item_record：记录了患者服用药品的使用记录，包括用药时间、用药剂量、用药方式等。
19. dms_medicine_model_item：记录了医生制定的药品使用模板，包括模板名称、创建时间、创建者等。
20. dms_medicine_prescription_record：记录了药品处方记录，包括处方编号、病历号、就诊时间等。
21. dms_non_drug：记录了非药品医疗服务信息，包括服务名称、服务费用、服务介绍等。
22. dms_non_drug_item_record：记录了患者非药品医疗服务使用记录，包括服务名称、使用时间、使用费用等。
23. dms_non_drug_model：记录了医生制定的非药品医疗服务使用模板，包括模板名称、创建时间、创建者等。
24. dms_registration：记录了病人的挂号信息，包括挂号号码、挂号类型、预约时间等。
25. pms_patient：记录了病人个人信息，包括病历号、姓名、性别、身份证号、出生日期等。
26. sms_dept：记录了医院科室信息，包括科室名称、科室介绍、科室电话等。
27. sms_description：记录了医院各项服务的详细描述信息。
28. sms_frequent_used：记录了常用的医疗服务、药品等信息，用于方便快捷的使用。
29. sms_login_log：记录了用户的登录日志信息，包括登录时间、用户名称、登录IP等。
30. sms_permission：记录了用户权限信息，包括权限名称、权限描述等。
31. sms_registration_rank：记录了医生的挂号等级信息，包括等级名称、等级费用等。
32. sms_role：记录了用户角色信息，包括角色名称、角色描述等。
33. sms_role_permission_relation：记录了角色和权限之间的关系。
34. sms_skd：记录了医生的排班信息，包括排班日期、排班时间、科室名称等。
35. sms_skd_rule：记录了排班规则，包括规则名称、规则描述、适用时间等。
36. sms_skd_rule_item：记录了排班规则中的时间段信息，包括时间段名称、开始时间、结束时间等。
37. sms_staff：记录了医院员工基本信息，包括员工编号、员工姓名、员工性别等。
38. sms_workload_record：记录了医生的工作量信息，包括医生姓名、就诊科室、就诊时间段等。

## 挂号收费模块

管理员登录，创建<font color='red'>收费员1</font>账户，密码设置为<font color='red'>1234</font>

![1689189508244](HIS.assets/1689189508244.png)

![1689189625877](HIS.assets/1689189625877.png)

使用收费员1登录

![1689189669728](HIS.assets/1689189669728.png)

![1689189702130](HIS.assets/1689189702130.png)

<font color='red'>更换回管理员用户--->选择Back to home</font>

## 门诊医生模块

添加医生1账户,密码为:1234

![1689189916574](HIS.assets/1689189916574.png)

使用医生1登录

![1689189942887](HIS.assets/1689189942887.png)

医生1有专属的工作台,与收费员不一样

![1689189957133](HIS.assets/1689189957133.png)

<font color='red'>更换回管理员用户--->选择Back to home</font>

## 医技医生模块

添加医技医生1(化验检查X光盘这类医生),密码为:1234

![1689190173723](HIS.assets/1689190173723.png)

使用医技医生1登录

![1689190241773](HIS.assets/1689190241773.png)

![1689190304997](HIS.assets/1689190304997.png)

<font color='red'>更换回管理员用户--->选择Back to home</font>

## 药房医生模块

创建药房医生1,登录密码为:1234 

![1689190451185](HIS.assets/1689190451185.png)

切换药房医生1登录

![1689190530148](HIS.assets/1689190530148.png)

<font color='red'>更换回管理员用户--->选择Back to home</font>



# HIS系统模块

医院信息管理系统

针对中小医院业务量相对少，人员计算机水平不高，信息化建设相对落后等特点，我公司专门针对以上医院开发了一套全新的HIS软件本系统专有特点：

1、远程维护
极大的解决了医院系统维护能力不足的问题，可以适时为医院处理问题。
2、Windows标准操作
软件设计符合卫生部标准，只要会操作电脑就会使用系统
3、实施周期短
软件可操作性强、易理解度高、掌握度高，基础数据收集便捷中小医院一周即可完成实施
4、药房药库、物资和设备进行统一管理
为医院药品、物资和设备的灵活管理提供了方便
5、科室、病区通过设置角色来确定
可以根据需要灵活的设置科室角色（即一个科室多种功能）方便医院进行管理。
6、自定义报表
对于计算机水平比较高的医院，可以使用自定义报表工具设计自己需要的报表。
7、费用数据先到财务
改变传统出纳被动收缴费用模式，保护了费用的安全，防治收款员作弊。
8、药品自助调动
当一个药房药品不足时可以立即选择别的药房，患者就可以到指定药房领取药。
9、药房病人用头像区分
药房工作人员可以非常直观的知道领药人的来源，便于区分病人。
10、院长查询简易化
决策层领导关注重点突出操作简易，使用笔记本在全球漫游随时监测医院动态
11、内部通讯QQ
这是一个内部交流平台，医务人员之间可以自由交流如同使用其它IM聊天软件。
12、短信、邮件、网站
结合web2.0建立互动信息平台，定期自动发布关怀、优惠信息



软件具体功能介绍

一、门诊挂号管理
建立完整的流程化门诊就诊系统，快速的门诊挂号录入，平均挂号需要时间不到30秒，完全遏止了不挂号、跑号、漏号的行为，同时遵循ICD10标准为门诊电子病理提供了一个完善的解决方案：
1、实现分类别挂号（初诊号、复诊号、普通号、专家号等），分科挂号、选医生挂号。
2、可接纳医保患者的挂号。建立门诊病案。
3、实现挂号费、诊疗费的分科、分医生核算。
4、与门诊收费、门（急）诊医生工作站数据共享。确保挂号费、诊疗费的收入。
5、向财务处输送二级科目的财务报表。
6、挂号员每日打出自己的日结对帐单向财务交费。
7、挂号速度快、平均20-30秒/人（次）。
8、与统计室数据共享，输送科室医生分项核算。
9、向院长输送本科室供院长查询的各类信息。
由于医院门诊挂号是病人就医活动的起点。减少病人挂号排队时间，方便病人挂号是服务宗旨。同时，要利于提高服务质量和工作效率，减轻医院工作人员的劳动强度。因此，本软件充分考虑了上述要求。


二、门诊病区管理
1、 提供了药品的即时查询，让门诊医生完全掌握目前药库药品动态，管理员可以对于零库存药品的是否显示进行设置，做到了不退药不缺药。
2、 庞大的快捷套餐模板系统，随时可以增加任何组合的药品+器材或项目+药品+器材的各种组合。
3、 患者可以根据门诊医生的录入第一时间了解任何项目与药品的价格，杜绝了患者因为不了解总的费用而在门诊收款时进行退、减项目的事情发生。
4、 系统自动建立全院门诊医生“常用项目列表”，医生就不必多次重复录入，只需要对着常用列表里的项目进行简单的双击就完成了项目的选定。
简单快捷的门诊医生工作站病史记录、处方、检查、检验、治疗、处置、手术、收入等全部医疗过程的计算机处理、存储和查询，可同时方便的接诊和处理多个病人。采用多种病历模板，提高医生的工作效率。 提供多种对病人的处理方式，方便病人就诊。支持门诊挂号，医生挂号，预约挂号，支持医技科室对病人的灵活处理。
5、 完成门诊医生对门诊病人的提供对门诊病人当前和既往门诊各种信息（病历）的查询、统计功能。
可打印门诊病人病历、处方和检查检验单，及诊断证明。
6、 可与门诊收费、门诊药房、医技科室（PACS、LIS）联网实现门诊费用自动核算，医疗信息自动传递，医学图像和检查检验报告的共享查阅。


三、门诊结算管理
1、 流程化的结算方式，患者只需用门诊挂号单上的门诊一号通，就可以快速的进行有选择性的费用结算。
2、 系统内置多种结算方式：
A、 收费模式 B、挂号收费模式 C、划价收费模式 D、医生工作站模式 E、其他扩展模式
以上的任何模式可以即时选择设定，也可以根据医院的需求设置多种模式混合的复合结算模式。
3、 管理严密，多方面堵塞漏洞。收据执行票据管理，杜绝利用票据作弊；退款完全按财务制度履行，防止退费中的舞弊行为；与挂号和门诊药房数据共享；可制止收费过程中的各种漏费弊端。
4、 当日结帐、当日交款，方便财务科管理使医院资金及时周转。
5、 提供二级科目的财务与分科、分医生分项核算的报表。节约结帐核算的人力与物力的投入，而且解决了多年来门诊收入分科、分项核算的难点，真正达到高效低耗、工效挂钩、节余提奖核算办法的实施。
6、 系统一次性收费录入，进行多方核算、提高了数据处理的及时性和准确性。提供各方面管理信息和详细、准确的资料。
7、 实现收费套餐、灵活制定打折比例。
8、 医院管理人员及时提供准确的收费及工作量信息，为行政工作提供诸多方便。 挂号与收费数据共享，可减少患者等候时间，提高医院服务质量。
9、 收费窗口双显示，面向患者既有显示应交款、实交款、应找款又有到几号窗口取药等信息。实现与门诊管理系统的数据共享，可确保门诊收入的统一，又能减少患者的等候时间，提高了医院的服务质量。
10、 完善的医保接口预置，对于市政、矿区、林区、军政、企业等种形式医保结算提供完美无缝连接。

四、住院登记管理
1、 标准的国际ICD10疾病编码及病案号管理，快捷的简码录入，成熟的“一号通”登记方式。人性化的更改、编辑患者登记信息，完美的配合病案管理。
2、 目前国际最简单的押金输入方式，在录入数字的同时以中文大写的汉字出现方便查看与校对。
3、 优秀的费用预警机制，可灵活的设置每个患者的费用预警线，也可以统一设置所有患者的费用预警，绝对解决了令医院头痛的住院患者费用管理，完全杜绝了跑费现象。
4、 严明的住院登记方式，要求患者必须先在住院病区进行出院登记后再根据患者的住院号到出院结算处进行结算，符合国际的流程化结算方式，提高管理效率极大的降低了管理成本。
5、  接收医保患者持卡交费。查询打印患者住院费用、总账、明细帐、预交押金等。支持不同类型患者单项费用结算，尤其是专科医院的特殊要求。
6、  患者费用结算，建立患者明细台帐。对医保患者，按当地医保收费标准进行动态的费用结算。进行日、月、年的帐务处理；日结自动计算床位费、护理费等。
7、  与财务数据共享，向财务输送二级科目财务报表。与统计室数据共享，输送科室医生分项核算。可进行出院结算；执行病人出院后的再召回；自动兑帐、催款和追加押金。
8、 查询、打印患者任意时间段费用明细，向患者查询系统输送患者花费明细信息。向院长输送本科室供院长查询的各类信息。

五、住院病区管理
1、 患者费用统计：预交款、总金额、自负金额、余额显示一目了然，患者费用及日清单在线打印。
2、 临床业务录入：医技术申请、长期医嘱、临时医嘱、体温单、护理记录及病程记录完全支持模板的导入，套餐的使用，简码查询录入等功能。
3、 完善的病案首页，手术登记、疾病诊断、及多次就诊统计，完全符合卫生部门的检查管理。
4、 住院病区手术、转科、会诊等处理科学流程化，极大的方便病区管理。
5、 接收患者入院信息，修改入院病历首页。支持护士医嘱审核,生成医嘱执行档案,从共享数据库中查询患者相关资料,支持患者生命体征等临床信息录入,支持患者转科调床,修改患者病历首页 ,提供病人,病床以及各种工作日志和统计报表,动态提供对医保病人的费用管理。
6、 接收医生工作站长期医嘱、临时医嘱，进行医嘱审核、确认和执行。
7、 按照病历号、姓名、床位号查询、打印患者医嘱、用药、检查项目以及执行情况等信息。具有办理患者出院、待出院、召回、病区转科调床功能、能够全程处理患者医嘱、生成、查询、打印患者医嘱单。
8、 动态地对患者的费用进行查询、执行计算。通过费用报警线、停止线实现患者费用的控制。向中心药房、煎药室、医技科室输出执行医嘱信息。向住院收费处发送执行医嘱信息，及时与之核对、上帐，及时对帐并反馈患者押金余额信息，给打印每日清单，对欠费患者及时催款。
9、 跟踪患者所在床位、护理级别、疾病类别信息。
10、向患者查询系统提供医嘱明细，给患者提供一日清单。查询打印本病区病人入出院统计，生成病房动态日报表。查询、打印患者任意时间段的费用明细。
11、向管理者(住院部主任、院长等)提供本病区相关查询数据。如：床位周转率、治疗率等。
12、可以进行本系统的多种组合查询，打出无限多的自由组合报表。

六、医技科室管理
1、 检查、检验、手术、其他医技分类授权管理，拥有最大的医技科室共性特性达到一模块解决所有医技科室管理。
2、 未做项目、已做项目进行分类浏览同时填写检查、检验结果，同时将报告结果地时间反馈给临床，互动、快捷，让临床医生及时掌握检查、检验结果。
3、 手术受理根据患者的门诊号、住院号进行快速检索登记，并按照国际标准格式进行手术编码分类，同时支持模板、套餐快速录入，对于门诊、住院患者进行分类管理。
4、 与门诊、住院工作站数据向连接提供准确结果及报告，同时与病案管理系统接口连接，可以直接用于病案统计，为学术研究等提供准确数据。

七、药库药房管理
1、 严格遵循国家现行的法律、法规和有关部门的具体规定的医院财务制度，医药分开核算等，作到“金额管理、数量统计、实耗实销”原则。
2、 药价：利用不同的检索码录入药品名称（支持别名），自动显示药品信息（规格、单位、价格、库存量、有效期等）。
3、 窗口发药：调出已收费的处方，并与手工处方核对无误后发药。
4、 药品入库：根据库存药品上、下限辅助自动生成向药库领药的领药单，传输到药库，药库实发数后打印出库单双方签字传回药房上帐。
5、库存管理：药品批次、药品报增损、药品借调、药品退药库、库存锁定、药品停用管理。对药品的期初、期间出、入库、期末结余的数量金额进行管理，对特殊药品、药品有效期进行管理。自动盘点生成盘点表和报増报损清单。报増报损清单经领导批示后更改库存。
6、 统计报表：药品消耗明细及汇总；各科室及医生用药明细及汇总；特殊药品的发药明细及汇总；本药房及发药人的工作量统计。
7、 药剂科药库管理系统是整个医院的药品管理中心，完成了药品从购入到发售的全面跟踪，能为管理人员提供科学化管理所需的准确、详尽的数据信息：掌握药品单位、厂家、追踪批准文号及生产许可证，避免伪劣药品入院，提供库存信息，了解当前低储、高储、积压和畅销药品等。该系统与门诊药房管理系统、住院处药房管理系统、院长查询系统和患者查询系统等模块实现数据共享，使它们之间便于协调统一。

八、后勤物资管理
1、院外入库信息的登记、院内入库登记、物品出库登记。
2、分级别审核制度，订货人与审批复核人分开，入库人与入库复核分开，科学缜密管理杜绝假帐、错帐、漏帐。
3、库存管理：库存盘存，盘存汇总统计、库存报增报损、库存调价、批次管理。
4、科室使用管理：科室权属变更、科室库存查询、报废。
5、查询统计：入库查询统计、出库查询统计、盘存查询、物品库存汇总、报增报损查询、物品调价查询、物品有效期查询、物品在位情况查询、物品在位情况汇总、物品权属变更查询、物品报废查询、物品编码查询。报表打印：入库单、出库单、增损单、报废单、转账通知单、对转账通知单、盘存报表、库存报表、物品在位情况报表、领用报表、出入库报表、超期清单、物品编码表的打印等。
6、将系统与查询分析器、综合查询及院长查询内部连接，为科室核算时后勤物资支出提供数据，方便经营部门核算，同时方便管理层领导在线查询并与目前库存进行对比，保证了后勤帐务的透明化，公开化，监管化。

九、经营核算管理
1、 基于网络的经济核算可以从不同渠道产生系列化的统计数据，每天可由门诊、住院、病区、财务、人事等子系统直接汇总生成统计数据，可大大地减少统计部门搜集、汇总、统计的工作量。当用户增加某一明细项目时，只需通过系统管理功能对相应数据进行修改，无需修改程序，赛优HIS提供灵活的报表打印格式，方便管理。
2、 门急诊信息统计：自动采集门诊、急诊各站点工作量信息、费用信息，可按任意时间进行统计（包括跨年统计），可以按不同财务核算科目核算，也可按操作员、科室、及全院情况进行分级统计核算。
3、药房药库信息统计：不仅可以统计药品进销存情况，而且对药库药房毒、麻、贵重药品允许单独统计核对。可以准确统计现存药品的调价盈亏情况。
4、住院部信息统计：自动采集住院部与病区发生的各类财务与医疗信息，统计时间无限制，不仅可按各科室、操作员及全院进行核算，也可以按财务的不同科目核算。
5、 工资奖金核算统计：可以按照医师与操作员的工作量的情况依当地医院的工资奖金核算标准，统计每人工资奖金额度，方便财务核算工作。

十、院长综合查询
1、 院长通过本系统可以实时查询住院病人、费用、病床使用信息，门诊挂号、收费信息，药品信息等网络上已经采用的系统的各种数据和统计报表，并作出提示以辅助院长决策。
2、 可以进行办公事务管理，如工作计划自动提示，文档管理，通讯录管理等。 实现对医院信息全面管理，联网实时查询住院病人、费用、病床使用信息查询。
3、 门诊挂号、收费信息查询；药库购入、出库、库存信息查询；药房处方、库存信息查询；药品使用信息在线查询、动态监控：单品种药品流量分析、医生与药品流量关系分析；病案统计信息查询；设备物资出入库、消耗、库存信息查询。
4、 医院财务管理分析、统计、收支执行情况和科室核算分配信息；重要仪器设备使用效率和完好率信息；后勤保障物资供应情况和经济核算；医务、护理管理质量和分析信息；人事工资信息查询；科室设置、重点学科、医疗水平有关决策信息。
5、 院长可以通过本系统在科室核算收入、科室工作量，同期数据对照，指定时期内全院经营对比等全面掌握医院第一手经营数据，配合医院行政管理及各项政策出台。
6、 本系统支持无线网络漫游，院长在全球任何一个可以登陆互联网的国家或地区只需要将移动笔记本接入网络通过我们系统的128位加密网络传输信息即对医院实施在线管理，与在医院本地一样的方便，本功能深受各大医院院长欢迎。

# 总结

- 掌握HIS医疗平台项目架构
- 掌握各个组件的作用
- 了解项目背景
- 了解项目需求
- 总结自己再该项目中的职责



