[TOC]

# Oracle数据简介

Oracle Database，又名Oracle RDBMS，或简称Oracle。是甲骨文公司的一款关系数据库管理系统。它是在数据库领域一直处于领先地位的产品。可以说Oracle数据库系统是世界上流行的关系数据库管理系统，系统可移植性好、使用方便、功能强，适用于各类大、中、小微机环境。它是一种高效率的、可靠性好的、适应高吞吐量的数据库方案。

# 环境准备

使用CentOS7.9模板机克隆一个新的名为Oracle虚拟机，要求如下

- 内存至少4096M（4G）
- 硬盘空间至少20G
- 交换空间至少2048M （2G）

虚拟机配置基础环境，修改主机名，分配IP地址

```shell
[root@localhost ~]# hostnamectl set-hostname oracle
[root@oracle ~]# nmcli connection modify ens33 ipv4.method auto connection.autoconnect yes 
[root@oracle ~]# nmcli connection up  ens33
```

查看虚拟机交换空间大小，默认2G，所以仅查看即可

```shell
[root@oracle ~]# grep SwapTotal /proc/meminfo 
SwapTotal:       2097148 kB
```

Oracle默认不支持CentOS，将系统标识改为<font color='red'>redhat-7</font>

```shell
[root@oracle ~]# echo 'redhat-7' > /etc/redhat-release		#修改系统标识为redhat-7
[root@oracle ~]# cat /etc/redhat-release					#查看验证
redhat-7
```

修改 /etc/sysctl.conf配置，调整系统内核参数以满足Oracle数据库的要求。

每个参数具体作用如下（了解）：

- fs.aio-max-nr：用于指定系统异步IO的最大请求数量。
- fs.file-max：用于指定系统可以打开的文件描述符的最大数量。
- kernel.shmmni：用于指定系统中共享内存标识符的最大数量。
- kernel.sem：用于设置信号量的参数，包括信号量数组最大值、单个信号量集中的最大信号量数、按需分配的信号量数组最大值、防止出现死锁的SEMMSL参数。
- kernel.shmall：用于指定系统中所有共享内存段总共占用的物理页框的数量。
- kernel.shmmax：用于指定系统中单个共享内存段的最大大小。
- net.ipv4.ip_local_port_range：用于指定系统分配本地端口的范围。
- net.ipv4.icmp_echo_ignore_broadcasts：用于禁止系统回应广播ICMP echo请求。
- net.ipv4.conf.all.rp_filter：用于开启反向路径过滤。
- net.core.rmem_default：用于指定接收缓冲区的默认大小。
- net.core.rmem_max：用于指定接收缓冲区的最大大小。
- net.core.wmem_default：用于指定发送缓冲区的默认大小。
- net.core.wmem_max：用于指定发送缓冲区的最大大小。

```shell
[root@oracle ~]# vim /etc/sysctl.conf
...此处省略一万字...在最后一行下方追加即可
fs.aio-max-nr = 1048576
fs.file-max = 6815744
kernel.shmmni = 4096
kernel.sem = 250 32000 100 128
kernel.shmall = 2097152
kernel.shmmax = 2147483648
net.ipv4.ip_local_port_range = 9000 65500
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.conf.all.rp_filter = 1
net.core.rmem_default = 262144
net.core.rmem_max= 4194304
net.core.wmem_default= 262144
net.core.wmem_max= 1048576
```

刷新生效

```shell
[root@oracle ~]# sysctl -p
```

创建oracle用户和用户组

```shell
[root@oracle ~]# groupadd oinstall					#创建安装oracle程序用户组
[root@oracle ~]# groupadd dba						#创建DBA用户组
[root@oracle ~]# useradd -g dba -m oracle			#创建用户oracle 并加入到dba组
[root@oracle ~]# usermod -a -G oinstall oracle		#将用户oracle加入到oinstall组
[root@oracle ~]# id oracle 							#验证用户是否创建成功
```

将oracle用户密码设置为"oracle"

```shell
[root@oracle ~]# passwd oracle						#为oracle用户设置密码为oracle
更改用户 oracle 的密码 。
新的 密码：											 #输入密码不显示
无效的密码： 密码少于 8 个字符
重新输入新的 密码：
passwd：所有的身份验证令牌已经成功更新。
```

准备oracle安装目录

```shell
[root@oracle ~]# mkdir -p /data/oracle				#创建oracle主目录
[root@oracle ~]# mkdir -p /data/inventory			#创建oralce配置目录
[root@oracle ~]# mkdir -p /data/src					#创建oracle压缩包解压目录
```

修改目录归属关系

```shell
[root@oracle ~]# chown -R oracle:oinstall /data/oracle
[root@oracle ~]# chown -R oracle:oinstall /data/inventory
[root@oracle ~]# chown -R oracle:oinstall /data/src

[root@oracle ~]# ls -l /data/						#查看Oracl目录的归属关系
总用量 0
drwxr-xr-x 2 oracle oinstall 6 8月  27 15:20 inventory
drwxr-xr-x 2 oracle oinstall 6 8月  27 15:20 oracle
drwxr-xr-x 2 oracle oinstall 6 8月  27 15:20 src
```

配置oracle用户shell权限

- 设置操作系统对于Oracle数据库的资源限制
- 这些资源限制的目的是为了保证Oracle数据库在运行时可以充分利用系统资源，提高数据库的运行性能。
- oracle soft nproc 2047：这条配置限制了运行用户oracle的最大进程数（nproc），软限制为2047。软限制表示该值可以通过修改来调整。确保用户oracle在运行时有足够数量的进程资源。
- oracle hard nproc 16384：这条配置限制了运行用户oracle的最大进程数，硬限制为16384。硬限制表示该值只能由系统管理员通过修改配置文件来调整。
- oracle soft nofile 1024：这条配置限制了运行用户oracle的最大文件描述符数（nofile），软限制为1024。确保Oracle数据库可以同时打开足够数量的文件描述符。
- oracle hard nofile 65536：这条配置限制了运行用户oracle的最大文件描述符数，硬限制为65536。确保Oracle数据库可以打开更多的文件描述符。

```shell
[root@oracle ~]# vim /etc/security/limits.conf
...第51行下方添加下方内容...其他任容不要动
 52 oracle soft nproc 2047
 53 oracle hard nproc 16384
 54 oracle soft nofile 1024
 55 oracle hard nofile 65536
...此处省略一万字...
```

修改用户变量

```shell
[root@oracle ~]# vim /home/oracle/.bashrc
...最后一行下方追加下方内容...其他地方不要动
export ORACLE_BASE=/data/oracle
export ORACLE_HOME=$ORACLE_BASE/product/11.2.0/db_1
export ORACLE_SID=orcl
export ORACLE_UNQNAME=orcl
export PATH=$ORACLE_HOME/bin:/usr/sbin:$PATH
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export LANG=C
export NLS_LANG=AMERICAN_AMERICA.AL32UTF8

[root@oracle ~]# source /home/oracle/.bashrc			#执行文件，使用配置生效 
```

将学习资料中的oracle-install上传至oracle虚拟机的/root/

安装oracle依赖包

```shell
[root@oracle ~]# cd /root/oracle-install
[root@oracle oracle-install]# unzip rpms.zip			#将依赖包解压到当前路径
[root@oracle ~]# cd rpms/								#切换至解压目录
[root@oracle rpms]# yum -y localinstall *.rpm			#安装依赖包
```

验证ksh是否安装

```shell
[root@oracle rpms]# ls -l /bin/ksh*
lrwxrwxrwx 1 root root      21 Aug 27 15:53 /bin/ksh -> /etc/alternatives/ksh
-rwxr-xr-x 1 root root 1519032 Aug 11  2022 /bin/ksh93
```

注：如果安装不正确，执行ksh安装： <font color='red'>rpm -ivh /root/oracle-install/rpms/ksh-20120801-144.el7_9.x86_64.rpm</font>

解压缩Oracle安装包

```shell
[root@oracle ~]# cd /root/oracle-install/
[root@oracle oracle-install]# unzip linux.x64_11gR2_database_1of2.zip -d /data/src/
[root@oracle oracle-install]# unzip linux.x64_11gR2_database_2of2.zip -d /data/src/
[root@oracle oracle-install]# chown -R oracle:oinstall /data/src/
[root@oracle oracle-install]# ls -l /data/src/
total 0
drwxr-xr-x 8 oracle oinstall 128 Aug 21  2009 database
```

关闭SELinux（已经在安装模板机的时候关闭过了，如果没有关闭，请按照如下方式关闭）

```shell
[root@oracle ~]# vim /etc/selinux/config
...其他内容不要动，只修改第七行内容...
7 SELINUX=disabled
...此处省略一万字...
```

重启

```shell
[root@oracle ~]# reboot
```

#  安装配置

- 配置文件修改比较复杂，因此直接使用教学环境中修改完毕的即可

- 从学习环境资料里将配置文件<font color='red'>dbca.rsp、db_install.rsp、netca.rsp</font>上传至oracle虚拟机的/data/src/database/response/

- 上传之后无需再次编辑，上传的文件就是编辑好的。配置文件修改部分说明如下

```shell
[root@oracle ~]# vim /data/src/database/response/db_install.rsp
oracle.install.option=INSTALL_DB_SWONLY 　　　　　　 				#安装类型
ORACLE_HOSTNAME=oracle.server 　　　 							  #主机名称
UNIX_GROUP_NAME=oinstall 										#安装组
INVENTORY_LOCATION=/data/inventory 								#INVENTORY目录
SELECTED_LANGUAGES=en,zh_CN 									#选择语言
ORACLE_HOME=/data/oracle/product/11.2.0/db_1 					#oracle_home
ORACLE_BASE=/data/oracle 										#oracle_base
oracle.install.db.InstallEdition=EE 							#oracle版本
oracle.install.db.DBA_GROUP=dba 								#dba用户组
oracle.install.db.OPER_GROUP=oinstall 							#oper用户组
oracle.install.db.config.starterdb.type=GENERAL_PURPOSE 		#数据库类型
oracle.install.db.config.starterdb.globalDBName=orcl 			#globalDBName
oracle.install.db.config.starterdb.SID=orcl 					#SID
oracle.install.db.config.starterdb.characterSet=AL32UTF8 		#默认数据库编码
oracle.install.db.config.starterdb.memoryLimit=800 				#自动管理内存的最小内存(M)
oracle.install.db.config.starterdb.password.ALL=oracle 	#设定所有数据库用户使用同一个密码
DECLINE_SECURITY_UPDATES=true 									#设置安全更新
```

```shell
[root@oracle ~]# vim /data/src/database/response/netca.rsp
INSTALL_TYPE=""custom"" 										#安装的类型
LISTENER_NUMBER=1 												#监听器数量
LISTENER_NAMES={"LISTENER"} 									#监听器的名称列表
LISTENER_PROTOCOLS={"TCP;1521"} 								#监听器使用的通讯协议列表
LISTENER_START=""LISTENER"" 									#监听器启动的名称
```

```shell
[root@oracle ~]# vim /data/src/database/response/dbca.rsp
RESPONSEFILE_VERSION ="11.2.0" 									#版本号
OPERATION_TYPE ="createDatabase" 								#操作为创建实例 
GDBNAME ="orcl" 												#数据库实例名
SID ="orcl" 													#实例名字
TEMPLATENAME = "General_Purpose.dbc" 							#建库用的模板文件
SYSPASSWORD = "oracle" 											#SYS管理员密码
SYSTEMPASSWORD = "oracle" 										#SYSTEM管理员密码
SYSMANPASSWORD= "oracle"
DBSNMPPASSWORD= "oracle"
DATAFILEDESTINATION =/data/oracle/oradata 						#数据文件存放目录
RECOVERYAREADESTINATION=/data/oracle/flash_recovery_area 		#恢复数据存放目录
CHARACTERSET ="AL32UTF8" 										#字符集
NATIONALCHARACTERSET= "AL16UTF16" 								#字符集
TOTALMEMORY ="1638" 											#1638MB，物理内存2G*80%
```

# 安装Oracle

使用oracle用户进行安装，提示<font color='green'>Successfully</font>即可

```shell
[root@oracle ~]# su - oracle
[oracle@oracle ~]# /data/src/database/runInstaller -silent -responseFile \ /data/src/database/response/db_install.rsp -ignorePrereq
```

![1693129592022](Oracle.assets/1693129592022.png)

<font color='red'>重新打开一个远程连接窗口</font>，使用root登录

```shell
[root@oracle ~]# /data/inventory/orainstRoot.sh
[root@oracle ~]# /data/oracle/product/11.2.0/db_1/root.sh
```

返回使用oracle登录的窗口，按回车

安装监听

```shell
[oracle@oracle ~]# /data/oracle/product/11.2.0/db_1/bin/netca /silent /responseFile \ /data/src/database/response/netca.rsp
```

如果提示下面的错误，则使用下方方法解决，如果没有报错，则忽略

![1693130263650](Oracle.assets/1693130263650.png)

```shell
[root@oracle ~]# find / -name libclntsh.so.11.1			#使用root用户搜索
```

![1693130437652](Oracle.assets/1693130437652.png)

复制备份文件到上面报错的文件

```shell
[root@oracle ~]# cp /data/oracle/product/11.2.0/db_1/inventory/backup/2023-05-13_06-04-35PM/Scripts/ext/lib/libclntsh.so.11.1  \ /data/oracle/product/11.2.0/db_1/lib/libclntsh.so.11.1
```

安装数据库实例

```shell
[oracle@oracle ~]# /data/oracle/product/11.2.0/db_1/bin/dbca -silent -responseFile \ /data/src/database/response/dbca.rsp
```

查看是否安装成功

```shell
[oracle@oracle ~]$ ps -ef | grep ora_ | grep -v grep
```

登录数据库测试

```shell
[oracle@oracle ~]$ sqlplus / as sysdba
SQL> select instance_name, status from v$instance;		#查看实例状态	

INSTANCE_NAME    STATUS
---------------- ------------
orcl             OPEN
SQL> exit												#退出
```

# 设置Oracle开机自启

修改启动配置

```shell
[oracle@oracle ~]$ vim /data/oracle/product/11.2.0/db_1/bin/dbstart
#修改第80行，指定监听器安装目录
80 ORACLE_HOME_LISTNER=/data/oracle/product/11.2.0/db_1
```

修改关闭配置

```shell
[oracle@oracle ~]$ vim /data/oracle/product/11.2.0/db_1/bin/dbshut
#修改第50行，指定监听器安装目录
 50 ORACLE_HOME_LISTNER=/data/oracle/product/11.2.0/db_1
```

修改/etc/oratab，将文件最后一行末尾的N改为<font color='red'>Y</font>

```shell
[oracle@oracle ~]$ vim /etc/oratab

#最后一行末尾改成Y
orcl:/data/oracle/product/11.2.0/db_1:Y
```

用root用户设置开机启动，先 exit退出oracle用户

```shell
[oracle@oracle ~]$ exit								#退出oracle用户
[root@oracle ~]# vim /etc/rc.d/rc.local
# 文件最后一行后追加
su oracle -lc "/data/oracle/product/11.2.0/db_1/bin/lsnrctl start"
su oracle -lc "/data/oracle/product/11.2.0/db_1/bin/dbstart"

[root@oracle ~]# chmod +x /etc/rc.d/rc.local		#赋予执行权限	
```

配置listener.ora

```shell
[root@oracle ~]# vim /data/oracle/product/11.2.0/db_1/network/admin/listener.ora
...
#第9行下方添加这些配置
 10 SID_LIST_LISTENER =
 11   (SID_LIST =
 12     (SID_DESC =
 13           (GLOBAL_DBNAME = ORCL)
 14       (SID_NAME = orcl)
 15       (ORACLE_HOME = /data/oracle/product/11.2.0/db_1)
 16     )
 17   )
 ...
```

重启监听器（使用oracle用户执行）

```shell
[root@oracle ~]# su - oracle
[oracle@oracle ~]$ lsnrctl stop
[oracle@oracle ~]$ lsnrctl start
```

使用oracle用户登录数据库，创建用户，用于登录数据库

```shell
[oracle@oracle ~]$ sqlplus / as sysdba
SQL> create user orcl identified by orcl;			#创建用户orcl，密码orcl
SQL> grant connect,resource,dba to orcl;			#授权
SQL> grant create session to orcl;					#授权
```

安装Dbeaver，用于连接Oracle数据库（安装包在教学环境中）

![1693133279132](Oracle.assets/1693133279132.png)

选择数据库类型为Oracle

![1693133250809](Oracle.assets/1693133250809.png)

![1693133429413](Oracle.assets/1693133429413.png)

![1693133713364](Oracle.assets/1693133713364.png)

![1693133740869](Oracle.assets/1693133740869.png)

![1693133906410](Oracle.assets/1693133906410.png)

![1693133951635](Oracle.assets/1693133951635.png)

#  Oracle核心概念

- 实例：oracle的数据库
- 用户（User、Schema）：用户是数据库中的账户，用于连接和访问数据库。每个用户可以拥有不同的权限和角色。
- 表空间（Tablespace）：表空间是数据库中逻辑存储结构的组成部分，它是一组数据文件的集合，用于存储用户数据和索引等对象。
- 数据文件（Datafile）：数据文件是数据库中存储数据和索引等对象的实际物理文件，每个数据文件属于一个表空间。
- 段（Segment）：段是逻辑存储结构，是一组相同类型的数据的集合，可以是表、索引、簇等对象。

| MySQL            | Oracle       |
| ---------------- | ------------ |
| MySQL            | 实例         |
| Database、Schema | 用户、Schema |
|                  | 表空间       |
| 数据文件         | 数据文件     |
|                  | Segment      |

在Dbeaver中执行SQL语句

```sql
CREATE tablespace orcldata
datafile '/data/oracle/oradata/orcl/orcldata.dbf'
SIZE 50m
AUTOEXTEND ON
NEXT 20m
segment space management auto
```

在服务器上查看文件列表

```shell
[oracle@oracle ~]$ ls -lh /data/oracle/oradata/orcl/
total 1.5G
-rw-r----- 1 oracle dba 9.3M Aug 27 19:15 control01.ctl
-rw-r----- 1 oracle dba  51M Aug 27 19:13 orcldata.dbf
-rw-r----- 1 oracle dba  51M Aug 27 19:14 redo01.log
-rw-r----- 1 oracle dba  51M Aug 27 18:04 redo02.log
-rw-r----- 1 oracle dba  51M Aug 27 18:04 redo03.log
-rw-r----- 1 oracle dba 481M Aug 27 19:09 sysaux01.dbf
-rw-r----- 1 oracle dba 671M Aug 27 19:09 system01.dbf
-rw-r----- 1 oracle dba  30M Aug 27 19:04 temp01.dbf
-rw-r----- 1 oracle dba  76M Aug 27 19:09 undotbs01.dbf
-rw-r----- 1 oracle dba 5.1M Aug 27 18:04 users01.dbf
```

增加表空间

```sql
alter tablespace orcldata
add datafile '/data/oracle/oradata/orcl/orcldata2.dbf'
size 30m
autoextend off	
```

在服务器上查看文件列表

```shell
[oracle@oracle ~]$ ls -lh  /data/oracle/oradata/orcl/
```

在Dbeaver中执行SQL语句

- 查看表空间 查看表空间状态

```sql
select tablespace_name,block_size,status,contents,logging from dba_tablespaces
```

查看表空间数据文件

```sql
select file_id,file_name,tablespace_name,status,bytes from dba_data_files
```

临时表空间和undo表空间 

- 临时表空间：处理计算或者排序，临时需要的 
- undo：和事务有关

```sql
create temporary tablespace temp1
tempfile '/data/oracle/oradata/orcl/temp1.dbf'
size 5m
autoextend off
```

```sql
create undo tablespace orcl_undo
datafile '/data/oracle/oradata/orcl/orcl_undo.dbf'
size 20m
autoextend off
```

删除表空间

```sql
DROP tablespace orcldata
DROP tablespace orcldata INCLUDING contents AND datafiles
```

查询表空间的使用率 以下两个复杂的查询都可以得到表空间使用率数据

```sql
select upper(f.tablespace_name) "TS-name",
d.tot_grootte_mb "TS-bytes(m)",
d.tot_grootte_mb - f.total_bytes "TS-used (m)",
f.total_bytes "TS-free(m)",
to_char(round((d.tot_grootte_mb - f.total_bytes) / d.tot_grootte_mb * 100,2),'990.99') "TS-per"
from (select tablespace_name,
round(sum(bytes) / (1024 * 1024), 2) total_bytes,
round(max(bytes) / (1024 * 1024), 2) max_bytes
from sys.dba_free_space
group by tablespace_name) f,
(select dd.tablespace_name,
round(sum(dd.bytes) / (1024 * 1024), 2) tot_grootte_mb
from sys.dba_data_files dd
group by dd.tablespace_name) d
where d.tablespace_name = f.tablespace_name
order by 5 desc
```

# 创建用户

- 创建用户zhangsan，密码为ora1234
- zhangsan用户的数据存放在neworcldata表空间，临时表空间存放在temp2表空间

```sql
CREATE tablespace neworcldata
datafile '/data/oracle/oradata/orcl/orcldatanew.dbf'
SIZE 50m
AUTOEXTEND ON
NEXT 20m
segment space management auto

create temporary tablespace temp2
tempfile '/data/oracle/oradata/orcl/temp2.dbf'
size 5m
autoextend off

create user zhangsan 
identified by ora1234 
default tablespace neworcldata
temporary tablespace temp2
```

![1693136540284](Oracle.assets/1693136540284.png)

## 修改用户密码

```sql
alter user zhangsan identified by zhangsan1234
```

## 用户授权

授予用户 connect,dba 角色

```sql
grant CONNECT,dba to zhangsan;
```

收回用户DBA角色的权限

```sql
revoke dba from zhangsan;
```

授予用户 能查询SCOTT 下的 EMP 表的权限

```sql
grant select on scott.emp to zhangsan;
```

授予用户能查询所有表的权限

```sql
grant select any table to zhangsan;
grant update any table to zhangsan;
grant insert any table to zhangsan;
grant DELETE any table to zhangsan;
grant alter any table to zhangsan;
grant CREATE any table to zhangsan;
grant DROP any table to zhangsan;
```

## 查看用户

```sql
select username,created from dba_users;
```

# 数据备份

Oracle Data Pump是Oracle提供的一种高性能，可扩展的数据导入/导出工具。它是一种逻辑备份和恢复数据库的方法，也是逻辑备份中的一种常用方法。

Oracle Data Pump提供了两个主要的工具：expdp和impdp。expdp用于将数据库对象和数据导出为一个或多个数据文件，impdp用于将数据文件导入到数据库中。

添加数据

```sql
#新建表并插入数据
CREATE TABLE tb1(id int, name varchar2(20))

CREATE INDEX idx_name ON tb1(name)

INSERT INTO tb1 values(1, 'A')
```

备份数据

```sql
# 查询数据备份目录
select * from dba_directories;

OWNER  DIRECTORY_NAME         DIRECTORY_PATH
------ ---------------------- -------------------------------------------
SYS    ORACLE_OCM_CONFIG_DIR  /data/oracle/product/11.2.0/db_1/ccr/state
SYS    DATA_PUMP_DIR          /data/oracle/admin/orcl/dpdump/
SYS    XMLDIR                 /ade/b/2125410156/oracle/rdbms/xml
```

后续使用data_pump_dir

![1693137113651](Oracle.assets/1693137113651.png)

备份：

```shell
[oracle@oracle ~]$ expdp orcl/orcl@orcl \
schemas=orcl directory=DATA_PUMP_DIR \
dumpfile=backup.dmp include=table,index
```

expdp - 数据泵实用程序的命令，用于导出数据库对象和数据

orcl/orcl@orcl - 数据库的用户名和密码。orcl是用户名，orcl是密码，@orcl是数据库实例名

schemas=orcl - 要导出的数据库模式，即用户

directory=DATA_PUMP_DIR - 数据库中已经创建的一个目录对象，用于指定导出文件的保存路径

dumpfile=backup.dmp - 导出的备份文件名为backup.dmp

include=table,index - 导出时包含的对象类型，这里包括表和索引。只会导出包含指定对象类型的数据和结构

查看备份文件

```shell
[oracle@oracle ~]$ ls -l /data/oracle/admin/orcl/dpdump/
```

模拟数据丢失

```sql
drop table tb1;
```

恢复数据

```
impdp orcl/orcl@orcl \
directory=DATA_PUMP_DIR dumpfile=backup.dmp \
include=table,index
```

# 总结

- 掌握Oracle数据库安装过程
- 掌握Oracle核心概念
- 掌握数据库备份