[TOC]

# SHELL基础

- shell是用户与linux内核之间的解释器
- shell环境准备，本实验需要用到一台虚拟机A即可

![1688303035665](Day10.assets/1688303035665.png)

## 查看有哪些解释器

```shell
 [root@som ~]# cat /etc/shells                  #查看shell解释器
```

## 使用usermod修改用户解释器

```shell
[root@som ~]# usermod -s /bin/tcsh test		#使用usermod指令修改用户解释器
[root@som ~]# grep test /etc/passwd			#从/etc/passwd过滤test用户信息
```

## BASH基本特性

查看

```shell
[root@som ~]# ls							#数据有颜色区分
```

重定向

![1690511362286](Day10.assets/1690511362286.png)



覆盖重定向

```shell
[root@som ~]# ls  >  a.txt
[root@som ~]# cat  a.txt
```

追加重定向

```shell
[root@som ~]# ls  >>  a.txt
[root@som ~]# cat a.txt 
```

显示一个错误的文件

```shell
[root@som ~]# ls xxyyzz.txt
ls: 无法访问xxyyzz.txt: 没有那个文件或目录
[root@som ~]# ls xxyyzz.txt > b.txt                     #失败，> 收集正确信息
ls: 无法访问xxyyzz.txt: 没有那个文件或目录
[root@som ~]# ls xxyyzz.txt  2>  b.txt                  #正确，2> 收集错误信息
[root@som ~]# cat b.txt 
ls: 无法访问xxyyzz.txt: 没有那个文件或目录
```

收集正确和错误的信息

```shell
[root@som ~]# ls  a.txt  xxyzz.txt  >  b.txt
[root@som ~]# ls  a.txt  xxyzz.txt  &>  b.txt           #收集所有信息
[root@som ~]# cat b.txt 
ls: 无法访问xxyzz.txt: 没有那个文件或目录
a.      txt
```

管道

```shell
[root@som ~]# ifconfig | head -2                      #查看ip信息前两行
```

![1690511384414](Day10.assets/1690511384414.png)

 快捷键与Tab键补齐，常见快捷键如表所示

![1690511401870](Day10.assets/1690511401870.png)

# shell脚本的设计与运行

什么是shell脚本

- Shell脚本是一种自动化执行任务的脚本语言，可以帮助我们完成日常任务，比如文件管理、进程管理等。
- 脚本可以理解为功能性文件

## 编写问世脚本

```shell
[root@som ~]# mkdir -p /root/shell/day0{1..2}
[root@som ~]# vim /root/shell/day01/first.sh
echo "Hello World"
[root@som ~]# chmod  +x /root/shell/day01/first.sh          #添加执行权限
[root@som ~]# /root/shell/day01/first.sh                    #绝对路径形式执行
Hello World
```

```shell
[root@som ~]# cd /root/shell/day01/		  	  #切换目录
[root@som day01]# ./first.sh                  #相对路径执行
Hello World
```

## 脚本格式规范

- 脚本声明（需要的解释器、作者信息等）
- 注释信息（步骤、思路、用途、变量含义等）
- 可执行语句（操作代码）

 

优化刚刚的first.sh脚本

```shell
[root@som day01]# vim /root/shell/day01/first.sh
#!/bin/bash                            #指定解释器
#This a test program for shell.        #这是一个测试性的程序
echo "Hello World"
[root@som day01]# ./first.sh		   #执行脚本
```

## 执行shell脚本

执行脚本的多种方式

![1690511424219](Day10.assets/1690511424219.png)

### 方法一

脚本在执行的时候要有执行（x）权限，否则会报错

```shell
[root@som day01]# chmod -x first.sh 
[root@som day01]# ./first.sh                     #报错
-bash: ./first.sh: 权限不够
[root@som day01]# /root/shell/day01/first.sh     #报错
-bash: /root/shell/day01/first.sh: 权限不够
```

### 方法二

不需要文件有可执行权限，指定解释器执行脚本

```shell
[root@som day01]# sh first.sh       		#指定sh来执行first.sh
[root@som day01]# bash first.sh  			#指定bash解释器执行first.sh
```

### 实验

```shell
[root@som day01]# vim tmp.sh				#编写tmp.sh
#!/bin/bash
exit
[root@som day01]# sh tmp.sh        			#指定运行脚本，没有关闭终端
```

```shell
[root@som day01]# vim tmp.sh				#编写tmp.sh
#!/bin/bash
exit
[root@som day01]# source  tmp.sh			#执行tmp.sh，会关闭终端
```

总结：

- 指定解释器会新开子进程
- 使用source不会新开子进程 

# 变量

## 自定义变量

- 环境变量（变量名通常大写，有操作系统维护）
- 位置变量（bash内置变量，存储脚本执行时的参数）
- 预定义变量（bash内置变量，可以调用但是不能赋值或修改）
- 自定义变量（用户自主设置）

定义变量

- 可以是数字，字母，下划线
- 变量名不能使用特殊符号，会报错
- 不能以数字开头

查看变量

- echo ${变量名}
- echo $变量名

定义变量

```shell
[root@som ~]# a=11
[root@som ~]# echo $a           	#调用变量,查看变量的值
[root@som ~]# a=33              	#变量名已经存在，再次赋值，里面的内容会被覆盖
[root@som ~]# echo $a			 	#调用变量,查看变量的值
```

```shell
[root@som ~]# a)=11					#变量包含特殊符号，所以定义失败
-bash: 未预期的符号 `)' 附近有语法错误	
[root@som ~]# 3a=33					#变量数字开头，所以定义失败
bash: 3a=33: 未找到命令...
```

```shell
[root@som ~]# a_0=11				#没有违规，所以成功
[root@som ~]# _a=11					#没有违规，所以成功
[root@som ~]# _0=11					#没有违规，所以成功
```

```shell
[root@som ~]# x=CentOS
[root@som ~]# echo $x           	#成功
[root@som ~]# echo ${x}        		#成功
```

若想要显示CentOS7.9

```shell
[root@som ~]# echo $x7.9          	#失败，会显示.9，此时是把$x7看成一个变量名
```

加上{}可以成功

```shell
[root@som ~]# echo ${x}7.9			#输出CentOS7.9
[root@som ~]# echo ${x}7.6			#输出CentOS7.6
```

取消变量

```shell
[root@som ~]# unset x					#取消变量
[root@som ~]# echo $x
```

## 环境变量

- 存储在/etc/profile或~/.bash_profile
- 命令env可以列出所有环境变量
- 环境变量通常是大写字母

```shell
[root@som ~]# echo $PATH             	#命令搜索的路径变量
[root@som ~]# echo $PWD             	#返回当前工作目录
/root
[root@som ~]# echo $USER            	#显示当前登录的用户
root
[root@som ~]# echo $UID               	#显示当前用户的uid
0
[root@som ~]# echo $HOME          		#显示当前用户的家目录
/root
[root@som ~]# echo $SHELL           	#显示当前的SHELL
/bin/bash 
```

## 位置变量

- 存储脚本时执行的参数
- $1  $2   $3 ......$9  ${10}    ${11}  ....        #从10开始位置变量需要加{}

```shell
[root@som ~]# vim /root/shell/day01/vars.sh
#!/bin/bash
echo $1
echo $2
echo $3
```

```shell
[root@som ~]# chmod +x /root/shell/day01/vars.sh 
[root@som ~]# /root/shell/day01/vars.sh aa bb cc       #执行脚本，传递参数
aa
bb
cc
```

### 案例

- 编写一个user.sh脚本，使用它创建用户

```shell
[root@som ~]# vim /root/shell/day01/user.sh
#!/bin/bash
useradd "$1"						#创建用户
echo "$2" | passwd --stdin $1		#设置密码
```

```shell
[root@som ~]# chmod +x /root/shell/day01/user.sh
[root@som ~]# /root/shell/day01/user.sh tom 123			#执行脚本
[root@som ~]# /root/shell/day01/user.sh jim 123			#执行脚本
```

## 预定义变量

- 用来保存脚本程序的执行信息，可以直接使用这些变量，但是不能为这些变量赋值

![1690511441098](Day10.assets/1690511441098.png)

$?：执行上一条命令的返回状态，0为正确，非0为错误

```shell
[root@som ~]# ls /etc/hosts				#执行命令成功
/etc/hosts
[root@som ~]# echo $?           		#返回值为0，正确
0
```

```shell
[root@som ~]# ls /xxxxxyyyy     		#执行命令错误    	
ls: 无法访问/xxxxxyyyy: 没有那个文件或目录
[root@som ~]# echo $?					#返回值为非0，失败
2
```

其他几个预定义变量的测试

```shell
[root@som ~]# vim /root/shell/day01/pre.sh
#!/bin/bash
echo $0             #执行脚本的名字
echo $$             #当前脚本的进程号
echo $#             #位置变量的个数
echo $*             #所有位置变量
```

```shell
[root@som7 ~]# chmod  +x /root/shell/day01/pre.sh
[root@som7 ~]# /root/shell/day01/pre.sh  a b c d
/root/shell/day01/pre.sh
46608
4
a b c d
```

# 变量的扩展运用

## 多种引号的区别

### 双引号的应用

- <font color='red'>使用双引号可以界定一个完整字符串</font>。

```shell
[root@som ~]# touch a b c            	#创建了三个文件
[root@som ~]# touch "a b c"           	#创建1一个文件
[root@som ~]# ls -l
```

```shell
[root@som ~]# rm -rf  a b c           	#删除三个文件
[root@som ~]# rm -rf  "a b c"       	#删除一个文件
```

### 单引号的应用

- 界定一个完整的字符串，并且可以实现屏蔽特殊符号的功能。
- 当双引号里面有变量时，会被扩展出来，也就是会取变量的值

```shell
[root@som ~]# hi="world"
[root@som ~]# echo "$hi"               #成功
world
[root@som ~]# echo '$hi'               #失败，当成一个字符串
$hi
```

当没有特殊符号时，单引号和双引号的含义是一样的

```shell
[root@som ~]# touch "a b c"			
[root@som ~]# touch 'c d e'
```

练习单引号和双引号的区别

```shell
[root@som ~]# echo "$USER id is $UID"	#调用变量
root id is 0
[root@som ~]# echo '$USER id is $UID'	#不调用变量
$USER id is $UID
```

### 反撇号或$()的应用

- 使用反撇号``或$()时，可以将命令执行的标准输出作为字符串存储，因此称为命令替换。

```shell
[root@som ~]# grep root /etc/passwd
[root@som ~]# test=`grep root /etc/passwd`		#定义变量，内容为命令输出结果
[root@som ~]# echo $test
```

```shell
[root@som ~]# test2=$(grep root /etc/passwd)	#定义变量，内容为命令输出结果
[root@som ~]# echo $test2
```

## read命令定义变量

- 使用read命令从键盘读取变量值
  - -p: 指定提示信息
  - -s: 屏蔽输入（键盘输入内容，在屏幕上不显示）
  - -t: 可指定超时秒数（指定秒数不输入，直接退出）

read基本用法

- 执行后从会等待并接受用户输入（无任何提示的情况），并赋值给变量：

```shell
[root@som ~]# read iname					#定义变量iname
123											#从键盘输入123作为iname的值
[root@som ~]# echo $iname					#输出变量iname
123
```

虽然可以赋值。但是屏幕上没有任何提示信息，在未来写脚本的时候不太方便，可以加上-p选项，给出提示

```shell
[root@som ~]# read -p "请输入用户名：" iname	#定义变量
请输入用户名：tom
[root@som ~]# echo $iname					 #输出变量
tom
```

### 案例

创建一个脚本，通过read定义变量创建用户，更改密码

```shell
[root@som ~]# vim /root/shell/day01/read.sh
#!/bin/bash
read -p "请输入用户名：" name
read -p "请输入密码："   pass
useradd $name
echo "$pass" | passwd --stdin $name
```

```shell
[root@som ~]# chmod +x /root/shell/day01/read.sh
[root@som ~]# /root/shell/day01/read.sh 
请输入用户名：user2
请输入密码：a
更改用户 user2 的密码 。
passwd：所有的身份验证令牌已经成功更新。
```

但是此时密码是名为显示的，不安全，可以使用-s参数，不显示终端输入的信息

```shell
[root@som ~]# vim /root/shell/day01/read.sh
read -p "请输入用户名：" name
read -s -p "请输入密码："   pass
useradd $name
echo "$pass" | passwd --stdin $name
```

```shell
[root@som ~]# /root/shell/day01/read.sh 
请输入用户名：user3
请输入密码：
更改用户 user3 的密码 。
passwd：所有的身份验证令牌已经成功更新。
```

```shell
[root@som ~]# read -t 3 iname               #3秒不输入直接退出
```

# 条件测试

- 语法格式：使用  [   表达式   ]，表达式两边至少要留一个空格。

## 字符串测试

- 是否为空 [     -z     字符串      ]

```shell
[root@som ~]# echo $TT
[root@som ~]# [ -z $TT ]         		#T为空吗
[root@som ~]# echo $?           		#是，返回值为0
0
```

```shell
[root@som ~]# TT="hello"
[root@som ~]# [ -z $TT ]         		#T为空吗
[root@som ~]# echo $?           		#否，返回值非0
1
```

等于: [    字符串1   ==    字符串2    ]

```shell
[root@som ~]# [ a == a ]				#判断a==a
[root@som ~]# echo $?           		#查看返回值
0
[root@som ~]# [ a == c ]				#判断a==c
[root@som ~]# echo $?					#查看返回值
1
```

变量和常量的判断

```shell
[root@som ~]# [ $USER == root ]			#环境变量USER的值是root吗
[root@som ~]# echo $?
0
[root@som ~]# [ $USER == tom ]			#环境变量USER的值是tom吗
[root@som ~]# echo $?
1
```

不等于[ 字符串1 != 字符串2 ]

```shell
[root@som ~]# [   $USER != tom   ]			#环境变量USER的值不是tom
[root@som ~]# echo $?
0
```

## 整数值比较

格式：[ 整数值1 操作符 整数值2 ]

- -eq：等于 
- -ne：不等于   
- -gt：大于   
- -ge：大于等于   
- -lt：小于   
- -le：小于等于

参与比较的必须是整数（可以调用变量），<font color='red'>比较非整数值时会出错</font>：

## 运算

四则运算：+ - * /

求模取余：%

计算练习

```shell
[root@som ~]# echo $[1+1]					#计算1+1
[root@som ~]# echo $[10-2]					#计算10-2
[root@som ~]# echo $[2*2]					#计算2*2
[root@som ~]# echo $[6/3]					#计算6/3
[root@som ~]# echo $[10%3]					#取10/3的余数
```

变量计算

```shell
[root@som ~]# a=10
[root@som ~]# b=20
[root@som ~]# echo $[a+b]					#计算变量a+变量b
```

### 常量与常量的比较

小于

```shell
[root@som ~]# [ 3 -lt 8 ]
[root@som ~]# echo $?
0
```

大于

```shell
[root@som ~]# [ 3 -gt 2 ]
[root@som ~]# echo $?
0
```

等于

```shell
[root@som ~]# [ 3 -eq 3 ]
[root@som ~]# echo $?
0
```

小于等于

```shell
[root@som ~]# [ 3 -le 3 ]
[root@som ~]# echo $?
0
```

大于等于

```shell
[root@som ~]# [ 3 -ge 1 ]
[root@som ~]# echo $?
0
```

### 常量与变量的比较

判断计算机登录的用户

```shell
[root@som ~]# who | wc -l
[root@som ~]# [ $(who | wc -l)  -ge  2 ]
[root@som ~]# echo $?
0
```

## 文件状态的测试 

- 格式: [ 操作符 文件或目录 ]
- -e 判断对象是否存在（不管是目录还是文件），存在则结果为真

```shell
[root@som ~]# [ -e /etc ]
[root@som ~]# echo $?
0
```

```shell
[root@som ~]# [ -e /etc/hosts ]
[root@som ~]# echo $?
0
```

```shell
[root@som ~]# [ -e /etc/xxyy ]
[root@som ~]# echo $?
1
```

-d 判断对象是否为目录（存在且是目录），是则为真

```shell
[root@som ~]# [ -d /etc/hosts ]
[root@som ~]# echo $?
1
[root@som ~]# [ -d /etc/ ]
[root@som ~]# echo $?
0
```

-f 判断对象是否为文件（存在且是文件）是则为真

```shell
[root@som ~]# [ -f /etc/ ]
[root@som ~]# echo $?
1
[root@som ~]# [ -f /etc/hosts ]
[root@som ~]# echo $?
0
```

-r 判断对象是否可读，是则为真

```shell
[root@som ~]# ls -l  /etc/hosts
-rw-r--r--. 1 root root 158 6月   7 2013 /etc/hosts
[root@som ~]# [ -r /etc/hosts ]
[root@som ~]# echo $?
0
```

-w 判断对象是否可写，是则为真

```shell
[root@som ~]# [ -w /etc/hosts ]
[root@som ~]# echo $?
0
```

-x 判断对象是否具有可执行权限，是则为真

```shell
[root@som ~]# [ -x /etc/hosts ]
[root@som ~]# echo $?
1
```

# 总结

- 掌握SHELL脚本执行流程
- 掌握SHELL中的变量
  - 自定义变量
  - 环境变量
  - 位置变量
  - 预定义变量
- 掌握SHELL中条件测试