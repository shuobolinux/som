[TOC]

# 实验环境

本实验需要一台<font color='red'>CentOS7.9虚拟机</font>即可

# 用户与组概述

用户作用： 

- 登陆操作系统  
- 方便做权限的不同设置
- 用户的唯一标识：UID
- LINUX系统默认管理员为root，管理员root的UID为0

组的作用

- 方便管理众多的用户，方便对用户进行分类
- 组的唯一标识：GID
- 组的分类：<font color='red'>基本组</font>，<font color='green'>附加组（从属组）</font>
  - 基本组：Linux自己创建的组，与用户同名，系统自动将用户加入
  - 附加组（从属组）：管理员自建创建，管理员将用户加入

- Linux一个用户必须至少属于一个组
  - 例如：创建一个用户zhangsan，默认会创建与用户同名的组，并把用户加入到这个组里面，这个组是zhangsan用户的基本组

```shell
[root@som ~]# useradd	zhangsan					#创建zhangsan用户
```



# 用户账号创建

用户基本信息存放在 <font color='red'>/etc/passwd</font> 文件，每个用户记录一行，以：分割为7字段，含义如下

<font color='red'>用户名</font>:<font color='orange'>密码占位符</font>:<font color='yellow'>用户UID</font>:<font color='green'>基本组GID</font>:<font color='turquoise'>描述信息</font><font color='blue'>:家目录(宿主目录)</font>:<font color='purple'>解释器</font>

```shell
[root@som ~]# wc -l /etc/passwd					#统计当前系统中的用户数量
[root@som ~]# grep zhangsan /etc/passwd			#从/etc/passwd文件中过滤zhangsan的信息
zhangsan:x:1004:1004::/home/zhangsan:/bin/bash
```

## 查看用户-查

- 使用id命令

- 格式：id   用户名

- 作用：判断该用户是否存在

```shell
[root@som ~]# id zhangsan							#判断用户是否存在
uid=1002(zhangsan) gid=1002(zhangsan) 组=1002(zhangsan)
[root@som ~]# id dachui								#判断用户是否存在
id: “dachui”：无此用户
```

## 创建用户-增

- 使用useradd命令

- 格式：useradd   [选项]   用户名

- 常用命令选项
  - -u	指定用户id
  - -d    指定家目录路径
  - -s     指定登录解释器
  - -G    指定用户附加组

```shell
[root@som ~]# useradd som01							#创建用户som01
[root@som ~]# grep som01 /etc/passwd				#查看/etc/passwd是否多出了用户som01
[root@som ~]# id som01								#查看用户som01是否存在
uid=1005(som01) gid=1005(som01) 组=1005(som01)
[root@som ~]# useradd som02							#创建用户som02
[root@som ~]# id som02								#查看用户som02是否存在
```

指定用户UID创建用户

```shell
[root@som ~]# useradd -u 1100 som03					#创建UID为1100的用户som03
[root@som ~]# id som03								#查看验证	
uid=1100(som03) gid=1100(som03) 组=1100(som03)
```

指定用户家目录创建目录

```shell
[root@som ~]# useradd -d /opt/som04 som04		#创建som04用户，其家目录为/opt/som04
[root@som ~]# grep som04  /etc/passwd			#查看验证是否多出来了som04用户
som04:x:1101:1101::/opt/som04:/bin/bash				
[root@som ~]# ls /opt/							#查看som04用户家目录
```

指定用户解释器

- /sbin/nologin，如果用户的解释器为/sbin/nologin，那么该用户无法登录系统

```shell
[root@som ~]# useradd -s /sbin/nologin som05	#创建用户som05并指定解释器
[root@som ~]# grep som05 /etc/passwd			#查看验证
som05:x:1103:1103::/home/som05:/sbin/nologin
```

指定用户附加组

- groupadd   组名，可以专门创建一个组

```shell
[root@som ~]# groupadd tarena				#创建组tarena
[root@som ~]# useradd -G tarena som06		#创建som06用户并添加到tarena组里面
[root@som ~]# id som06						#验证
uid=1104(som06) gid=1105(som06) 组=1105(som06),1104(tarena)
```

指定基本组

```shell
[root@som ~]# useradd -g tarena som07		#创建som06用户指定基本组为tarena
[root@som ~]# id som07						#验证
uid=1104(som07) gid=1105(tarena) 组=1104(tarena)
```
## 修改用户属性-改

- 使用usermod命令
- 只能操作已存在的用户
- 格式：usermod  [选项]   用户
  - -u	指定用户id
  - -d    指定家目录路径
  - -s     指定登录解释器
  - -G    指定用户附加组(重置附加组)

```shell
[root@som ~]# useradd alex					#创建用户alex
[root@som ~]# usermod -u 1200 alex			#修改用户alex的UID为1200
```

```shell
[root@som ~]# usermod -s /bin/sh alex		#修改用户alex的解释器为/bin/sh
[root@som ~]# grep alex  /etc/passwd		#查看用户alex的信息
```

```shell
[root@som ~]# groupadd mygroup 				#添加mygroup组
[root@som ~]# usermod -G tarena alex		#将alex附加组设置为tarena
[root@som ~]# id alex 						#查看验证
uid=1004(alex) gid=1004(alex) 组=1004(alex),1003(tarena)
[root@som ~]# usermod -G mygroup alex		#将alex附加组设置为mygroup
[root@som ~]# id  alex 						#查看验证，-G其实是重置附加组
uid=1004(alex) gid=1004(alex) 组=1004(alex),1005(mygroup)
```

```shell
[root@som ~]# grep alex /etc/passwd		#查看当前alex的家目录
alex:x:1004:1004::/home/alex:/bin/bash		
[root@som ~]# usermod -d /opt/alex alex	#修改alex用户家目录为/opt/alex 
[root@som ~]# grep alex /etc/passwd		#查看验证，alex用户家目录已经修改为/opt/alex
alex:x:1004:1004::/opt/alex:/bin/bash
[root@som ~]# ls /opt/				#但是/opt/下没有alex家目录，因为usermod -d不会创建目录
```

## 删除用户-删

- 删除用户：userdel
  - -r：连同用户家目录、信箱一并删除

```shell
[root@som ~]# userdel  som03				#删除用户som03，家目录、信箱不删除
[root@som ~]# userdel -r som04				#删除用户som04，家目录、信箱一并删除
```

# 用户密码管理

- 记录用户密码信息的文件：/etc/shadow
- 文件格式如下

<font color='red'>用户名</font>:<font color='blue'>加密密码</font>:<font color='orange'>从1970年1月1日到最近一次修改密码时间</font>:<font color='pink'>密码最短有效天数，默认为0</font>:<font color='purple'>密码最长有效天数，默认99999</font>:<font color=green>密码过期前警告天数，默认是7</font>:<font color=yellow>密码过期后多少天禁用此账户</font>:账号失效天数，默认是空:保留字段(未使用)

## 交互式修改密码

- 使用passwd命令修改密码
- 格式：passwd  用户名
- 只有root用户才能指定用户修改密码
- passwd直接回车则代表为当前登录用户修改密码
- 普通用户修改密码需要满足策略，root用户可以忽略密码策略

```shell
[root@som ~]# passwd lisi						#修改lisi用户的密码
更改用户 lisi 的密码 。
新的 密码：										 #为了安全，输入密码不显示
无效的密码： 密码少于 8 个字符
重新输入新的 密码：								  #为了安全，输入密码不显示
passwd：所有的身份验证令牌已经成功更新。
```

## 非交互式修改密码

- 格式：echo 密码 | passwd  --stdin  用户名

```shell
[root@som ~]# echo 123 | passwd --stdin zhangsan		#修改用户zhangsan用户密码为123
```

# 用户初始配置文件

- 新建用户是，根据/etc/skel模板目录复制内容至用户家目录下

- 主要的初始配置文件

  - ~/.bash_profile: 每次登录时执行
  - ~/.bashrc: 每次进入新的bash环境时执行
  - ~/.bash_logout：每次退出登录时执行

  - 全局配置文件：/etc/bashrc、/etc/profile

root用户使用~/.bashrc文件永久定义别名

```shell
[root@som ~]# vim ~/.bashrc
...
alias hn='hostname'
...
```

# 基本权限和归属

## 基本权限的类别

- 读取：允许查看内容-read   <font color='red'>r</font>  
- 写入：允许修改内容-write    <font color='red'>w</font>
- 可执行：允许运行和切换-execute <font color='red'> x</font>

常见报错提示：<font color='red'>Permission denied :权限不足</font>

 对于<font color='red'>文本文件</font>，拥有相应权限能做操作哪些命令(举例)：

​     r 读取权限：cat  less  head tail 

​     w 写入权限：vim    >   >>

​     x 可执行权限: Shell脚本编写时可以赋予

## 权限适用对象(归属)

- 所有者:拥有此文件/目录的用户-user  <font color='red'>u</font>
- 所属组:拥有此文件/目录的组-group    <font color='red'>g</font>
- 其他用户:除所有者、所属组以外的用户-other  <font color='red'>o</font>

## 查看权限

- 查看文件权限: ls -l  文件1  文件2
- 查看目录权限: ls  -ld   目录1  目录2  ...                   

​     以 <font color='red'>- 开头</font>： 文本文件

​     以 <font color='red'>d 开头</font>：目录

​     以 <font color='red'>l 开头</font>： 快捷方式

```shell
[root@som ~]# ls -ld /etc/						#查看/etc/目录权限
[root@som ~]# ls -l /etc/rc.local					#查看/etc/rc.local权限
[root@som ~]# ls -l /etc/passwd					#查看/etc/passwd权限
```

![1687693817118](Day05.assets/1687693817118.png)

# 修改权限

## 设置基本权限

- 使用 chmod 命令
- 命令格式: chmod [-R]  归属关系+-=权限类别    文档...
  - -R：递归修改

```shell
[root@som ~]# mkdir /som01						#创建素材目录
[root@som ~]# ls -ld /som01						#查看/som01目录权限
drwxr-xr-x. 2 root root 6 2月  17 12:54 /som01
```

取消/som01目录所有者的w权限

```shell
[root@som ~]# chmod u-w /som01					#u-w权限
[root@som ~]# ls -ld /som01						#查看权限
```

/som01目录所有者怎加w权限

```shell
[root@som ~]# chmod u+w /som01					#u+2权限
[root@som ~]# ls -ld /som01						#查看权限
```

修改/som01目录所属组为只读权限

```shell
[root@som ~]# chmod g=r /som01					#g=r权限
[root@som ~]# ls -ld /som01						#查看权限
```

修改/som01目录其他人没有任何权限

```shell
[root@som ~]# chmod o=--- /som01				#其他人取消所有权限
[root@som ~]# ls -ld /som01						#查看权限
```

同时设置权限

```shell
[root@som ~]# chmod u=rwx,o=rx /som01			#所有者权限为rwx,其他人权限为rx
[root@som ~]# ls -ld /som01						#查看权限
[root@som ~]# chmod u=rwx,g=rx,o=rx /som01		#所有者权限为rwx,所属组/其他人权限为rx
[root@som ~]# chmod ugo=rwx /som01				#所有者/所属组/其他人权限为rwx
[root@som ~]# ls -ld /som01						#查看权限
```

-R递归修改权限

```shell
[root@som ~]# mkdir -p /opt/aa/bb/cc			#递归创建/opt/aa/bb/cc
[root@som ~]# ls -lR /opt/						#递归查看属性
```

查看aa，bb，cc(目录其他人的权限都是rx)

```shell
[root@som ~]# ls -ld /opt/aa/
[root@som ~]# ls -ld /opt/aa/bb/
[root@som ~]# ls -ld /opt/aa/bb/cc/
```

递归修改权限，目录本身包括此目录里面会发生变化

```shell
[root@som ~]# chmod  -R o=--- /opt/aa/			#递归设置/opt/aa其他人没有任何权限
[root@som ~]# ls -ld /opt/aa/					#查看验证
[root@som ~]# ls -ld /opt/aa/bb/
[root@som ~]# ls -ld /opt/aa/bb/cc/
```

### 如何判断用户对某目录所具备怎样的权限

```shell
[root@som ~]# chmod  g=rx,o=rx /som01/				#设置权限
[root@som ~]# ls -ld /som01/						#查看权限
[root@som ~]# useradd zhangsan						#创建zhangsan用户
drwxr-xr-x. 2 root root 6 7月   1 03:56 /som01/			
[root@som ~]# id zhangsan							#判断用户归属关系
uid=1004(zhangsan) gid=1004(zhangsan) 组=1004(zhangsan)
```

### 实验不同用户写入文件：

```shell
[root@som ~]# mkdir /som02							#创建/som02目录
[root@som ~]# echo 123 > /som02/a.txt				#创建/som02/a.txt文件内容为123
```

zhangsan用户测试

```shell
[root@som ~]# su - zhangsan 						#切换用户
[zhangsan@som ~]$ cat /som02/a.txt    				#可以查看
123
[zhangsan@som ~]$ echo haha > /som02/a.txt     		#写入haha失败
-bash: /som02/a.txt: 权限不够
[zhangsan@som ~]$ exit								#退回到root用户
[root@som ~]# chmod  o+w /som02/a.txt           	#赋予w的权限
[root@som ~]# su - zhangsan 						#切换用户zhangsan
[zhangsan@som ~]$ echo haha >> /som02/a.txt       	#写入成功
[zhangsan@som ~]$ cat /som02/a.txt					#可以验证
123
haha
```

## 实验不同用户操作目录：

```shell
[root@som ~]# ls -ld /som02/						#查看权限
drwxr-xr-x. 2 root root 19 6月  28 13:22 /som02/		
[root@som ~]# id zhangsan							#查看zhangsan用户归属
uid=1004(zhangsan) gid=1004(zhangsan) 组=1004(zhangsan)
```

zhangsan用户测试

```shell
[root@som ~]# su - zhangsan 					#切换用户zhangsan
[zhangsan@som ~]$ ls -l /som02/            		#可以查看
[zhangsan@som ~]$ cd /som02               	 	#可以切换目录
[zhangsan@som som02]$ exit
```

```shell
[root@som ~]# chmod o-x /som02					#其他人取消x权限
[root@som ~]# su - zhangsan 					#切换zhangsan用户	
[zhangsan@som ~]$ cd /som02                		#切换失败
-bash: cd: /som02: 权限不够
[zhangsan@som ~]$ ls /som02                 		#查看虽然显示，但是也异常
ls: 无法访问/som02/a.txt: 权限不够
a.      txt
```

在目录下写入内容，但是修改的是目录里面的内容，对目录本身没有修改权限：

```shell
[root@som ~]# chmod o=rwx /som02
[root@som ~]# su - zhangsan 
[zhangsan@som ~]$ cd  /som02
[zhangsan@som som02]$ touch zs.txt				#创建成功
[zhangsan@som som02]$ mkdir zs					#创建成功
[zhangsan@som som02]$ exit
```

如果想要对目录本身有修改的权限，找此目录的父目录即可：

```shell
[zhangsan@som som02]$ cd
[zhangsan@som ~]$ mv /som02 /stu02				#修改失败
mv: 无法将"/som02" 移动至"/stu02": 权限不够
[root@som ~]# su - zhangsan 
[root@som ~]# chmod o+w /          				#修改/的权限
[root@som ~]# su - zhangsan 
[zhangsan@som ~]$ mv /som02 /stu02				#修改成功
[zhangsan@som ~]$ ls /
```

### 小结

目录的 r 权限：能够 ls 浏览此目录内容

目录的 w 权限：能够执行 rm/mv/cp/mkdir/touch/等更改目录内容的操作

目录的 x 权限：能够 cd 切换到此目录

## 权限位的8进制数表示

![1687693896117](Day05.assets/1687693896117.png)

7:rwx  6:rw-  5:r-x  4:r--  3:-wx  2:-w-  1:--x  0:---

数字的形式修改权限：

```shell
[root@som ~]# mkdir /som03
[root@som ~]# ls -ld /som03
```

```shell
[root@som ~]# chmod  750 /som03
[root@som ~]# ls -ld /som03
[root@som ~]# chmod  700 /som03
```

## 修改归属

### 设置归属关系

- 使用 chown 命令，-R递归设置
  - chown  [-R]  属主      文档...
  - chown  [-R]  :属组      文档...
  - chown  [-R]  属主:属组  文档...

同时修改所有者（属主）和所属组（属组）

```shell
[root@som ~]# mkdir /ansible 				#创建/ansible
[root@som ~]# ls -ld /ansible				#查看权限
[root@som ~]# groupadd  stugrp				#创建组stugrp
[root@som ~]# useradd dc					#创建dc用户
[root@som ~]# chown dc:stugrp /ansible  	#修改/ansible目录的所有者为dc,所属组为stugrp
[root@som ~]# ls -ld /ansible				#查看归属
```

```shell
[root@som ~]# chown zhangsan /ansible     	#仅/ansible目录的修改所有者为zhangsan
[root@som ~]# ls -ld /ansible				#查看归属
```

只修改所属组

```shell
[root@som ~]# chown :root /ansible/      	#仅修改所属组
[root@som ~]# ls -ld /ansible				#查看归属
```

递归修改归属关系

```shell
[root@som ~]# chown -R dc /opt/aa         	#递归修改/opt/aa的所有者为dc
[root@som ~]# ls -ld /opt/aa				#验证
[root@som ~]# ls -ld /opt/aa/bb/
[root@som ~]# ls -ld /opt/aa/bb/cc/
```

### 实验

```shell
[root@som ~]# mkdir /som05
[root@som ~]# chmod g=rwx,o=--- /som05		#设置目录权限
[root@som ~]# chown  zhangsan:stugrp /som05
[root@som ~]# ls -ld /som05            		#zhangsan用户有读写执行的权限，其他用户没有权限
```

测试

```shell
[root@som ~]# su - zhangsan
[zhangsan@som ~]$ mkdir /som05/aa
[zhangsan@som ~]$ exit
```

```shell
[root@som ~]# su - lisi						#切换lisi用户
[lisi@som ~]$ mkdir /som05/lisi
mkdir: 无法创建目录"/som05/lisi": 权限不够
[lisi@som ~]$ exit							#退出
```

可以把lisi加入到组里面，继承组的权限

```shell
[root@som ~]# usermod -G stugrp lisi		#将lisi用户加入到stugrp组
[root@som ~]# id lisi
[root@som ~]# su - lisi						#切换lisi用户
[lisi@som ~]$ mkdir /som05/lisi				#可以创建目录
```

去掉所有者的执行权限，zhangsan添加到stugrp组里面，虽然组里面有执行权限，但是依然不能cd

```shell
[root@som ~]# chmod  u-x /som05/			#所有者去掉x权限
[root@som ~]# usermod -G stugrp zhangsan	#将zhangsan加入stugrp组		
[root@som ~]# su - zhangsan
[zhangsan@som ~]$ cd /som05/
-bash: cd: /som05/: 权限不够
```

总结: 权限判断步骤

1、判断用户身份(所有者>所属组>其他人 匹配即停止)

2、查看相应身份的权限

# 总结

- 掌握用户增删改查
- 掌握LINUX下修改密码的方式
- 掌握LINUX系统基本权限
- 掌握修改权限的方式
- 掌握修改归属关系的方式