[TOC]

# Prometheus简介

- Prometheus是一个开源系统监控和警报工具包，最初由 SoundCloud构建。
- 也是一款监控软件，也是一个时序数据库。Prometheus 将其指标收集并存储为时间序列数据，即指标信息与记录时的时间戳以及称为标签的可选键值对一起存储。
- 主要用在容器监控方面，也可以用于常规的主机监控。
- 使用google公司开发的go语言编写。
- Prometheus是一个框架，可以与其他组件完美结合

![1689003203404](Day15.assets/1689003203404.png)

# 环境准备

| 主机名     | IP地址                          |
| ---------- | ------------------------------- |
| prometheus | 192.168.8.100（vmnet8网络模式） |
| node1      | 192.168.8.101（vment8网络模式） |

- 重新安装一台CentOS7.9图形版虚拟机作为模板机、
- 关闭防火墙和SELinux

- 模板机机需要使用 centos7.9的镜像创建

- 英文安装，时区选择亚洲上海，时间和自己真机保持一致

由于装机大家已经很熟练了，这里不再做展示，如果安装出现问题，可参阅之前笔记

下方罗列本次装机需要注意的事项

![1689003626048](Day15.assets/1689003626048.png)

![1689003677604](Day15.assets/1689003677604.png)

## 配置模板机环境

```shell
[root@localhost ~]# systemctl stop firewalld				#停止防火墙
[root@localhost ~]# systemctl disable firewalld				#将防火墙设置为开机不自启
```

```shell
[root@localhost ~]# vim /etc/selinux/config
...
7 SELinux=disabled						#将SELinux状态改为disabled禁用
...
[root@localhost ~]# poweroff			#关闭虚拟机
```

## 部署阿里镜像源

- 阿里云镜像站点，主要提供各种开发者资源和开源软件的镜像下载
  - https://developer.aliyun.com/mirror/
- wget为Linux下载命令
  - -O表示将数据另存为制定路径

模板机操作

```shell
[root@localhost ~]# rm -rf /etc/yum.repos.d/*.repo	 	#删除自带的repo文件
[root@localhost ~]# wget -O /etc/yum.repos.d/CentOS-Base.repo \ https://mirrors.aliyun.com/repo/Centos-7.repo		     #下载阿里镜像源
```



## 实验环境准备

使用模板机<font color='red'>链接克隆prometheus、node1</font>

prometheus主机操作（修改主机名及配置IP地址，网卡名不要照抄）

```shell
    [root@localhost ~]# hostnamectl set-hostname prometheus
    [root@prometheus ~]# nmcli connection modify ens33 ipv4.method auto connection.autoconnect yes
    [root@prometheus ~]# nmcli connection up ens33
```

node1主机操作（修改主机名及配置IP地址，网卡名不要照抄）

```shell
[root@localhost ~]# hostnamectl set-hostname node1
[root@node1 ~]# nmcli connection modify ens33 ipv4.method auto connection.autoconnect yes
[root@node1 ~]# nmcli connection up ens33
```

两台主机使用MobaXterm远程连接

将 <font color='red'>prometheus_soft.tar.gz</font> 压缩包拷贝至 promethues主机的/root 家目录下

# 部署prometheus服务

```shell
[root@prometheus ~]# tar -xf prometheus_soft.tar.gz 
[root@prometheus ~]# cd prometheus_soft/
]# tar -xf prometheus-2.17.2.linux-386.tar.gz 
]# mv prometheus-2.17.2.linux-386 /usr/local/prometheus
```

修改配置文件，让 promethues 自己监控自己

```yml
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml
....
static_configs:
 - targets: ['192.168.8.100:9090'] 				#修改 IP，指定自己监控自己
```

检查配置文件是否修改正确（\表示一行写不下，折行写）

```yml
[root@prometheus ~]# /usr/local/prometheus/promtool check config  \ /usr/local/prometheus/prometheus.yml
SUCCESS: 0 rule files found
```

默认prometheus启动服务是很复杂的，如果想利用systemd更快速更方便的管理prometheus
服务，就需要编写服务文件，让 systemd 管理

```shell
[root@prometheus ~]# vim /usr/lib/systemd/system/prometheus.service  #编辑服务配置文件
[Unit]
Description=Prometheus Monitoring System
Documentation=Prometheus Monitoring System
[Service]
ExecStart=/usr/local/prometheus/prometheus \
--config.file=/usr/local/prometheus/prometheus.yml \
--storage.tsdb.path=/usr/local/prometheus/data
[Install]
WantedBy=multi-user.target
```

```shell
[root@prometheus ~]# systemctl enable prometheus.service --now #设置为开启自启并立即启动服务
[root@prometheus ~]# systemctl status prometheus.service
[root@prometheus ~]# ss -ntulp | grep 9090				#Prometheus服务监听9090端口
tcp LISTEN 0 128 :::9090 :::* users:(("prometheus",pid=10885,fd=8))
```

## 查看及测试

- 通过浏览器访问 prometheus 的 web 监控页面，查看监控数据
- 访问：http://192.168.8.100:9090（不要照抄，以自己的IP为准）

![1689027457981](Day15.assets/1689027457981.png)



![1689027486458](Day15.assets/1689027486458.png)



- 用真机访问 prometheus 时，会出现时差问题；
- 用 prometheus 本机的浏览器访问测试，时差问题就可以解决。

```shell
[root@prometheus ~]# firefox http://192.168.8.100:9090
```

![1689027515377](Day15.assets/1689027515377.png)

![1689027531678](Day15.assets/1689027531678.png)

![1689027548569](Day15.assets/1689027548569.png)

# Promethues 被监控端

- 先将 <font color='red'>prometheus_soft.tar.gz</font> 压缩包拷贝至 node1 的/root 家目录下，然后安装被监控端	

部署被监控端（node1）

```shell
[root@node1 ~]# tar -xf prometheus_soft.tar.gz 
[root@node1 ~]# cd prometheus_soft/
[root@node1 prometheus_soft]# tar -xf node_exporter-1.0.0-rc.0.linux-amd64.tar.gz 
]# mv node_exporter-1.0.0-rc.0.linux-amd64 /usr/local/node_exporter
[root@node1 prometheus_soft]# ls /usr/local/node_exporter/
LICENSE node_exporter NOTICE
```

如果想更好更快的管理 node_exporter 导出器服务，需要编写 service 服务文件，让 systemd
进行管理

```shell
[root@node1 ~]# vim /usr/lib/systemd/system/node_exporter.service
[Unit]
Description=node_exporter
After=network.target
[Service]
Type=simple
ExecStart=/usr/local/node_exporter/node_exporter
[Install]
WantedBy=multi-user.target
```

```shell
[root@node1 ~]# systemctl enable node_exporter --now
[root@node1 ~]# systemctl status node_exporter
[root@node1 ~]# ss -utnlp | grep node_exporter
tcp LISTEN 0 128 :::9100 :::* 
users:(("node_exporter",pid=11222,fd=3))
```

prometheus服务端修改监控端服务器配置

```shell
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml 
......
 - job_name: 'node1' 						#定义监控任务名字，可以任意名称
 static_configs:
 - targets: ['192.168.8.101:9100'] 			#被监控端主机和端口
```

```shell
[root@prometheus ~]# systemctl restart prometheus	#重启服务，再查看web页面信息
```

![1689027659609](Day15.assets/1689027659609.png)

![1689027675122](Day15.assets/1689027675122.png)

![![1689027707327](Day15.assets/1689027707327.png)

# Grafana简介

- Grafana是一种开源的数据可视化和监控平台，用于实时分析和可视化大规模的指标数据。它提供了丰富的数据可视化工具和面板，可以轻松地创建交互式仪表板来监控、查询和分析多种数据源的数据。Grafana支持广泛的数据源，包括Graphite、InfluxDB、<font color='red'>Prometheus</font>、Elasticsearch、MySQL等。用户可以通过Grafana的灵活的查询编辑器和插件系统，自定义数据查询和展示方式，从而满足各种不同的数据分析和监控需求。Grafana还具有强大的告警功能，可以及时通知用户关于数据异常或者预先设定的指标阈值的超出情况。Grafana简单易用、功能强大，已经成为开源社区和企业中常用的数据可视化和监控平台之一。

## 部署 Grafana 服务器

- Grafana 可以在任意主机部署，我们的规划是在监控服务器 prometheus 上安装部署 Grafana
- Grafana默认端口：3000

```shell
[root@prometheus ~]# cd prometheus_soft/
[root@prometheus prometheus_soft]# ls
.....
grafana-6.7.3-1.x86_64.rpm
[root@prometheus prometheus_soft]# yum -y install grafana-6.7.3-1.x86_64.rpm
[root@prometheus ~]# systemctl enable grafana-server --now
[root@prometheus ~]# systemctl status grafana-server
[root@prometheus ~]# ss -utnlp | grep grafana-server
```

- 在启动 grafana 服务后，可以通过浏览器访问 grafana 的 web 的页面，注意需要加上 3000 端
  口访问，默认的的初始用户名和密码都是 admin，但浏览器如果和 Grafana 版本不兼容，我
  们修改新密码就会失败
- 浏览器访问：http://192.168.8.100:3000

![1689028029810](Day15.assets/1689028029810.png)

![1689028057930](Day15.assets/1689028057930.png)

![1689028074933](Day15.assets/1689028074933.png)

- 如果无法修改密码，就需要修改配置文件，设置成可以通过匿名访问的方式，再重新访问 Grafana 页面

```shell
[root@prometheus ~]# vim /etc/grafana/grafana.ini 
304 [auth.anonymous]
306 enabled = true 				#启用匿名访问
312 org_role = Admin 			#以管理员的身份登录
[root@prometheus ~]# systemctl restart grafana-server		#重启服务
```

![1689028141285](Day15.assets/1689028141285.png)

## 修改数据源

- 如果需要 Grafana 能够显示 prometheus 监控的数据，通过数据绘制成图形，需要将
  prometheus 监控的数据作为数据源添加到 Grafana 里面
- 点击 Add data source，添加数据源

![1689028224833](Day15.assets/1689028224833.png)

选择Prometheus作为数据源

![1689028261745](Day15.assets/1689028261745.png)

给数据源进行命名，并设置为默认数据源，然后设置数据源来源地址，最后保存配置

![1689028288109](Day15.assets/1689028288109.png)

![1689028299474](Day15.assets/1689028299474.png)

![1689028309176](Day15.assets/1689028309176.png)

保存成功之后，可以点击齿轮图标，选择 data sources，查看数据源，这样就有了数据的来源：

![1689028339725](Day15.assets/1689028339725.png)

- 在有了数据源之后，想让 Grafana 通过数据绘制出图形，这个时候还需要导入数据模板，这
  里 Grafana 也自带了一个模板，我们也可以尝试导入，然后查看绘制的图形，在这里也需要
  知道，不同的数据模板最终绘制的图形以及显示的数据信息也都不一样

![1689028379866](Day15.assets/1689028379866.png)

![1689028394064](Day15.assets/1689028394064.png)

![1689028412547](Day15.assets/1689028412547.png)

![1689028424893](Day15.assets/1689028424893.png)

- 默认的模板无法查看被监控主机的信息；
- 如何还想查看被监控主机node1主机node_exporter的内存、CPU 等信息，这里也需要导入 node_exporter 导出器的模板（模板文件运维工程师无法自己编写，涉及到开发，如果想要其他模板，需要到官网上下载）：

![1689028473077](Day15.assets/1689028473077.png)

![1689028486644](Day15.assets/1689028486644.png)

![1689028499697](Day15.assets/1689028499697.png)

![1689028512118](Day15.assets/1689028512118.png)

![1689028529306](Day15.assets/1689028529306.png)

最后可以将该面板保存下来

![1689028546349](Day15.assets/1689028546349.png)

![1689028558815](Day15.assets/1689028558815.png)

![1689028568046](Day15.assets/1689028568046.png)

# 监控数据库

## 安装部署Mariadb

将 node1 主机搭建成 Mariadb 数据服务器，配置账户和密码

```shell
[root@node1 ~]# yum -y install mariadb mariadb-server
[root@node1 ~]# systemctl enable mariadb --now 			#设置为开启自启，并立即启动服务
[root@node1 ~]# mysql
MariaDB [(none)]> GRANT ALL ON *.* TO tom@'127.0.0.1' IDENTIFIED BY '123';
MariaDB [(none)]> EXIT;
```

## 安装导出器

- 如果想让 prometheus 能够监控 node1 主机的数据库，就需要安装数据库相关的导出器，
- 安装 mysqld_exporter 导出器，并修改导出器的配置文件，指定数据库的用户名、密码、端
  口、主机信息，这样导出器才能获取本机的数据库的状态信息
- mysqld_exporter默认端口号：9104

```shell
[root@node1 ~]# cd prometheus_soft/
[root@node1 prometheus_soft]# tar -xf mysqld_exporter-0.12.1.linux-amd64.tar.gz 
]# mv mysqld_exporter-0.12.1.linux-amd64 /usr/local/mysqld_exporter
[root@node1 prometheus_soft]# ls /usr/local/mysqld_exporter/
LICENSE mysqld_exporter NOTICE
[root@node1 prometheus_soft]# vim /usr/local/mysqld_exporter/.my.cnf
[client]
host=127.0.0.1
port=3306
user=tom
password=123
```

```shell
[root@node1 ~]# vim /usr/lib/systemd/system/mysqld_exporter.service
[Unit]
Description=mysqld_exporter
After=network.target
[Service]
ExecStart=/usr/local/mysqld_exporter/mysqld_exporter \
--config.my-cnf=/usr/local/mysqld_exporter/.my.cnf
[Install]
WantedBy=multi-user.target
```

```shell
[root@node1 ~]# systemctl enable mysqld_exporter --now   #设置开机自启，并立即启动服务
[root@node1 ~]# systemctl status mysqld_exporter
[root@node1 ~]# ss -nutlp | grep mysqld_exporter		 #默认监听9104
tcp LISTEN 0 128 :::9104 :::* 
users:(("mysqld_exporter",pid=11807,fd=3))
```

## 修改 Prometheus服务端配置

- 如果要将 node1 的数据库进行监控，需要在 prometheus 上添加 node1 数据库监控相关的配
  置

```yml
[root@prometheus ~]# vim /usr/local/prometheus/prometheus.yml 
......
 - job_name: 'node1_mysql'
 static_configs:
 - targets: ['192.168.4.11:9104'] 		#指定 node1 导出器服务端口和地址
[root@prometheus ~]# systemctl restart prometheus #重启服务让配置生效
```

## Grafana配置

- 如果想在 Grafana 中展示 node1 的数据库监控状态信息，也需要在 Grafana 中导入数据库的
  相关模板，用于展示数据库的状态信息

![1689028928706](Day15.assets/1689028928706.png)

![1689028938777](Day15.assets/1689028938777.png)

![1689028949321](Day15.assets/1689028949321.png)

![1689028967069](Day15.assets/1689028967069.png)

![1689028975084](Day15.assets/1689028975084.png)

![1689028982897](Day15.assets/1689028982897.png)

# 总结

- 掌握Prometheus使用场景及工作原理
- 掌握Prometheus部署方式
- 掌握exporter作用
- 掌握Prometheus监控服务器硬件及数据库服务
- 掌握Grafana可视化工具