[TOC]

# 基本命令使用

本节实验需要一台CentOS7.9的虚拟机，将其开机并使用root用户登录即可！

## 查看文本文件内容-cat命令

格式：cat    \[选项\]    目标文件

查看/etc/passwd文件

```shell
[root@localhost ~]# cat /etc/shells
```

查看/etc/centos-release文件

```shell
[root@localhost ~]# cat /etc/centos-release
```

查看/etc/passwd文件

```shell
[root@localhost ~]# cat /etc/passwd
```

查看/proc/meminfo文件(该文件用于记录当前系统内存信息)

```shell
[root@localhost ~]# cat /proc/meminfo 
MemTotal:        1828200 kB						#总内存
MemFree:         126980 kB						#剩余内存
```



## 分页查看文本文件-less命令

格式: less   目标文件

- less查看文件内容为交互式查看，在交互式模式中：

  - 按<font color='red'>/</font>表示搜索关键词，按n查找下一个、按N查找上一个

  - 按<font color='red'>空格</font>以页为单位翻页浏览，按<font color='red'>回车</font>以行为单位翻页浏览

  - 按<font color='red'>q</font>退出交互式界面

分页查看/etc/passwd文件

```shell
[root@localhost ~]# less /etc/passwd
```

## 查看CPU信息-lscpu命令

格式：lscpu

```shell
[root@localhost ~]# lscpu
```

## 查看系统内核版本-uname命令

格式：uname  \[选项\]

 -r	 输出内核发行号

```shell
[root@localhost ~]# uname -r
```

## 查看机修改主机名-hostname命令

格式：hostname   \[主机名\]

- 该方式修改主机名为临时生效
- 提示符中的主机名只会显示主机名第一个小数点左边的内容(例如a.b.c，只会在提示符中显示a)
- 修改主机名后，提示符上不会马上显示，关闭终端重新打开即可

查看主机名

```shell
[root@localhost ~]# hostname
```

修改主机名为som.tedu.cn

```shell
[root@localhost ~]# hostname som.tedu.cn		#修改主机名，并重新打开终端
[root@som ~]# 									#提示符已发生改变
[root@som ~]# hostname							#查看完整主机名
```

## 查看IP地址-ifconfig命令

格式：ifconfig [网卡]

```shell
[root@som ~]# ifconfig							#查看IP地址
[root@som ~]# ifconfig ens160					#指定网卡查看IP地址
```

## 创建目录-mkdir命令

格式：mkdir   \[选项\]    [/路径/]目录名 ...

-p：连同父目录一并创建

创建/opt/som01目录

```shell
[root@som ~]# mkdir /opt/som01
```

创建/opt/aa/bb/cc/dd

```shell
[root@som ~]# ls /opt/									#查看/opt/没有aa目录
a.txt  som01
[root@som ~]# mkdir /opt/aa/bb/cc/dd					#此时直接创建报错
mkdir: 无法创建目录 “/opt/aa/bb/cc/dd”: 没有那个文件或目录
[root@som ~]# mkdir -p /opt/aa/bb/cc/dd					#加上-p选项，成功创建
[root@som ~]# ls -R /opt/aa/							#递归查看/opt/aa
```

## 创建空文件-touch命令

格式：touch  [选项]   目标文件 ...

创建空文件/opt/1.txt、/opt/2.txt

```shell
[root@som ~]# touch  /opt/1.txt
[root@som ~]# touch  /opt/2.txt
```

## 查看文件前几行-head命令

格式：head  [-n]  行数  目标文件

- -n 选项也可以省略，直接写<font color='red'>-行数</font>
- 当没有指定看前几行时间，默认看前10行

查看/etc/passwd文件的前2行

```shell
[root@som ~]# head -n 2 /etc/passwd
[root@som ~]# head -2 /etc/passwd
```

查看/etc/passwd文件的前10行

```shell
[root@som ~]# head -n 10 /etc/passwd
[root@som ~]# head -10 /etc/passwd
[root@som ~]# head  /etc/passwd
```

## 查看文件后几行-tail命令

格式：tail  [-n]  行数  目标文件

- -n 选项也可以省略，直接写<font color='red'>-行数</font>
- 当没有指定看前几行时间，默认看后10行

查看/etc/group文件后2行

```shell
[root@som ~]# tail -n 2 /etc/group
[root@som ~]# tail -2 /etc/group
```

查看/etc/group文件后10行

```shell
[root@som ~]# tail -n 10  /etc/group
[root@som ~]# tail -10  /etc/group
[root@som ~]# tail   /etc/group
```

## 快速编辑技巧

- <font color='red'>Tab键补齐</font>：命令、选项、参数、文件路径、软件名、服务名...
- 快捷键
  - <font color='red'>Ctrl l</font>：清空整个屏幕
  - <font color='red'>Ctrl c</font>：废弃当前编辑的命令行操作
  - <font color='red'>Esc .</font> 或者<font color='red'>ALT .</font>:粘贴上一个命令的参数

## 关机及重启

重启：reboot

关机：poweroff

```shell
[root@som ~]# reboot					#重新启动
[root@som ~]# poweroff					#关闭机器
```

## 别名管理-alias/unalias

作用：别名相当于生活中的的<font color='red'>"外号"</font>，<font color='red'>用于将复杂的命令简单化</font>

格式：alias  [别名='真实命令']

- 查看当前系统已有别名直接alias
- alias定义别名是临时生效的
- 一般不要把其他命令关键字作为别名(例如将ls作为其他命令的别名)，因为别名优先级更高
- 取消别名: unalias  别名

查看当前已有别名

```shell
[root@som ~]# alias 
```

将hn定义为hostname的别名

```shell
[root@som ~]# alias hn='hostname'			#定义hn为hostname的别名
[root@som ~]# alias 						#查看已有别名
[root@som ~]# hn							#验证别名是否生效
```

取消别名hn

```shell
[root@som ~]# unalias  hn					#取消别名hn
[root@som ~]# alias 						#查看是否取消别名hn
[root@som ~]# hn							#查看hn是否生效
```

## 删除操作-rm命令

格式：rm \[选项\]  参数 ...]

-i  提示是否删除

-r  递归删除

-f  强制删除

- rm 默认是rm -i的别名
- <font color='red'>-f优先级大于-i</font>，所以当-f和-i同时出现时，优先使用-f选项的功能

使用rm命令删除文件

```shell
[root@som ~]# touch /opt/test.txt			#创建文件
[root@som ~]# rm /opt/test.txt 				#直接删除文件
rm：是否删除普通空文件 '/opt/test.txt'？y		 #会有提示(回答y表示删除，回答n表示不删除)
[root@som ~]# ls  /opt/						#查看/opt/是否还有test.txt

[root@som ~]# touch /opt/test2.txt			#创建/opt/test2.txt
[root@som ~]# rm -f /opt/test2.txt 			#强制删除/opt/test2.txt
[root@som ~]# ls /opt/						#查看/opt/是否还有test2.txt
```

使用rm命令删除目录

```shell
[root@som ~]# mkidr -p /opt/aa/bb/cc/dd	#创建目录
[root@som ~]# rm /opt/aa/					#直接删除报错，想要删除目录必须加上-r
rm: 无法删除'/opt/aa/': 是一个目录
[root@som ~]# ls /opt/						#查看/opt/aa目录还在

[root@som ~]# rm -r /opt/aa/				#使用-r选项递归删除(因为是rm -i的别名所以有提示)
rm：是否进入目录'/opt/aa/'? y
rm：是否进入目录'/opt/aa/bb'? y
rm：是否进入目录'/opt/aa/bb/cc'? y
rm：是否删除目录 '/opt/aa/bb/cc/dd'？y
rm：是否删除目录 '/opt/aa/bb/cc'？y
rm：是否删除目录 '/opt/aa/bb'？y
rm：是否删除目录 '/opt/aa/'？y

[root@som ~]# mkdir -p /opt/aa/bb/cc/dd			#再次创建目录
[root@som ~]# rm -rf /opt/aa/					#加上-f选项不在有提示
```

## 删除空目录rmdir

- rmdir命令是Linux系统下一个用于删除空目录的命令

```shell
[root@som ~]# mkdir /som1						#创建目录/som1
[root@som ~]# mkdir /som2						#创建目录/som2
[root@som ~]# touch /som1/test.txt				#创建文件/som1/test.txt
[root@som ~]# rmdir /som1/						#删除非空目录/som1，失败
rmdir: 删除 '/som1/' 失败: 目录非空			
[root@som ~]# rmdir /som2						#删除空目录/som2，成功
```

## 移动操作-mv

格式：mv <源数据>  ...  <目标位置>

- mv移动数据会使源文件消失
- mv可以同时移动多个参数，只有最后一个是目标位置
- mv可以再移动的过程中改名
- 目标地址不变的移动等于重命名

移动文件操作

```shell
[root@som ~]# mkdir /opt/som10				#创建目录/opt/som10
[root@som ~]# touch /opt/1.txt				#创建/opt/1.txt
[root@som ~]# mv /opt/1.txt /opt/som10		#将/opt/1.txt移动至/opt/som10目录
[root@som ~]# ls /opt/som10/ /opt/			#同时查看/opt/som10和/opt目录验证
```

移动过程中改名操作

```shell
[root@som ~]# touch /root/linux.txt				#创建/root/linux.txt
[root@som ~]# mv /root/linux.txt /opt/cc.txt	#将linux.txt移动至/opt下并改名为cc.txt
[root@som ~]# ls /opt/ /root/					#同时查看/opt/和/root/目录验证
```

使用mv进行"重命名"操作

```shell
[root@som ~]# ls /opt/
[root@som ~]# mv /opt/som10 /opt/game			#将/opt/som01重命名为/opt/game
[root@som ~]# ls /opt/
```

## 复制操作-cp

格式：cp \[选项\]  <源数据> ...  <目标路径>

-f 强制覆盖

-r 递归拷贝

-p 保留数据原属性复制

- cp支持多参数，最后一个为目标路径
- cp不会使源文件消失
- cp操作可以在复制过程中对目标文件改名
- cp默认是cp -i的别名，<font color='red'>其-i选项优先级高于-f</font>，所以当-i和-f同时出现时，使用-i的属性

拷贝文件及目录

```shell
[root@som ~]# cp /etc/passwd /opt/			#将/etc/passwd文件复制到/opt/目录
[root@som ~]# cp /etc/shells /opt/			#将/etc/shells文件复制到/opt/目录
[root@som ~]# cp /etc/passwd /opt/ps.txt		
											#将/etc/passwd文件复制到/opt/目录下改名为ps.txt
[root@som ~]# cp /boot/ /opt/					#将/boot目录复制到/opt/目录下，报错
[root@som ~]# cp -r /boot/ /opt/				#将/boot目录复制到/opt/目录下，成功
[root@som ~]# ls /opt							#验证是否成功拷贝
```

cp多参数拷贝（最后一个为目标路径）

```shell
[root@som ~]# cp -r /boot/ /home/ /etc/passwd /etc/shells  /mnt/ 
							#将/boot/，/home/，/etc/passwd，/etc/shells复制到/mnt目录下
[root@som ~]# ls /mnt/    						#验证
```

cp与.连用(<font color='red'>.表示当前路径</font>)

```shell
[root@som mnt]# cd /mnt						#切换至/mnt
[root@som mnt]# cp /etc/fstab .				#将/etc/fstab复制到当前路径下
[root@som mnt]# ls							#验证
```

cp复制保持属性不变

```shell
[root@som ~]# ls -ld /home/lisi/
drwx------. 4 lisi lisi 113 2月  24 10:22 /home/lisi/
[root@som ~]# cp -r /home/lisi/ /opt/			#将/home/lisi目录复制到/opt下
[root@som ~]# ls -ld /opt/lisi/					#所有者与所属组发生了改变
drwx------. 4 root root 113 2月  28 12:33 /opt/lisi/

[root@som ~]# rm -rf /opt/lisi/					#删除/opt/lisi目录
[root@som ~]# cp -rp /home/lisi/ /opt/			#保留属性将/home/lisi目录复制到/opt下
[root@som ~]# ls -ld /opt/lisi/					#查看目标属性不变
drwx------. 4 lisi lisi 113 2月  24 10:22 /opt/lisi/
```

## 软连接

- 软连接 --> 原始文档 --> 文档数据
- 格式：ln  -s  原始文件或目录  软连接文件 
- <font color='red'>若原始文件或目录被删除，连接文件将失效</font>
- 软连接可存放在不同分区/文件系统

```shell
[root@som ~]# ln -s /etc/passwd /opt/ps.txt	#将/opt/ps.txt设置为/etc/passwd的软连接
```

## 硬链接

- 硬链接 --> 文档数据
- 格式：ln  原始文件  硬链接文件
- <font color='red'>若原始文件被删除，链接文件仍可用</font>
- 硬链接与原始文件必须在同一分区/文件系统

```shell
[root@som ~]# ln /etc/shells /opt/shells		#将/opt/shells设置为/etc/shells的硬链接
```

## 软连接硬链接区别实验

创建素材文件

```shell
[root@som ~]# cp /etc/centos-release /opt/a.txt	#产生素材文件
[root@som ~]# ln -s /opt/a.txt /opt/b.txt		#将/opt/b.txt设置为/opt/a.txt的软连接
[root@som ~]# ln /opt/a.txt /opt/c.txt			#将/opt/c.txt设置为/opt/a.txt的硬链接
```

```shell
[root@som ~]# rm -rf /opt/a.txt					#删除源文件/opt/a.txt
[root@som ~]# cat /opt/b.txt					#无法查看软连接文件/opt/b.txt
[root@som ~]# cat /opt/c.txt					#可以正常查看硬链接文件/opt/c.txt
```



## 通配符

概念：使用固定的字符对数据进行模糊匹配

类似生活中：张三、张三丰、张三花，<font color='red'>张xx(此时xx可能代表三丰、三花)</font>，<font color='blue'>张x(x可以表示三)</font>

*：匹配任意字符，包含0个字符

?：匹配任意单个字符

[0-9]：匹配0-9任意单个数字

[a-z]：匹配任意单个字符

{xx,yy,zz}：严格匹配大括号内的元素

```shell
[root@som ~]# ls /dev/tty*						#匹配/dev/tty后边任意字符内容
[root@som ~]# ls /dev/tty?						#匹配/dev/tty后边1个字符内容
[root@som ~]# ls /dev/tty??						#匹配/dev/tty后边2个字符内容
[root@som ~]# ls /dev/tty[0-9]					#匹配/dev/tty后边1个数字内容
[root@som ~]# ls /dev/tty[0-9][0-9]				#匹配/dev/tty后边2个数字内容	

[root@som ~]# touch /opt/{a,b,c}.txt			#创建/opt/a.txt,/opt/b.txt,/opt/c.txt
[root@som ~]# ls /opt/[a-z].txt					#查看/opt/下一个字符.txt的文件
[root@som ~]# ls -d /m{edia,nt}					#严格匹配/media和/mnt
```

## 重定向操作

作用：将前方命令输出内容保存到文件

\>：覆盖重定向

\>>：追加重定向

- 当目标文件<font color='red'>存在时</font>，则使用目标文件存储数据
- 当目标文件<font color='red'>不存在时</font>，重定向会<font color='red'>创建该目标文件并存储数据</font>

补充：echo指令，echo会将指定内容输出到屏幕上

例如

```shell
[root@som ~]# echo hahaxixi			#在屏幕输出hahaxixi
hahaxixi
[root@som ~]# echo "I LOVE YOU"		#在屏幕输出I LOVE YOU
I LOVE YOU
```



覆盖重定向

```shell
[root@som ~]# cat /opt/hn.txt					#查看/opt/hn.txt，没有此文件
[root@som ~]# hostname > /opt/hn.txt			#将hostname输出结果写入/opt/hn.txt文件
[root@som ~]# cat /opt/hn.txt 					#查看/opt/hn.txt，有文件，有内容
[root@som ~]# ifconfig > /opt/hn.txt			#将ifconfig输出结果写入/opt/hn.txt文件
```

追加重定向

```shell
[root@som ~]# echo 123 > /opt/a.txt				#将123重定向至/opt/a.txt文件
[root@som ~]# cat /opt/a.txt					#查看/opt/a.txt文件内容
[root@som ~]# echo 456 >> /opt/a.txt			#将456追加重定向至/opt/a.txt文件
[root@som ~]# cat /opt/a.txt					#查看/opt/a.txt文件内容(123没有被覆盖)
```

## 管道操作

作用：将前方命令输出结果最为后方命令参数

![1677637631702](Day03.assets/1677637631702.png)



- 管道可以多重使用

```shell
[root@som ~]# ls --help | less					#将ls --help输出信息交给less命令作为参数
[root@som ~]# ifconfig | head -2				#将ifconfig输出信息交给head -2作为参数
```

# find精确查找

## find基本使用

格式：find  查找目录  条件  

- find可用于在Linux操作系统中精确查找某些资料
- find查找为"地毯式搜索"不会放过任何一个子目录、隐藏目录
- 查找过程中如果遇到/proc目录下的报错，属于正常现象，因为/proc不占用磁盘空间，占用的是内存空间
- 常用查找条件
  - -type	按类型查找（f、d、l）
  - -name    按名字查找（可与通配符连用） 
  - -iname   按名字查找（可忽略名字大小写查找、可与通配符连用）
  - -size        按数据大小查找（k、M、G）
  - -mtime   按数据最近修改时间查找
  - -user      按数据所有者查找

按类型查找

```shell
[root@som ~]# find /boot/ -type f				#在/boot/下递归查找文件
[root@som ~]# find /boot/ -type d				#在/boot/下递归查找目录
[root@som ~]# find /boot/ -type l				#在/boot/下递归查找连接文件(快捷方式)
```

按名字查找

```shell
[root@som ~]# find /etc/ -name "passwd"			#在/etc/下递归查找名为passwd的数据
[root@som ~]# find /etc/ -name "*tab"			#在/etc/下递归查找tab结尾的数据
[root@som ~]# find /etc/ -name "*passwd*"		#在/etc/下递归查找名字包含passwd的数据
```

忽略名字大小写查找

```shell
[root@som ~]# find /etc/ -iname "PaSSwd"		#在/etc/查找名为passwd的数据（忽略大小写）
```

按大小查找

- k：kb作为单位
- M：MB作为单位
- G：GB作为单位
- 注意：在使用大小进行查找时，对于某些目录可能存在一定的BUG，例如1M需要写成1024k，当然这里需要多测试才会发现

```shell
[root@som ~]# find /boot/ -size +2M				#在/boot/查找文件大小大于2M的数据
[root@som ~]# ls -lh /boot/grub2/fonts/unicode.pf2 		#验证大小
-rw-r--r--. 1 root root 2.5M 12月 19 12:10 /boot/grub2/fonts/unicode.pf
[root@som ~]# find /boot/ -size -1M			#在/boot/查找文件大小小于1M的数据（出现BUG）
[root@som ~]# find /boot/ -size -1024k		#在/boot/查找文件大小小于1024k的数据（小于1M）
```

按修改时间

```shell
[root@som ~]# find /etc/ -mtime +10			#查找10天前修改的数据
[root@som ~]# find /etc/ -mtime -10			#查找近10天修改的数据
```

查找按所有者查找

```shell
[root@som ~]# ls -ld /home/lisi/				#查看“文档”属性，查看所有者
drwx------. 4 lisi lisi 113 2月  24 10:22 /home/lisi/
[root@som ~]# find /home/ -user lisi			#从/home中查找所有者为lisi的数据
[root@som ~]# find /etc/ -user root				#从/etc中查找所有者为root的数据
```

## find多条件使用

-a：and逻辑与，多个条件同时成立才满足条件（默认为逻辑与）

-o：or逻辑或，多个条件成立其中一个即可

创建实验素材

```shell
[root@som ~]# touch /root/som01.txt 		#创建/root/som01.txt
[root@som ~]# touch /root/som02.txt			#创建/root/som02.txt
[root@som ~]# mkdir /root/som03				#创建/root/som03目录
```

逻辑与案例-a，在/root/下查找<font color='red'>som开头</font>的<font color='red'>文件</font>

```shell
[root@som ~]# find /root/ -name "som*" -a -type f	
[root@som ~]# find /root/ -name "som*"  -type f	#-a也可以省略不写，默认就是-a	
```

逻辑或案例-o，在/root/下查找<font color='red'>som开头</font>的资料，或者<font color='red'>是文件即可</font>

```shell
[root@som ~]# find /root/ -name "som*" -o -type f	
```

# find高级使用

- 可用于处理找到的文件
- 格式：find  [范围]    \[条件]   -exec   处理命令  {} \ \;
- <font color='red'>{}</font>表示查找到的每一个结果
- <font color='red'>\\;</font>表示操作结束

```shell
[root@som ~]# find /boot/ -size +10M -exec ls {} \;   #ls查看/boot下大于10M的数据
[root@som ~]# find /boot/ -size +10M | xargs ls;	  #同上（xargs传递|前查找到的每个数据）
```

将/boot下vm开头的数据拷贝到/root/findfiles中

```shell
[root@som ~]# mkdir /root/findfiles				  #新建文件夹/root/findfiles
[root@som ~]# find /boot/ -name "vm*" -exec cp -r {} /root/findfiles/ \;	#查找并拷贝
```

# 总结

- 掌握Linux常用命令
  - hostname、ifconfig、mkdir、touch、head、tail、alias
  - rm、rmdir、mv、cp、ln
- 掌握管道、重定向
- 掌握find命令使用
- 掌握find命令高级使用



