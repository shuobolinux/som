[TOC]

# 环境准备

本节课需要提前准备一台<font color='red'>CentOS7.9虚拟机</font>

# 进程管理

## 程序

- 保存在磁盘中的可执行文件

- 是静态保存的代码

## 进程

- 在CPU及内存中运行的程序指令

- 是动态执行的代码

- 父/子进程：进程可以创建一个或多个子进程

## 查看进程树

- pstree — Processes Tree

- 格式:pstree \[选项]  \[PID或用户名]
  - -a: 显示完整的命令行
  - -p: 列出对应PID编号

- systemd:所有进程的父进程 （上帝进程）

 ```shell
[root@som ~]# pstree         					#查看整个进程树信息
[root@som ~]# pstree -u lisi					#查看lisi用户的进程
未发现进程。    
 ```

另开一个终端

```shell
[root@som ~]# su - lisi							#切换lisi用户
[lisi@som ~]$ vim a.txt          				#不编辑，不退出即可
```

回到之前终端

```shell
[root@som ~]# pstree -u  lisi                 	#显示lisi用户正在运行的进程
bash───vim
[root@som ~]# pstree -pu  lisi              	#p，显示pid号
bash(11980)───vim(12017)
[root@som ~]# pstree -apu  lisi            		#a，显示完整的命令行
bash,11980
  └─vim,12017 a.txt
```

## 查看进程

- ps aux 操作：信息非常全面详细,列出正在运行的所有进程
  - a：显示当前终端所有进程
  - u：以用户格式输出
  - x：当前用户在所有终端下的进程

- ps -elf 操作：信息中有该进程的父进程信息列出正在运行的所有进程
  - -e：显示系统内所有进程
  - -l：以长格式输出信息，包括最完整的进程信息

```shell
[root@som ~]# ps aux
USER  PID       %CPU %MEM    VSZ        RSS    TTY   STAT     START   TIME COMMAND
用户  进程ID     %cpu %内存   虚拟内存   固定内存   终端   状态    起始时间 cpu时间 程序指令
```

```shell
[root@som ~]# ps -elf     				#会有一个PPID
PPID：代表父进程的PID
[root@som ~]# ps -elf | wc -l          	#统计正在运行的进程有多少
```

## 进程动态排名

- top 交互式工具，默认3秒刷新一次

- 格式:top [-d 刷新秒数][-U 用户名]

```shell
[root@som ~]# top          			#查看进程，默认每隔3秒刷新一次
[root@som ~]# top -d 1 				#每隔1秒刷新一次
按大写P 根据CPU使用进行排序，CPU占用最高的排首位**
按大写M 根据内存使用进行排序，内存占用最高的排首位**
```

Top命令参数详解：https://www.cnblogs.com/ggjucheng/archive/2012/01/08/2316399.html

## 查看CPU负载

- 可以通过uptime命令来非交互式查看CPU平均负载等信息

```shell
[root@som ~]# uptime
13:55:28 up 10 days,  1:13,  2 users,  load average: 0.00, 0.01, 0.05
```

## 检索进程信息

- pgrep — Process Grep
  - -l：输出进程名,而不仅仅是 PID
  - -U：检索指定用户的进程
  - -t：检索指定终端的进程
  - -x：精确匹配完整的进程名

- 用途:pgrep [选项]... 查询条件

```shell
[root@som ~]# pgrep a                 	#检索进程名包含a的进程，但是只显示PID
[root@som ~]# pgrep -l a        		#检索进程名包含a的进程，-l输出进程名称
[root@som ~]# pgrep -l crond
1246 crond    
[root@som ~]#  pgrep -lU lisi    		#检索lisi用户的进程
```

## 进程的前后台调度

- 前台启动
  - 输入正常的命令行
  - 运行期间占用当前终端

- 后台启动
  -  在命令行末尾添加“&”符号,以正在运行的状态放入后台
  - 运行期间不占用当前终端

 ```shell
[root@som ~]# sleep  3           		#当前终端睡3秒
[root@som ~]# sleep  1000            	#当前终端睡1000秒，会一直占用这个终端
[root@som ~]# sleep  1000 &         	#当前终端睡1000秒，但是是放到后台，不影响使用终端
[root@som ~]# firefox
[root@som ~]# firefox &
 ```

-  Ctrl + z 组合键
  - 挂起当前进程  (暂停并转入后台)

- jobs：查看后台任务列表

 ```shell
[root@som ~]# jobs           			#查看后台进程任务列表
[root@som ~]# sleep 1000
^Z                            			#按Ctrl+z 暂停放入后台
[2]+  已停止               sleep 1000
[root@som ~]# jobs
 ```

bg：激活后台被挂起的任务

```shell
[root@som ~]# bg  2       				#将后台编号为2的进程继续运行
[root@som ~]# jobs
```

fg：将后台任务恢复到前台运行

```shell
[root@som ~]# fg  2      				#将后台编号为2的进程恢复到前台
sleep 800
^C                        				#按Ctrl+c终止进程
[root@som ~]# jobs
[root@som ~]# fg  1
sleep 1000
^C
[root@som ~]# jobs
```

## 杀死进程

- 干掉进程的不同方法
  - Ctrl+c 组合键,中断当前命令程序
  - kill [-9] PID... 、kill [-9] %后台任务编号，-9强制杀死
  - killall [-9] 进程名...
  - pkill 查找条件 

 ```shell
[root@som ~]# sleep 1000 &
[1] 21406
[root@som ~]# sleep 2000 &
[2] 21407
[root@som ~]# sleep 3000 &
[3] 21408
[root@som ~]# jobs -l              		#-l显示进程的pid
[root@som ~]# kill -9 21406        		#强制杀死pid为21406的进程
 ```

```shell
[root@som ~]# killall  -9 sleep           #强制杀死所有sleep进程
[3]+  已终止               sleep 3000
[root@som ~]# jobs -l
```

杀死一个用户的开启的所有进程（强制踢出一个用户）

```shell
[root@som ~]# pkill  -9 -u lisi
```

## systemctl命令

- systemctl是Linux系统中常用的服务管理工具，可以用于启动、停止、重启、查看服务状态等操作。
- firewalld是CentOS 7内置的防火墙服务，本实验采用firewalld服务练习
- <font color='red'>最终需要关闭防火墙并设置为开机不自启。</font>

```shell
[root@som ~]# systemctl status firewalld		#查看firewalld服务状态
[root@som ~]# systemctl start firewalld			#启动firewalld服务
[root@som ~]# systemctl restart firewalld		#重启firewalld服务
[root@som ~]# systemctl stop	firewalld		#停止firewalld服务
[root@som ~]# systemctl enable firewalld		#将firewalld服务设置为开机自启动
[root@som ~]# systemctl disable firewalld		#将firewalld服务设置为开机不自启动
[root@som ~]# systemctl is-enabled firewalld		#查看firewalld服务是否为开机自启动
```

## SELinux

- SELinux（Security-Enhanced Linux）是一个安全性增强型的Linux操作系统，它是基于MILS（多级安全）架构的一种强制访问控制机制。通过将SELinux应用到内核模块和文件系统中，可以实现对系统和应用程序的安全保护。
- SELinux有三种模式：enforcing、permissive和disabled。
- <font color='blue'>Enforcing模式</font>
  - 在Enforcing模式下，SELinux强制对系统资源和应用程序进行安全保护，禁止未经授权的访问和操作。如果出现违规行为，SELinux会自动记录并且防止该行为进行。<font color='red'>Enforcing模式是SELinux的默认模式。</font>

- <font color='blue'>Permissive模式</font>
  - 在Permissive模式下，SELinux只是记录违规行为并不会拦截。其目的是观察应用程序如何与SELinux互动并了解SELinux规则如何影响应用程序。Permissive模式的作用在于不中断系统程序的正常工作，同时记录下可能违规行为，以提高系统的安全性。

- <font color='blue'>Disabled模式</font>
  - 在Disabled模式下，SELinux不进行任何操作，应用程序和系统资源可以自由访问和操作。在需要暂时关闭SELinux时可以使用Disabled模式。

### 关闭SELinux

将SELinux改为Disabled模式

```shell
[root@som ~]# vim /etc/selinux/config		#修改文件
# This file controls the state of SELinux on the system.
# SELINUX= can take one of these three values:
#     enforcing - SELinux security policy is enforced.
#     permissive - SELinux prints warnings instead of enforcing.
#     disabled - SELinux is fully disabled.
SELINUX=disabled
...
[root@som ~]# reboot						#重启
```

# 总结

- 掌握进程管理
  - 掌握进程与程序的区别
  - pstree、ps、top、uptime
- 掌握进程前后台调度
  - &、fg、bg
- 掌握systemctl命令停启服务
- 了解SELinux



