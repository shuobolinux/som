[TOC]

# 克隆虚拟机

- 通过克隆技术，可以快速且方便地创建出与源虚拟机完全相同的新的虚拟机。
- 克隆方式
  - 链接克隆：依赖于源服务器
  - 完整克隆：与源服务器完全独立

使用CentOS7.9虚拟机链接克隆虚拟机A和虚拟机B，下方以克隆虚拟机B为例

![1690274570568](Day08.assets/1690274570568.png)

![1687927712005](Day08.assets/1687927712005.png)

![1687927729344](Day08.assets/1687927729344.png)

![1687927750556](Day08.assets/1687927750556.png)

![1687928482617](Day08.assets/1687928482617.png)

![1687927812960](Day08.assets/1687927812960.png)

# 修改网卡命名规则

修改网卡名

```shell
[root@som ~]# vim /etc/default/grub
…
GRUB_CMDLINE_LINUX="rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet net.ifnames=0 biosdevname=0"         			#添加net.ifnames=0 biosdevname=0
…
[root@som ~]# grub2-mkconfig  -o /boot/grub2/grub.cfg    #重新生成引导文件，使配置文件生效
[root@som ~]# reboot             				#重启
```

验证，网卡名是否发生变化

```shell
[root@som ~]# ifconfig           				#有eth0网卡
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
      inet 192.168.71.130  netmask 255.255.255.0  broadcast 192.168.71.255
```

## Linux配置IP地址

LINUX系统配置IP地址方法

- /etc/sysconfig/network-scripts/ifcfg-网卡名
- nmtui、ip、<font color='red'>nmcli</font>

nmcli命令配置IP地址

- nmcli connection show		#查看连接信息

- 手动配置IP

  - nmcli connection modify 网卡名 ipv4.method manual  \

    ipv4.address <IP/子网掩码> ipv4.gateway <网关> ipv4.dns <dns服务器> \
    connection.autoconnect  yes

- 自动配置IP（DHCP分配）

  - nmcli connection modify 网卡名 ipv4.method auto \

    connection.autoconnect  yes

- nmcli  connection  up   网卡名     #激活网卡

```shell
[root@som ~]# nmcli connection show         		 #查看连接名
[root@som ~]# nmcli connection delete ens33    		 #删除
[root@som ~]# nmcli connection delete 有线连接\ 1
[root@som ~]# nmcli connection show 
NAME        UUID                                  TYPE      DEVICE
virbr0      0ace37a8-f7ad-45d6-9088-00b367999c0b  bridge    virbr0
```

重新添加网卡

```shell
[root@som ~]# nmcli connection add  type ethernet ifname  eth0 con-name eth0  #添加网卡
连接“eth0”(7b47bf24-859e-45f5-ace7-3b9706b61e55) 已成功添加。
[root@som ~]# nmcli connection show 
NAME    UUID                                  TYPE      DEVICE 
eth0    7b47bf24-859e-45f5-ace7-3b9706b61e55  ethernet  eth0
```

配置IP

```shell
[root@som ~]# nmcli connection modify eth0 ipv4.method manual ipv4.addresses 192.168.4.7/24 ipv4.gateway 192.168.4.254 connection.autoconnect yes           
													#配置IP地址和网关并实现开机自动连接
[root@som ~]# nmcli connection up eth0     			#激活eth0
[root@som ~]# ifconfig            					#查看配置的IP
[root@som ~]# route -n          					#查看网关
```

# 虚拟网络类型

## NAT模式

- VMware的NAT（Network Address Translation）模式是一种网络连接模式，它允许虚拟机通过主机的网络连接与外部网络通信。

- 在NAT模式下，VMware会为主机创建一个虚拟网络适配器，并为其分配一个私有IP地址。主机通过这个私有IP地址与虚拟机进行通信。当虚拟机需要访问外部网络时，VMware会将其请求转发给主机，再由主机通过NAT技术将请求发送到外部网络，然后将响应传递回虚拟机。

## 隔离模式

- Guest可访问同一虚拟交换机上的其他Guest，但无法访问Host所在外部网络

### VMware-NAT模式

![1684662884529](Day08.assets/1684662884529.png)





## 设置VMnet8网络模式

![1687926913353](Day08.assets/1687926913353.png)

![1687926998585](Day08.assets/1687926998585.png)

![1688005142930](Day08.assets/1688005142930.png)

![1688005213358](Day08.assets/1688005213358.png)

## 为虚拟机自动配置IP地址

```shell
[root@som ~]# nmcli  connection  modify eth0 ipv4.method manual ipv4.gateway '' connection.autoconnect yes 							   #清空旧的网关
[root@som ~]# nmcli connection up eth0 
[root@som ~]# nmcli  connection  modify eth0 ipv4.method auto connection.autoconnect yes 													#配置IP地址
[root@som ~]# nmcli connection up eth0 
[root@som ~]# ping www.baidu.com
```

# 管理IP地址-ifconfig命令

- 可以使用ifconfig命令查看IP地址/临时配置IP地址

```shell
[root@som ~]# ifconfig					#查看所有网卡信息
[root@som ~]# ifconfig  eth0			#查看eth0网卡信息(以自己的网卡名为准)
```

临时配置IP地址，将虚拟机CentOS7.9的IP地址配置为：192.168.4.7/24

```shell
[root@som ~]# ifconfig  eth0 192.168.4.7/24		#配置eth0网卡IP(以自己的网卡名为准)
[root@som ~]# ifconfig  eth0					#查看验证
```

# 常用网络工具

## ip命令

查看IP地址

```shell
[root@som ~]# ip address show
```

添加IP地址

```shell
[root@som ~]# ip  address add 192.168.10.1/24 dev eth0         #会有10.1的IP，但是是临时的
[root@som ~]# ip addr show
[root@som ~]# ping 192.168.10.1   							#可以ping通
```

如果想要每次开机生效，可以写/etc/rc.d/rc.local文件，给个执行的权限

```shell
[root@som ~]# vim /etc/rc.d/rc.local
ip  address add 192.168.10.1/24 dev eth0
[root@som ~]# chmod +x /etc/rc.d/rc.local
```

添加路由

```shell
[root@som ~]# ip route add 10.0.0.0/24 via 192.168.10.100 dev eth0    #via是下一跳
[root@som ~]# ip route  show               							  #查看路由表
```

删除路由

```shell
[root@som ~]# ip route del 10.0.0.0/24
[root@som ~]# ip route  show
```

## 追踪路由

- traceroute功能：可以知道从本机到达目标所经过的路由器有哪些

- 命令格式: traceroute    [参数][主机]

```shell
[root@som ~]# traceroute 192.168.4.7
traceroute to 192.168.4.7 (192.168.4.7), 30 hops max, 60 byte packets
1       som.tedu.cn (192.168.4.7)  0.043 ms  0.013 ms  0.011 ms
```

```shell
[root@som ~]# traceroute 192.168.4.207
```

## ss命令

-a：显示所有端口的信息

-n：以数字格式显示端口号

-t：显示TCP连接的端口

-u：显示UDP连接的端口

-l：显示服务正在监听的端口信息

-p：显示监听端口的服务名称是什么（也就是程序名称）

```shell
[root@som ~]# netstat -anptu
[root@som ~]# ss -anptu
[root@som ~]# netstat -anptu |   grep :22			#过滤22端口
[root@som ~]# ss  -anptu |grep :22
```

##  ping命令

- -c：测试包个数
- -i：ping的速度（多久ping一次）
- -W：大写，ping的等待时间，不通等待多久返回结果

```shell
[root@som ~]# ping -c2 -i0.2 -W1  192.168.8.1   
#0.2秒ping1次，总共ping 2次，如果不通等待1秒返回结果
```

# 练习

- 将克隆的虚拟机B，主机名修改为：pc.tedu.cn
- 采用NAT模式为虚拟机配置IP地址
- 使A、B主机可以互相通信

# 总结

- 掌握Linux系统中常用网卡命名规则
- 掌握VMware-NAT模式
- 掌握nmcli配置IP地址原理
- 了解IP命令用法
- 了解其他常用网络管理工具