[TOC]



# 环境准备

本节课需要使用到环境如下，需要提前准备好（已放在教学环境中）

- <font color='red'>Cisco Packet Tracer 6.2 for Windows Student Version (no tutorials).exe</font>软件
- <font color='red'>Chinese.ptl</font>文件

# OSI七层参考模型

国际标准化组织（International Standard Organization，ISO）于1984年颁布了开放系统互连（Open System Interconnection，OSI）参考模型

它规定将网络分为七层，从下往上依次是：物理层、数据链路层、网络层、传输层、会话层、表示层和应用层

![img](Day01.assets/AgAACJ54zJrUG3OhtqZGjZv2bbixpjKd.png)

# 什么是协议？

- 为了使数据可以在网络上从源传递到目的地，网络上所有设备需要“讲”相同的“语言”

- 描述网络通信中“语言”规范的一组规则就是协议



# 协议数据单元(PDU)

![1684377644224](Day01.assets/1684377644224.png)



# 设备与层的对应关系

![img](Day01.assets/AgAACJ54zJpLvGotwh1HP543Ogk4afEp.png)

# 什么是IP地址？

- IP地址：网络中唯一标识一台主机

- IP地址由32个二进制数组成，二进制的世界里只有0和1，二进制逢2进1

- 每8位加一个. 因此又叫做点分十进制

  11111111.11111111.11111111.11111111

  255.255.255.255

# IP地址分类

- A类：1-126(127)

- B类：128-191

- C类：192-223

- D类：224-239

- E类：240-255

- 工作中常用的地址是A、B、C三类、D和E类分别用于组播和科研

# IP的网络位和主机位

网络位：相当于标识区域

主机位：相当于标识顺序

举例子

- 010-6752610

- 010-6752611

- 0313-6743121

- 0313-6743122

# IP地址默认网络位与主机位

A类：<font color='red'>网络位</font>.主机位.主机位.主机位	例：10.0.0.1
B类：<font color='red'>网络位.网络位</font>.主机位.主机位 	例：172.25.0.1

C类：<font color='red'>网络位.网络位.网络位</font>.主机位 	例：192.168.1.1

某个类别中的第一个IP地址和最后一个IP地址均不能直接使用

- 第一个地址表示 网段

- 最后一个地址表示 广播地址

  - 192.168.4.0			表示192.168.4.0网段(本网段第一个IP)
  - 192.168.4.255		表示广播地址

  - 192.168.4.1			表示192.168.4.0网段中的第一个可用IP

# 子网掩码

- 子网掩码：计算机用来标识网络位和主机位

- 网络为使用二进制的1 表示

- 主机位使用二进制的0 表示

每8位转换成一个十进制

192.168.4.1-->C类-->192.168.4-->11111111.11111111.11111111.00000000

255.255.255.0

172.25.0.1-->B类-->172.40-->11111111.11111111.00000000.00000000

255.255.0.0

10.0.0.1-->A类-->10-->11111111.00000000.00000000.00000000

255.0.0.0

# 默认子网掩码

A类:   255.0.0.0			也可以使用8表示(8表示二进制有8个1)

B类： 255.255.0.0		也可以使用16表示(16表示二进制有16个1)

C类： 255.255.255.0		也可以使用24表示(24表示二进制有24个1)

# 查看IP地址

```shell
C:\Users\15790>ipconfig  							#查看IP地址
C:\Users\15790>ping 127.0.0.1						#ping测试
```







# 安装CISCO

![1686552408203](Day01.assets/1686552408203.png)

![1686552480985](Day01.assets/1686552480985.png)

安装再自己空间充足的分区下

![1686552512833](Day01.assets/1686552512833.png)

![1686552542110](Day01.assets/1686552542110.png)

![1686552581769](Day01.assets/1686552581769.png)

![1686552685529](Day01.assets/1686552685529.png)

![1686552727093](Day01.assets/1686552727093.png)



## 汉化CISCO(可选操作)

​             1、      将Chinese.ptl文件复制到CICSO安装文件夹/languages/

​             2、      选择CISCO左上角Optinons--->Preferences-->选择CHinese.ptl--->选择Change language

​             3、      重新启动CISCO

![1686552758533](Day01.assets/1686552758533.png)



# CISCO之PC机器

实验拓扑图

![1684378243326](Day01.assets/1684378243326.png)

为两台PC机器配置IP地址，如下方法

- 第一台配置：192.168.1.1 255.255.255.0(子网掩码)

- 第二台配置：192.168.1.2 255.255.255.0(子网掩码)

![img](Day01.assets/AgAACJ54zJq_rnKYgG5ExIPqaqZ8MYo6-1684378286165.png) 

![img](Day01.assets/AgAACJ54zJrFpXMPhrdLep0oU5qiMkMU-1684378286138.png)

## 验证通信

- ping 命令：专门用于测试网络连通性的命令工具，数据有去有回

- 使用格式：ping    IP地址

![1684378334242](Day01.assets/1684378334242.png)

```shell
PC>ping 192.168.1.2
Pinging 192.168.1.2 with 32 bytes of data:

Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128

Ping statistics for 192.168.1.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
PC>
```

# CISCSO之交换机

交换机：是组网的必备设备，可以解决网线接口不足的问题，可以实现网络隔离(VLAN)

![1686987915848](Day01.assets/1686987915848.png)

- 第一台配置：192.168.1.1 255.255.255.0(子网掩码)
- 第二台配置：192.168.1.2 255.255.255.0(子网掩码)
- 第三台配置：192.168.1.3 255.255.255.0(子网掩码)

下方以一台PC为例

![1686987961558](Day01.assets/1686987961558.png)

![1686987979779](Day01.assets/1686987979779.png)

## 测试使用交换机组网通信(各个PC机之间相互ping)

![1686987999061](Day01.assets/1686987999061.png)

以下方为例

```shell
PC>ping 192.168.1.2
Pinging 192.168.1.2 with 32 bytes of data:

Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128

Ping statistics for 192.168.1.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
```

# 路由器基本使用

- 用户模式
- 特权模式
- 全局配置模式
- 接口模式

![1686988021505](Day01.assets/1686988021505.png)

```shell
Router>		 					 #用户模式
Router> enable         			 #进入特权模式   
Router#                    		 #特权模式
Router# configure terminal       #进入全局配置模式   
Router(config)#		        	 #全局配置模式
Router(config)# interface GigabitEthernet 0/0	       #接口模式
Router(config-if)#
```

## 路由器使用技巧

```shell
Router>                     		#用户模式
Router> en(Tab)       				#进入特权模式
Router#                     		#特权模式
Router# conf(Tab)    t(Tab)     	#进入全局配置模式
Router(config)#                     #全局配置模式
Router(config)#  ex(Tab)            #进行后退
Ctrl  + Shift + 6      		  		#输入错误，取消DNS的查询
```

## 路由器关闭DNS查询功能

```shell
Router> enable            					#进入特权模式
Router# configure    terminal    			#进入全局配置模式
Router(config)# no    ip     domain-lookup  #关闭DNS查询功能
Router(config)# end            				#返回到特权模式
Router# haha             				 	#测试验证
 Translating "haha"
 % Unknown command or computer name, or unable to find computer address
Router#
Router# write      							#特权模式下，保存路由器配置
Building configuration...
[OK]
```

## 路由器接口介绍

- 路由器查看接口信息 

```shell
Router> enable
Router# show ip interface brief				#查看IP接口信息
```

## 路由器设置接口IP地址

```shell
Router> enable            			     	     		#进入特权模式
Router# configure    terminal         		    		#进入全局配置模式
Router(config)# interface GigabitEthernet  0/0          #进入接口g0/0
Router(config-if)# ip  address  192.168.1.1   255.255.255.0
Router(config-if)# no  shutdown       		    		#激活启用接口
Router(config-if)# exit                        	        #退回到全局配置模式
Router(config-if)# end                                  #退回到特权模式
Router# show ip interface brief                         #查看接口IP地址信息
Interface                 IP-Address OK? Method Status Protocol
GigabitEthernet0/0 192.168.1.1 YES manual up  up
GigabitEthernet0/1 unassigned YES unset administratively down down
GigabitEthernet0/2 unassigned YES unset administratively down down
Vlan1 unassigned YES unset administratively down dow

```

## 路由器删除或更改接口IP地址

```shell
Router> enable            								#进入特权模式
Router# configure    terminal          					#进入全局配置模式
Router(config)# interface GigabitEthernet 0/0 			#进入接口g0/0
Router(config-if)# no  ip  address        				#删除接口IP地址
Router(config-if)# exit                          		#退回到全局配置模式
Router(config)# interface GigabitEthernet 0/1    		#进入接口g0/1
Router(config-if)# ip  address    192.168.3.1   255.255.255.0
Router(config-if)# no  shutdown      					#激活启用接口
Router(config-if)# end                       			#退回到特权模式
Router# show ip interface brief      					#查看接口IP地址信息
```

# 路由器综合实验

- 一台2911路由器 
- 两台交换机   
- 四台PC

![1686988050262](Day01.assets/1686988050262.png)

## 路由器设置接口IP地址

```shell
Router> enable           		  					#进入特权模式
Router# configure    terminal          				#进入全局配置模式
Router(config)# interface GigabitEthernet  0/0		#进入接口g0/0
Router(config-if)# ip  address  192.168.1.254   255.255.255.0
Router(config-if)# no  shutdown       				#激活启用接口
Router(config-if)# exit                        		#退回到全局配置模式
Router(config)# interface GigabitEthernet 0/1 		#进入接口g0/1
Router(config-if)# ip  address    192.168.2.254   255.255.255.0
Router(config-if)# no  shutdown      				#激活启用接口
Router(config-if)# end                       		#退回到特权模式
Router# show ip interface brief      				#查看接口IP地址信息

```

## 配置IP及子网掩码

![1686988077273](Day01.assets/1686988077273.png)



![1686988098513](Day01.assets/1686988098513.png)

## 测试是否通信

```shell
PC>ping     192.168.2.1         
Pinging 192.168.1.2 with 32 bytes of data:
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
Reply from 192.168.1.2: bytes=32 time=0ms TTL=128
```

# 总结

- 掌握什么是IP地址
  - IP地址分类
  - 网络为与主机位
  - 子网掩码
- 熟练安装Cisco软件
- 掌握交换机工作原理
- 掌握路由器工作原理
- 掌握Cisco模拟器中路由器的配置方法
- 了解路由表填充方式