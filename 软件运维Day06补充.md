# RAID磁盘阵列

- RAID（Redundant Array of Independent Disks）磁盘阵列是一种将多个独立硬盘组合起来，形成一个逻辑上单一、物理上分散的高性能存储系统。主要目的是通过数据冗余和分布式存储来提高数据可靠性和性能。

## RAID0条带模式

- 至少需要两块磁盘
- 数据并行写入，提高写入速度
- 可靠性未增加，不允许损坏磁盘
-  存储原始数据的磁盘使用率约等于100%

![1695630132697](软件运维Day06补充.assets/1695630132697.png)

## RAID1镜像模式

- 至少需要2块磁盘
- 使用一块盘的空间做备份
- 读写效率无明显提升，最多允许损坏一块磁盘

- 使用50%的空间存储原始数据

![1695630520687](软件运维Day06补充.assets/1695630520687.png)

## RAID5高性价比模式

- 至少需要3块磁盘
- 其中一块磁盘的空间（总空间大小为一块磁盘的大小）用于存储奇偶校验
- 读写效率提升，有冗余备份
- 最多允许损坏一块磁盘
- 存储原始数据的磁盘利用率达到n-1/n（n指的是磁盘数量）

![1695631164255](软件运维Day06补充.assets/1695631164255.png)

## RAID01

- 至少需要4块磁盘
- 先做RAID0，在做RAID1
- RAID0和RAID1的结合版
- 最多允许损坏2块磁盘，但是这两块磁盘存储的数据不能一致
- 存储原始数据的磁盘利用率达到50%

![1695631787743](软件运维Day06补充.assets/1695631787743.png)

## RAID10

- 至少需要4块磁盘
- 先做RAID1，在做RAID0
- RAID0和RAID1的结合版
- 最多允许损坏2块磁盘，但是这两块磁盘存储的数据不能一致
- 存储原始数据的磁盘利用率达到50%

![1695632097312](软件运维Day06补充.assets/1695632097312.png)

# 逻辑卷

- 将零散的空间整合化，再次划分新空间，可以使用逻辑卷提升磁盘利用率，节约成本。

![1695634111486](软件运维Day06补充.assets/1695634111486.png)

## 一块磁盘的使用流程

```mermaid
graph LR  
  A(识别磁盘) --> B(分区规划)  
  B(分区规划) --> C(格式化)  
  C(格式化)   --> D(挂载使用)
```

## 逻辑卷的使用流程

```mermaid
graph LR  
  A(识别磁盘) --> B(分区规划)  
  B --> C(物理卷)  
  C --> D(卷组)
  D --> E(逻辑卷)
  E --> F(格式化)
  F --> G(挂载使用)
```

# 制作逻辑卷

### 步骤一：添加硬盘

在虚拟机CentOS7.9中添加一块大小为20G的磁盘。

添加过程，此处省略一万字。

```shell
[root@localhost ~]# lsblk /dev/sdd				#查看是否自动识别新磁盘（编号以自己的为准）
NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sdd    8:48   0  20G  0 disk
```

### 步骤二：分区规划

使用/dev/sdd采用GPT分区方案划分三个大小为2G的主分区

```shell
[root@localhost ~]# parted /dev/sdd
(parted) mktable gpt							#指定分区表（分区方案）
(parted) mkpart 
分区名称？  []? mypart1
文件系统类型？  [ext2]? ext2
起始点？ 0
结束点？ 2G
警告: The resulting partition is not properly aligned for best
performance.
忽略/Ignore/放弃/Cancel? Ignore

(parted) mkpart 
分区名称？  []? mypart2
文件系统类型？  [ext2]? ext2
起始点？ 2G
结束点？ 4G

(parted) mkpart 
区名称？  []? mypart3
文件系统类型？  [ext2]? ext2
起始点？ 4G
结束点？ 6G

(parted) print
...
Number  Start   End     Size    File system  Name     标志
 1      17.4kB  2000MB  2000MB               mypart1
 2      2001MB  4000MB  2000MB               mypart2
 3      4000MB  6000MB  2000MB               mypart3

(parted) quit										#保存并退出
```

```shell
[root@localhost ~]# lsblk /dev/sdd					#验证分区是否成功
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdd      8:48   0   20G  0 disk 
├─sdd1   8:49   0  1.9G  0 part 
├─sdd2   8:50   0  1.9G  0 part 
└─sdd3   8:51   0  1.9G  0 part
```

### 步骤三：制作物理卷

命令：pvcreate      设备1      设备2     ...  设备n

```shell
[root@localhost ~]# pvcreate /dev/sdd1 /dev/sdd2 /dev/sdd3		#创建物理卷
  Physical volume "/dev/sdd1" successfully created.
  Physical volume "/dev/sdd2" successfully created.
  Physical volume "/dev/sdd3" successfully created.
```

```shell
[root@localhost ~]# pvs											#查看物理卷
  PV         VG     Fmt  Attr PSize   PFree
  /dev/sda2  centos lvm2 a--  <79.00g 4.00m
  /dev/sdd1         lvm2 ---    1.86g 1.86g
  /dev/sdd2         lvm2 ---    1.86g 1.86g
  /dev/sdd3         lvm2 ---    1.86g 1.86g
```

### 步骤四：制作卷组

命令：vgcreate   卷组名	  物理卷1   物理卷2   ... 物理卷3 

```shell
[root@localhost ~]# vgcreate systemvg /dev/sdd1 /dev/sdd2 /dev/sdd3 	#创建卷组
  Volume group "systemvg" successfully created
```

```shell
[root@localhost ~]# vgs													#查看卷组
  VG       #PV #LV #SN Attr   VSize   VFree 
  centos     1   3   0 wz--n- <79.00g  4.00m
  systemvg   3   0   0 wz--n-  <5.58g <5.58g
```

### 步骤五：制作逻辑卷

命令：lvcreate  -n 逻辑卷名   -L 逻辑卷大小   卷组

```shell
[root@localhost ~]# lvcreate -n mylv -L 5G systemvg 				#制作逻辑卷
  Logical volume "mylv" created.
```

```shell
[root@localhost ~]# lvs												#查看逻辑卷
  LV   VG       Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home centos   -wi-ao---- 26.99g                                                    
  root centos   -wi-ao---- 50.00g                                                    
  swap centos   -wi-ao----  2.00g                                                    
  mylv systemvg -wi-a-----  5.00g   
[root@localhost ~]# ls /dev/systemvg/mylv 							#查看逻辑卷设备文件
/dev/systemvg/mylv
```

### 步骤六：格式化逻辑卷

```shell
[root@localhost ~]# mkfs.xfs /dev/systemvg/mylv						#格式化
[root@localhost ~]# blkid /dev/systemvg/mylv
/dev/systemvg/mylv: UUID="fe8f5427-cdd0-4202-b764-913f162011b6" TYPE="xfs"
```

### 步骤七：挂载逻辑卷

```shell
[root@localhost ~]# mkdir /mylv							#创建挂载点
[root@localhost ~]# vim /etc/fstab						#编写文件，实现永久挂载逻辑卷
...
/dev/systemvg/mylv  /mylv  xfs  defaults 0 0
[root@localhost ~]# mount -a							#刷新
```

逻辑卷使用

```shell
[root@localhost ~]# cp /etc/passwd /mylv
[root@localhost ~]# echo 123 > /mylv/testfile.txt
[root@localhost ~]# ls /mylv/
passwd  testfile.txt
```

```shell
[root@localhost ~]# lsblk /dev/sdd
NAME              MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdd                 8:48   0   20G  0 disk 
├─sdd1              8:49   0  1.9G  0 part 
│ └─systemvg-mylv 253:3    0    5G  0 lvm  /mylv
├─sdd2              8:50   0  1.9G  0 part 
│ └─systemvg-mylv 253:3    0    5G  0 lvm  /mylv
└─sdd3              8:51   0  1.9G  0 part 
  └─systemvg-mylv 253:3    0    5G  0 lvm  /mylv\
  
  
```

