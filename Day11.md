[TOC]

# if语句

## if单分支

if单分支的语法组成：

```shell
if  条件测试;then  
	命令序列
fi
```

```shell
if  条件测试
	then 命令序列
fi
```

### 应用案例

判断用户名与密码是否为空

```shell
[root@som ~]# cd /root/shell/day01

[root@som day01]# vim user_v1.sh
#!/bin/bash
read -p "请输入用户名：" user
read -s -p "请输入密码：" pass
if [ ! -z "$user" ];then
	 useradd "$user"
fi
if [ ! -z "$pass" ];then
     echo "$pass" | passwd --stdin "$user"
fi
echo
```

测试，输入用户名和密码

```shell
[root@som day01]# chmod  +x user_v1.sh 
[root@som day01]# ./user_v1.sh 
请输入用户名：testuser       						#输入用户名
请输入密码：更改用户 testuser 的密码 。
passwd：所有的身份验证令牌已经成功更新。
```

测试，直接回车

```shell
[root@som day01]# ./user_v1.sh 
请输入用户名：            							#直接回车
请输入密码：                 						 #直接回车
```

优化脚本，使用更简单的语句书写脚本，合并if语句

```shell
[root@som day01]# vim user_v2.sh
#!/bin/bash
read -p "请输入用户名：" user
read -s -p "请输入密码：" pass
if [ ! -z "$user" ] && [ ! -z "$pass" ];then
    useradd "$user"
    echo "$pass" | passwd --stdin "$user"
fi
```

```shell
[root@som day01]# chmod +x user_v2.sh 
[root@som day01]# ./user_v2.sh 
请输入用户名：testuser2
请输入密码：更改用户 testuser2 的密码 。
passwd：所有的身份验证令牌已经成功更新。
```

## if双分支

- if双分支的语法组成：既可以执行成功的命令，也可以执行失败的命令

```shell
if  条件测试;then
	命令序列1
else  
	命令序列2
fi
```

### 应用案例

测试主机是否能ping通，需要用户输入一个参数，比如一个IP或者域名

```shell
[root@som day01]# vim if_ping.sh
#!/bin/bash
if [ -z "$1" ];then
      echo -n "用法：脚本"
      echo -e "\033[32m域名或IP\033[0m"
      exit
fi
ping -c2 -i0.1 -W1 "$1" &> /dev/null

if [ $? -eq 0 ];then
		echo "$1 is up"
else
	    echo "$1 is down"
fi
```

```shell
[root@som day01]# chmod +x if_ping.sh 
[root@som day01]# ./if_ping.sh 
用法：脚本域名或IP
```

```shell
[root@som day01]# ./if_ping.sh  192.168.4.7
192.168.4.7 is up
[root@som day01]# ./if_ping.sh  192.168.10.1
192.168.10.1 is down
```

# if多分支

- if多分支：针对多个条件分别执行不同的操作

```shell
if    条件测试1 ;then  
		命令序列1
elif  条件测试2 ;then  
		命令序列2
else
		命令序列n
fi
```

### 应用案例

```shell
[root@som day01]# echo $RANDOM             #系统随机生成的数字
[root@som day01]# vim guess_num.sh
#!/bin/bash
clear
num=$[RANDOM%10+1]
read -p "请输入1-10之间的整数：" guess
if [ $guess -eq $num ];then
      echo "恭喜你，才猜对了，就是$num"
elif [ $guess -lt $num ];then
      echo "Oops,猜小了"
else
      echo "Oops,猜大了"
fi
```

```shell
[root@som day01]# chmod +x guess_num.sh
[root@som day01]# ./guess_num.sh
请输入1-10之间的整数：6
Oops,猜大了
```

# for循环

- 根据变量的不同取值，重复执行命令序列（for循环使用场合，在知道循环次数的时候使用）

for循环的语法结构：第一种格式

```shell
for  变量名  in  值列表
do
	命令序列
done
```

for循环范例，循环输出数字和字符

```shell
[root@som ~]# cd /root/shell/day01/
[root@som day01]# vim for_demo1.sh
#!/bin/bash
for i in 1 8 ab 99 qq
do
       echo "I am $i"
done
```

for循环的语法结构：第二种格式

```shell
for  ((初值;条件;步长))
do
   命令序列
done
```

案例：

```shell
[root@som day01]# vim for_demo2.sh
#!/bin/bash
for ((i=1;i<=5;i++))
do
      echo "I am $i"
done
```

```shell
[root@som day01]# chmod +x for_demo2.sh 
[root@som day01]# ./for_demo2.sh
```

循环创建10个系统账户（有规律的创建）

```shell
[root@som day01]# vim for_user.sh
#!/bin/bash
for i in {1..10}
do
       useradd test$i
      echo 123456 | passwd --stdin test$i
done
```

```shell
[root@som day01]# chmod +x for_user.sh 
[root@som day01]# ./for_user.sh
```

批量创建系统账户（无规律的创建，既可以读取用户名列表文件创建系统账户）

```shell
[root@som day01]# vim user.txt             #创建系统用户的文件
ocean
book
page
```

```shell
[root@som day01]# vim user.sh
#!/bin/bash
for i in $(cat user.txt)
do
       useradd $i
       echo 123456 | passwd --stdin $i
done
```

```shell
[root@som day01]# chmod  +x user.sh 
[root@som day01]# ./user.sh
```

# while循环

- 反复测试条件，只要成立就执行命令序列（死循环或者条件循环时使用【不知道循环次数时】）

while循环的语法结构：

```shell
while  条件测试
do
   命令序列
done
```

死循环语法结构

```shell
while :
do
  命令序列
done
```

练习while循环基本用法

定义i=1判断$i是否小于等于5，条件成立则执行循环语句

```shell
[root@som day01]# vim while_1.sh
#!/bin/bash
i=1
while [ $i -le 5 ]
do
       echo $i
done
```

定义i=1判断$i是否小于等于5，条件成立则显示$i，并执行let i++，如果条件失败，则循环结束

```shell
[root@som day01]# vim while_2.sh
#!/bin/bash
i=1
while [ $i -le 5 ]
do
     echo $i
      let i++
done
```

```shell
[root@som day01]# chmod +x while_1.sh
[root@som day01]# chmod +x while_2.sh
[root@som day01]# ./while_1.sh          			 #ctrl+C结束循环
[root@som day01]# ./while_2.sh
```

死循环脚本，输出hello word 和 nihao

```shell
[root@som day01]# vim while_3.sh
#!/bin/bash
while :
do
       echo hello world
       echo nihao
done
```

```shell
[root@som day01]# chmod +x while_3.sh
[root@som day01]# ./while_3.sh
```

## 案例

- 使用while循环批量创建有规律的用户，定义两个变量PREFIX和i，使用while循环，判断i小于等于5，条件满足会执行创建用户的命令，否则则退出脚本

```shell
[root@som day01]# vim uaddwhile.sh
#!/bin/bash
PREFIX="tuser"
i=1
while [ $i -le 5 ]
do
       useradd ${PREFIX}$i
       echo 123456 | passwd --stdin ${PREFIX}$i &> /dev/null
       let i++
done
```

```shell
[root@som day01]# chmod +x uaddwhile.sh 
[root@som day01]# ./uaddwhile.sh
```

- 改编随机猜数脚本，可以无限次猜数，直到猜对才退出

```shell
[root@som day01]# vim guess_num2.sh
#!/bin/bash
num=$[RANDOM%10+1]
while :
do
      read -p "请输入1-10之间的整数：" guess
      if [ $guess -eq $num ];then
             echo "恭喜，猜对了，就是$num."
             exit    
      elif [ $guess -lt $num ];then
             echo "Oops,猜小了."
      else 
             echo "Oops,猜大了."
fi
done
```

```shell
[root@som day01]# chmod +x guess_num2.sh 
[root@som day01]# ./guess_num2.sh
```

# 正则表达式

- 正则表达式是一种基于字符模式匹配的文本处理工具

## 基本正则

![1685083301838](Day11.assets/1685083301838.png)

```shell
[root@som ~]# grep root /etc/passwd                    #查找包含root的行
[root@som ~]# grep ^root /etc/passwd                  	#查找以root开头的行
[root@som ~]# grep bash$ /etc/passwd                 	#查找以bash结尾的行
[root@som ~]# grep "[abc]" /etc/passwd               	#查找包含a或者b或者c的行
[root@som ~]# grep "[^abc]" /etc/passwd             	#查找不包含a或者b或者c的其他内容
[root@som ~]# grep . /etc/passwd                       #查找任意单个字符
[root@som ~]# grep r.*t /etc/passwd                    #查找以r开头以t结尾的
[root@som ~]# grep "[0-9]*"  /etc/passwd          		#查找包含数字的，*代表任意次
[root@som ~]# grep "[0-9]\{3,4\}" /etc/passwd    		#查找包含数字3-4次的
[root@som ~]# grep "[0-9]\{3\}" /etc/passwd       		#查找包含3位数的
```

![1690791428709](Day11.assets/1690791428709.png)

扩展正则的使用

```shell
[root@som ~]# grep -E "0{2,3}" /etc/passwd         #查找0出现2-3次
[root@som ~]# grep -E "[a-z]+"  /etc/passwd        #查找a-z等字母至少出现一次
[root@som ~]# grep -E "s?bin"  /etc/passwd         #查找sbin或者bin(?匹配前面的s字符0-1次)
[root@som ~]# grep -E "(root|daemon)" /etc/passwd  #查找root或者daemon
[root@som ~]# echo "ababab" | grep ab              #查找ab
ababab
[root@som ~]# echo "ababab" | grep -E "(ab)"       #查找ab
[root@som ~]# echo "ababab" | grep -E "(ab){2}"    #将ab组合，匹配两次
```

## Perl兼容的正则

![1685083469917](Day11.assets/1685083469917.png)

```shell
[root@som ~]# grep -P "bin" /etc/passwd      #匹配包含bin的行，只要包含bin字符的都出现
[root@som ~]# grep -P "\bbin\b" /etc/passwd  
									# \b单词边界，b前面不能有内容，n后面也不能有内容，只匹配bin
[root@som ~]# grep -P "\w"  /etc/passwd      #查找字母数字下划线
[root@som ~]# grep -P "\W"  /etc/passwd      #查找不是字母数字下划线部分
[root@som ~]# grep -P "\s"  /etc/passwd      #查找空白，空格，tab键都算
[root@som ~]# grep -P "\d"  /etc/passwd      #查找数字
[root@som ~]# grep -P "\D"  /etc/passwd      #查找非数字
```

# 综合练习

准备素材文件

```shell
[root@som ~]# python -c "import this" > /opt/python.txt		#生成素材文件
```

过滤/opt/python.txt文件中包含the的行

```shell
[root@day01 ~]# grep "the"  /opt/python.txt
```

过滤包含bet或者better的行

```shell
[root@day01 ~]# grep -E  "(bet|better)" /opt/python.txt
```

过滤包含1-2个字母o的行

```shell
[root@day01 ~]# grep -E  "o{1,2}" /opt/python.txt
```

过滤所有标点符号

```shell
[root@day01 ~]# grep -P "\W"  /opt/python.txt
```

过滤/opt/python.txt文件中以.结尾的行

```shell
[root@day01 ~]# grep "\.$" /opt/python.txt
```

过滤包含2个o的行

```shell
[root@day01 ~]# grep -E "o{2}" /opt/python.txt
```

过滤大写字母开头的行

```shell
[root@day01 ~]# grep  "^[A-Z]"  /opt/python.txt
```

# 总结

- 掌握SHELL中的if判断语句
  - 单分支、双分支、多分支
- 掌握for循环使用格式
- 掌握while循环使用格式
- 掌握正则表达式
  - 基本正则、扩展正则、Perl兼容正则



