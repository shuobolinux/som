# rsync增量同步

- rsync是增量同步的一种工具，可以实现本地目录之间数据同步，也可以实现远程跨主机之间数据同步

## scp与rsync的区别

- scp属于全量拷贝，如果目标位置已经有了相同的数据，则直接覆盖。
- rsync属于增量拷贝，如果目标位置已经有了相同的数据，则跳过，只传输变化的数据。
- 如果是首次传递数据，scp和rsync没有区别。

## rsync常用选项

- -a：是-rlptgoD选项的集合
- -v：显示传输的过程
- -z：传递数据的过程中对数据进行压缩
- --delete：删除目标目录中的多余数据（保持目标目录和源目录数据一致性）

# rsync本地实验

在虚拟机A创建两个文件夹/src和/dst

```shell
[root@som ~]# mkdir /src
[root@som ~]# mkdir /dst
```

向/src增加一些源数据

```shell
[root@som ~]# echo 123 > /src/a.txt
[root@som ~]# cp /etc/passwd /src/
[root@som ~]# mkdir /src/test
```

使用rsync向目标目录/dst同步数据

- 在同步时，源路径没有写目录后边的/，则表示将源目录本身一并同步至目标目录
- 在同步时，源路径写了后边的/，则表示只同步源目录下边的数据

```shell
[root@som ~]# rsync -av /src /dst/			#同步源目录下边的数据及源目录本身
sending incremental file list
src/
src/a.txt
src/passwd
src/test/

sent 2,568 bytes  received 62 bytes  5,260.00 bytes/sec
total size is 2,343  speedup is 0.89

[root@som ~]# ls /dst/
src
```

```shell
[root@som ~]# rm -rf /dst/src			#删除数据
```

```shell
[root@som ~]# rsync -av /src/ /dst/		#同步源目录下边的数据
sending incremental file list
./
a.txt
passwd
test/

sent 2,558 bytes  received 61 bytes  5,238.00 bytes/sec
total size is 2,343  speedup is 0.89
[root@som ~]# ls /dst/
a.txt  passwd  test	
```

向/src增加新数据，测试增量同步

```shell
[root@som ~]# cp /etc/centos-release /src/
[root@som ~]# rsync  -av /src/  /dst/			#只传输新数据
sending incremental file list
./
centos-release

sent 278 bytes  received 39 bytes  634.00 bytes/sec
total size is 2,601  speedup is 8.21
```

验证--delete选项（用于保持目标目录和源目录数据一致）

向目标目录新增数据

```shell
[root@som ~]# echo "1234567890"  > /dst/test.txt
[root@som ~]# cp /etc/default/useradd  /dst/
```

向源目录新增数据

```shell
[root@som ~]# echo 000 > /src/game.txt
```

测试

```shell
[root@som ~]# rsync -av --delete /src/ /dst/
sending incremental file list
deleting useradd
deleting test.txt
./
game.txt

sent 269 bytes  received 62 bytes  662.00 bytes/sec
total size is 2,605  speedup is 7.87
```

# rsync远程同步实验

- 上传：rsync [选项]  源数据    用户@主机:/目标路径
- 下载：rsync [选项]  用户@主机:/源数据   /目标路径  

## 练习上传

在虚拟机A，将虚拟机A的/src下边的数据同步至虚拟机B的/opt，保持数据一致

```shell
[root@som ~]# rsync -avz --delete /src/ root@192.168.4.207:/opt/
```

## 练习下载

在虚拟机B把/opt/多余的数据都删除掉

```shell
[root@pc207 ~]# rm -rf /opt/*
```

在虚拟机B，将虚拟机A的/src下边的数据同步至虚拟机B的/opt，保持数据一致

```shell
[root@pc207 ~]# rsync -avz --delete root@192.168.4.7:/src/ /opt/
```

# 总结

- rsync增量同步的原理
- 掌握rsync与scp的区别
- 掌握rsync常用选项

